'use strict';

const express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const session = require('express-session');
// var login = require('./utils/login');

const app = express();
app.use(session({
                  secret: 'JddsHRBe1dPSWm98M29d5gQMrY78jQ1em5m',
                  saveUninitialized: true,
                  resave: true,
                  secure: true,
                  rolling: true,
                  cookie: { //secure: true ,
                          maxAge: 3600000,},
                }));
// var router = express.Router();
// app.use(router);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
// app.all('/hw/*', login.checkUserSession);
require('./utils/routes.js')(app);


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public/images', 'favicon.ico')));
app.use(logger('dev'));


app.use(express.static(path.join(__dirname, 'public')));

//
// app.get('/login', login.loginPage);
// app.post('/login', login.loginFunc);


// app.use('/public/easy-autocomplete', express.static(__dirname + '/node_modules/easy-autocomplete/dist/'));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
// development error handler
// will print stacktrace
//  if (app.get('env') === 'development') {
//    app.use(function(err, req, res, next) {
//      res.status( err.code || 500 )
//      .json({
//        status: 'error',
//        message: err
//      });
//    });
//  }
//
//  // production error handler
//  // no stacktraces leaked to user
//  app.use(function(err, req, res, next) {
//    res.status(err.status || 500)
//    .json({
//      status: 'error',
//        message: err.message
//    });
//  });
});

module.exports = app;
