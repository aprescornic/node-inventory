#!/usr/bin/python
import argparse
import os
import sys
import subprocess
import getpass
import urllib2

pubkeys = [
    'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCzJlVGu9MYLM6lMv/mN42yoBVzsTbjg73qN7OvNmAnrUfWcYtubg1xRgnbe9dl9aqWfuF9H7AhXSgdNHTSxGqjCCLzBbw+EhH2IPYVCr8xqUHcAMGuhsIjYSX2iFVtDh8+nzRMtpbw4AWO7rwtYkj+1JRwUoClUa7Ahx3yX+zQkZJ/+TNrIkPq4qkeL/ZprEAVeSbMt4wVHckB2zyvKuZa/dIhwZM//NjhSg+q59b2JtzSURYfFGb6FezzRpXJc6E+2yby17O0/HmELBoe7SRlq5llutjnJ3Zv78Qp9tbJEMbbdtQiFmULXlKYMw6tz6ySTUWHjJ9yGRSwMoZVykIB ibreahna@ion'
]

parser = argparse.ArgumentParser(prog='cfhw-linux', description='Code Factory asset management tool v1.0.0')

parser.add_argument('-u', '--inventory-url',
    help='Inventory server URL',
    metavar='URL',
    default='http://inventory.codefactorygroup.com/api/collected2/')

parser.add_argument('-i', '--user-id',
    help='User ID for which the hardware information is being sent',
    metavar='USER_ID',
    required=True)

parser.add_argument('-p', '--pin',
    help='Unique random PIN which may be used for authentication on the inventory server side',
    metavar='PIN')

parser.add_argument('--use-apt',
    help='Use apt-get to install OpenSSH server (only when "-a" is passed). Default - False',
    action='store_true',
    default=False)

parser.add_argument('--use-adduser',
    help='Use adduser to add administrator users (only when "-a" is passed). Default - False',
    action='store_true',
    default=False)

parser.add_argument('-a', '--access',
    help='Set up remote administration access. Default - False',
    action='store_true')

parser.add_argument('--ra-username',
    help='Remote administration username. Default - cfadmin',
    metavar='USERNAME',
    default='cfadmin')

args = parser.parse_args()

def collect():
    return subprocess.check_output(['lshw', '-json'])

def send(url, data):
    f = urllib2.urlopen(url, data)
    response = f.read()
    f.close()
    return response

def provide_access():
    #print 'Dissabled temporary'
    if args.use_apt:
        os.system('apt-get update >/dev/null \
        && apt-get install openssh-server >/dev/null')
    if args.use_adduser:
        os.system('adduser --disabled-password --gecos "" ' + args.ra_username +
        '&& mkdir /home/' + args.ra_username + '/.ssh')
        os.system('adduser ' + args.ra_username + ' sudo')

    os.system('echo > /home/' + args.ra_username + '/.ssh/authorized_keys')
    for pubkey in pubkeys:
        os.system('echo \'' + pubkey + '\' >> /home/' + args.ra_username + '/.ssh/authorized_keys')

if sys.version_info[0] != 2:
    print 'Please, use Python version 2.'
    sys.exit(1)

if getpass.getuser() != 'root':
    print 'Please, run this tool as root.'
    #sys.exit(1)

if args.access:
    try:
        provide_access()
    except Exception, e:
        print 'Unable to provide remote administration access: ' + str(e)

try:
    hardwareInformation = collect()
except Exception, e:
    print 'Unable to collect hardware information: ' + str(e)
    sys.exit(1)

try:
    url = args.inventory_url + urllib2.quote(args.user_id)
    if args.pin != None:
        url += '&pin=' + urllib2.quote(args.pin)
    send(url, hardwareInformation)
except Exception, e:
    print 'Unable to send hardware information to inventory server: ' + str(e)
    sys.exit(1)
