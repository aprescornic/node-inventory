$(function () {
  "use strict";
  $.jgrid.defaults.styleUI = 'Bootstrap';
  var $grid = $("#list"),
  initDateEdit = function (elem) {
    $(elem).datepicker({
      dateFormat: "dd-M-yy",
      autoSize: true,
      changeYear: true,
      changeMonth: true,
      showButtonPanel: true,
      showWeek: true
    });
  },
  initDateSearch = function (elem) {
    setTimeout(function () {
      initDateEdit(elem);
    }, 100);
  };
  $grid.jqGrid({
    // data: mydata,
    url: defaultTableURL,
	  datatype: "json",
    jsonReader : {
        root: "data",
      },
    loadonce: true,
    colNames: [ "ID", "Name"],
    colModel: [
      // { name: "act", template: "actions" },
      { name: "id", width: 56, template: "integer", editable:false },
      { name: "name", align: "left", editrules: {required: true} },
    ],
    cmTemplate: { editable: true, autoResizable: true },
    guiStyle: "bootstrap",
    iconSet: "fontAwesome",
    rowNum: 10,
    autoResizing: { compact: true },
    autowidth: true,
    rowList: [5, 10, 20, "10000:All"],
    viewrecords: true,
    // width:750,
    pager: true,
    toppager: true,
    rownumbers: true,
    sortname: "id",
    sortorder: "asc",
    altRows: true,
    searching: {
      defaultSearch: "cn"
    },
    // subGrid: true,
    // subGridRowExpanded: function (subgridDivId, rowId) {
    //   $("#" + $.jgrid.jqID(subgridDivId)).html("<em>simple subgrid data for the row with id=" + rowId + "</em>");
    // },
    // inlineEditing: { keys: true },
    caption: "Positions list",
    editurl: defaultTableURL+"/",
    onSelectRow: function (id) {
        var $this = $(this), gridIdSelector = '#' + $.jgrid.jqID(this.id);
        $this.jqGrid('setGridParam', {
            editurl: defaultTableURL+"/"+id
        });
      },
    gridComplete: initGrid
  }).jqGrid("navGrid",
            { view: false,edit:true,add:true,del:true,search:true },
            {mtype : "PUT", afterSubmit: function (id) {jQuery("#list").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');}},
            {url: defaultTableURL, afterSubmit: function (id) {jQuery("#list").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');}},
            {mtype : "DELETE"})
  // .jqGrid("inlineNav")
  .jqGrid("filterToolbar")
  .jqGrid("gridResize")
  .jqGrid("navButtonAdd",
      {caption: "", buttonicon: "fa fa-print fa-2x", title: "Print", onClickButton: function () {PrinTable("list");}
		});
});


function initGrid() {
  $(this).contextMenu('contextMenu', {
    menuStyle :{
      width : "150px"
    },
    bindings: {
      'edit': function (t) {
        editRow();
      },
      'add': function (t) {
        addRow() ;
      },
      'del': function (t) {
        delRow();
      }
    },
    onContextMenu: function (event, menu) {
      var rowId = $(event.target).parent("tr").attr("id")
      var grid = $("#list");
      grid.setSelection(rowId);

      return true;
    }
  });
}

function addRow() {
  var grid = $("#list");
  grid.jqGrid('setGridParam', {
      editurl: defaultTableURL
  });
  grid.editGridRow("new", { closeAfterAdd: true, afterSubmit: function (id) {jQuery("#list").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');}})
}

function editRow() {
  var grid = $("#list");
  var rowKey = grid.getGridParam("selrow");
  if (rowKey) {
    grid.editGridRow(rowKey, {closeAfterEdit: true,mtype : "PUT", afterSubmit: function (id) {jQuery("#list").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');} });

  }
  else {
    alert("No rows are selected");
  }
}

function delRow() {
  var grid = $("#list");
  var rowKey = grid.getGridParam("selrow");
  if (rowKey) {
    grid.delGridRow(rowKey, {mtype : "DELETE"});
  }
  else {
    alert("No rows are selected");
  }
}

function PrinTable(tableid){
  start=1;
  // if (multiselect===true){start=2};
  var newWin3=window.open('','Print workplaces','');
  newWin3.focus();
  newWin3.document.write('<html>');
  newWin3.document.write("<script>printable=true;\x3C/script>");
  newWin3.document.write($("#idheader").html());
  newWin3.document.write('<body>');
  colNames=jQuery("#"+tableid).jqGrid('getGridParam',"colNames");
  colModel=jQuery("#"+tableid).jqGrid('getGridParam',"colModel");
  dataids=$("#"+tableid).getDataIDs();

  table='<table class="table table-striped table-bordered table-condensed">';
  table=table+'<thead><tr>';
  for(i=start;i<colModel.length;i++){
    if (colModel[i].hidden==false){
      table=table+'<th>'+colNames[i]+'</th>';
    };
  };
  table=table+'</tr></thead>';
  for(z=0;z<dataids.length;z++){
    dat=$("#"+tableid).getRowData(dataids[z]);
    table=table+'<tr>';
    for(i=start;i<colModel.length;i++){
      if (colModel[i].hidden==false){
        table=table+'<td>'+dat[colModel[i].name]+'</td>';
      };
    };
    table=table+'</tr>';
  };

  table=table+"</table>";
  newWin3.document.write(table);
  newWin3.document.write('</body></html>');
  newWin3.document.close();
};
