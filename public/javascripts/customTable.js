
function runningFormatter(value, row, index) {
  return 1+index;
}

function formatSelect2Data(data) {
  return $.map(data, function (obj) {
    obj.text = obj.text || obj.name; // replace name with the property used for the text
    return obj;
  })
}

function initSelect2Data(element, url, parrent){
  var parrentElement = null;
  if (parrent) parrentElement = $(parrent);
  $.getJSON(url).done(
    function( url_data ) {
      $(element).select2({ //prepend('<option selected></option>').
        data: formatSelect2Data(url_data.data),
        dropdownAutoWidth: true,
        dropdownParent:  parrentElement,
      })
    }
  )
}


function getIdChecked(table) {
  var list = [];
  table.bootstrapTable('getSelections').forEach(function(item){list.push(item.id)})
  return list;
}

function printTable(table) {
    var pdf = new jsPDF('p', 'pt', 'a4');
    source = $(table)[0];
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function (element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 522
    };
    var pdfHeader = function(data) {
      pdf.setFontSize(18);
      pdf.setTextColor(40);
      pdf.setFontStyle('normal');
      //doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
      pdf.text(title, data.settings.margin.left, 50);
    };

    var options = {
      beforePageContent: pdfHeader,
      margin: {
        top: 80
      },
      styles: {overflow: 'linebreak', columnWidth: 'wrap'},
      //startY: pdf.autoTableEndPosY() + 80
    };
    var res = pdf.autoTableHtmlToJson(source);
    pdf.autoTable(res.columns, res.data, options);
    var string = pdf.output('datauristring');

      var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"

      var x = window.open();
      x.document.open();
      x.document.write(iframe);
      x.document.close();
    //pdf.save('Test.pdf');
}

/**
 * @author Michael Sogos <michael.sogos@gurustudioweb.it>
 * @version v1.0.0
 * @modified AP
 * @link https://github.com/michaelsogos/bootstrap-table-toolbar-buttons
 */

(function ($) {
	'use strict';
	var sprintf = $.fn.bootstrapTable.utils.sprintf;

  $.extend($.fn.bootstrapTable.columnDefaults, {
    // align: 'center',
    // valign: 'middle'
    sortable:true,
    // class: 'min',
    filterStrictSearch: false,
  });


	$.extend($.fn.bootstrapTable.defaults, {
    //            method: 'post',
    pagination: true,
    // paginationVAlign: 'top',
    // paginationDetailHAlign: 'right',
    //            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    showRefresh: true,
    sidePagination: 'client',
    showColumns: true,
    showRefresh: true,
    // showFullscreen: true,
    showExport: true,
    exportTypes: ['csv', 'excel', 'doc', 'txt','json'],
    pageList: [10, 25, 50, 100, 'All'],
    sortable: true,
    clickToSelect: true,
    showFooter: false,
    filter: true,
    buttonsAlign: 'left',
    // toolbarAlign: 'right',
		customToolbarButtons: []
	});

	// $.extend($.fn.bootstrapTable.defaults);
  //
	var BootstrapTable = $.fn.bootstrapTable.Constructor,
		_initToolbar = BootstrapTable.prototype.initToolbar;

	BootstrapTable.prototype.initToolbar = function () {
		var self = this;
		_initToolbar.apply(this, Array.prototype.slice.apply(arguments));

		if (this.options.customToolbarButtons.length > 0) {
			this.showToolbar = true;
      var btnGroup = self.$toolbar.find('>.btn-group');
      // $('<div class="clearfix"></div>').prependTo(btnGroup);
			$.each(this.options.customToolbarButtons.reverse(), function (index, item) {

				var button = btnGroup.find('button[name=' + item.name + ']');
				if (!button.length) {
          var disabledStr = '';
          if(item.disabled)
            disabledStr=' disabled ';

					var htmlButton = '<button ' +
						sprintf('name="%s" ', item.name) +
            sprintf('id="%s" ', item.name) +
						sprintf('title="%s" ', item.title) +
						sprintf('aria-label="%s" ', item.title) +
						'class="btn ' +
						sprintf('btn-%s ', self.options.buttonsClass) +
						sprintf('btn-%s ', self.options.iconSize) +
						'" type="button"'+ disabledStr +'><i class="' +
						sprintf('%s ', self.options.iconsPrefix) +
						sprintf('%s ', item.icon) +
						'"></i></button>';

					// button = $(htmlButton).appendTo(btnGroup);
          button = $(htmlButton).prependTo(btnGroup);

					button.on("click", item.callback);
				}
			});
		}
	};
})(jQuery);
