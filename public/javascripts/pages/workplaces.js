var $table = $('#dataTable');

var typeMap = new Map();
var departmentsMap = new Map();
var employeesMap = new Map();

var editMode={
  insertMode: true,
  url: table_source,
  method: 'post',
}

$(document).ready(function () {
  initPageData();
})

function initPageData(){
  initSelect2Data("#modal_wp_type_id", "/api/wp_type" , '#editModal')
  initSelect2Data("#modal_departments_id", "/api/departments" , '#editModal')
  initSelect2Data("#modal_employees_id", "/api/employees" , '#editModal')
  initTypeFilterData();
  initDepartmentsFilterData();
  initEmployeeFilterData();

  var table_columns = [
      {
        formatter: 'runningFormatter',
        class: 'min',
        title: "#",
      },
      {
        field: 'state',
        checkbox: 'true',
        class: 'min',
      },
      {
        field: "id",
        title: "ID",
        visible: false,
        class: 'min',
        filter: {
          type: "input"
        }
      },{
        field:  "name",
        title: "Name",
        formatter: formatName,
        filter: {
          type: "input",
          filterControlPlaceholder: "Name",
        }
      },
      {
        field: "wp_type_id",
        title: "Type",
        formatter: formatType,
        filterStrictSearch: true,
        class: 'min',
        filter: {
                type: "select",
                data: [],
              }
      },
      {
        field: "departments_id",
        title: "Department",
        formatter: formatDepartments,
        filterStrictSearch: true,
        class: 'min',
        filter: {
                type: "select",
                data: [],
              }
      },
      {
        field: "employees_id",
        title: "Employee",
        formatter: formatEmployees,
        filterStrictSearch: true,
        class: 'min',
        filter: {
          type: "select",
          data: [],
        }
      },
      {
        field: "room",
        title: "Room",
        class: 'min',
        filter: {
                type: "input",
              }
      },{
        field: "ip",
        title: "IP",
        class: 'min',
        filter: {
                type: "input",
              }
      },{
        field: "phone",
        title: "Phone",
        class: 'min',
        filter: {
                type: "input",
              }
      },{
        field: "comments",
        title: "Comments",
        class: 'min',
        filter: {
                type: "input",
              }
      }
    ]

  $table.bootstrapTable({
    filterControl: true,
    url: table_source,
    groupBy: false,
    groupByField: 'departments_id',
    customToolbarButtons: [
      {
        name: "addItem",
        title: "Add",
        icon: "glyphicon-plus",
        callback: addComponents,
      },{
        name: "editItem",
        title: "Edit",
        icon: "glyphicon-edit",
        callback: editComponents,
        disabled: true,
      },{
        name: "deleteItem",
        title: "Delete",
        icon: "glyphicon-trash",
        callback: deleteComponents,
        disabled: true,
      },{
        name: "table_pdf",
        title: print,
        icon: "glyphicon-print",
        callback: function(){ printTable($table)},
      }
    ],
    columns: table_columns,
  }
  );
}

///Column Formaters
function formatName(value, row, index){
  return "<a href=/workplaces/"+row.id+">"+value+"</a>";
}


function initTypeFilterData(){
  $.getJSON("/api/wp_type").done(
    function( type_data ) {
      if (type_data.data != null) {
        for(var i=0;i< type_data.data.length; i++){
          typeMap.set(type_data.data[i].id, type_data.data[i].name);
        }
        var select2values = Array.from( typeMap.values());
        select2values.unshift("");
        $table.bootstrapTable("setSelect2Data", "wp_type_id", select2values);
      }
    })
}

function formatType(value,row,index){
  return typeMap.get(value);
}

function initDepartmentsFilterData(){
  $.getJSON("/api/departments").done(
    function( departments_data ) {
      if (departments_data.data != null) {
        for(var i=0;i< departments_data.data.length; i++){
          departmentsMap.set(departments_data.data[i].id, departments_data.data[i].name);
        }
        var select2values = Array.from( departmentsMap.values());
        select2values.unshift("");
        $table.bootstrapTable("setSelect2Data", "departments_id", select2values);
      }
    })
}

function formatDepartments(value,row,index){
  return departmentsMap.get(value);
}

function initEmployeeFilterData(){
  $.getJSON("/api/employees").done(
    function( employees_data ) {
      if (employees_data.data != null) {
        for(var i=0;i< employees_data.data.length; i++){
          employeesMap.set(employees_data.data[i].id, employees_data.data[i].name);
        }
        var select2values = Array.from( employeesMap.values());
        select2values.unshift("");
        $table.bootstrapTable("setSelect2Data", "employees_id", select2values);
      }
    })
}

function formatEmployees(value,row,index){
  return employeesMap.get(value);
}
///EnableDisable editDelete buttons
$table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
    $('#deleteItem').prop('disabled', !$table.bootstrapTable('getSelections').length);
    $('#editItem').prop('disabled', !($table.bootstrapTable('getSelections').length===1));
 });

///Edit Items
$('#modalForm').submit(function(e){
  e.preventDefault();
  var url=$(this).closest('form').attr('action'),
  data=$(this).closest('form').serialize();
  $.ajax({
    url:editMode.url,
    type:editMode.method,
    data:data,
    success:function(){
      $('#editModal').modal('hide');
      $table.bootstrapTable('refresh')
     }
  });
});

function addComponents(){
  editMode={
    insertMode: true,
    url: table_source,
    method: 'post',
  }
  $('#editModal').modal('show');
}

function editComponents(){
  var checkedRows = getIdChecked($table);
  if (checkedRows.length === 1){
    editMode={
      insertMode: false,
      url: table_source + $table.bootstrapTable('getSelections')[0].id ,
      method: 'put',
    }
    $('#modal_name').val( $table.bootstrapTable('getSelections')[0].name);
    $('#modal_cwp_type_id').val( $table.bootstrapTable('getSelections')[0].wp_type_id).trigger('change');
    $('#modal_departments_id').val( $table.bootstrapTable('getSelections')[0].departments_id).trigger('change');
    $('#modal_employees_id').val( $table.bootstrapTable('getSelections')[0].employees_id).trigger('change');
    $('#modal_room').val( $table.bootstrapTable('getSelections')[0].room)
    $('#modal_ip').val( $table.bootstrapTable('getSelections')[0].ip)
    $('#modal_phone').val( $table.bootstrapTable('getSelections')[0].phone)
    $('#modal_comments').val( $table.bootstrapTable('getSelections')[0].comments)

    $('#editModal').modal('show');
  }
}

function deleteComponents() {
  var checkedRows = getIdChecked($table);
  if (checkedRows.length  && confirm('Are you sure you want to detele from database?')) {
    for (var i = 0, len = checkedRows.length; i < len; i++) {
      $.ajax({
        url:table_source+checkedRows[i],
        type: 'DELETE',
        success: function(){ $table.bootstrapTable('refresh')}
      });
    }
  }
};
