var $table = $('#dataTable');

var typeComponetsMap = new Map();
var manufacturersMap = new Map();

var editMode={
  insertMode: true,
  url: table_source,
  method: 'post',
}

$(document).ready(function () {
  initPageData();
})

function initPageData(){
  initSelect2Data("#modal_cpnt_type_id", "/api/cpnt_type" , '#editModal')
  initSelect2Data("#modal_manufacturers_id", "/api/manufacturers" , '#editModal')
  initComponentTypeFilterData();
  initManufacturerFilterData();

  var table_columns = [
      {
        formatter: 'runningFormatter',
        class: 'min',
        title: "#",
      },
      {
        field: 'state',
        checkbox: 'true',
        class: 'min',
      },
      {
        field: "id",
        title: "ID",
        visible: false,
        class: 'min',
        filter: {
          type: "input"
        }
      },{
        field:  "name",
        title: "Name",
        formatter: formatName,
        filter: {
          type: "input",
          filterControlPlaceholder: "Name",
        }
      },
      {
        field: "cpnt_type_id",
        title: "Type",
        formatter: formatComponentType,
        filterStrictSearch: true,
        class: 'min',
        filter: {
                type: "select",
                data: [],
              }
      },
      {
        field: "manufacturers_id",
        title: "Manufacturer",
        formatter: formatManufacturer,
        filterStrictSearch: true,
        class: 'min',
        filter: {
                type: "select",
                data: [],
              }
      },
      {
        field: "price",
        title: "Price",
        class: 'min',
        filter: {
          type: "input"
        }
      },
      {
        field: "comments",
        title: "Comments",
        class: 'min',
        filter: {
                type: "input",
              }
      },
    ]

  $table.bootstrapTable({
    filterControl: true,
    url: table_source,
    customToolbarButtons: [
      {
        name: "addItem",
        title: "Add",
        icon: "glyphicon-plus",
        callback: addComponents,
      },{
        name: "editItem",
        title: "Edit",
        icon: "glyphicon-edit",
        callback: editComponents,
        disabled: true,
      },{
        name: "deleteItem",
        title: "Delete",
        icon: "glyphicon-trash",
        callback: deleteComponents,
        disabled: true,
      },{
        name: "table_pdf",
        title: print,
        icon: "glyphicon-print",
        callback: function(){ printTable($table)},
      }
    ],
    columns: table_columns,
  }
  );
}

///Column Formaters
function formatName(value, row, index){
  return "<a href=/hw/components/"+row.id+">"+value+"</a>";
}


function initComponentTypeFilterData(){
  $.getJSON("/api/cpnt_type").done(
    function( type_data ) {
      if (type_data.data != null) {
        for(var i=0;i< type_data.data.length; i++){
          typeComponetsMap.set(type_data.data[i].id, type_data.data[i].name);
        }
        var select2values = Array.from( typeComponetsMap.values());
        select2values.unshift("");
        $table.bootstrapTable("setSelect2Data", "cpnt_type_id", select2values);
      }
    })
}

function formatComponentType(value,row,index){
  return typeComponetsMap.get(value);
}

function initManufacturerFilterData(){
  $.getJSON("/api/manufacturers").done(
    function( manufacturers_data ) {
      if (manufacturers_data.data != null) {
        for(var i=0;i< manufacturers_data.data.length; i++){
          manufacturersMap.set(manufacturers_data.data[i].id, manufacturers_data.data[i].name);
        }
        var select2values = Array.from( manufacturersMap.values());
        select2values.unshift("");
        $table.bootstrapTable("setSelect2Data", "manufacturers_id", select2values);
      }
    })
}

function formatManufacturer(value,row,index){
  return manufacturersMap.get(value);
}

///EnableDisable editDelete buttons
$table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
    $('#deleteItem').prop('disabled', !$table.bootstrapTable('getSelections').length);
    $('#editItem').prop('disabled', !($table.bootstrapTable('getSelections').length===1));
 });

///Edit Items
$('#modalForm').submit(function(e){
  e.preventDefault();
  var url=$(this).closest('form').attr('action'),
  data=$(this).closest('form').serialize();
  $.ajax({
    url:editMode.url,
    type:editMode.method,
    data:data,
    success:function(){
      $('#editModal').modal('hide');
      $table.bootstrapTable('refresh')
     }
  });
});

function addComponents(){
  editMode={
    insertMode: true,
    url: table_source,
    method: 'post',
  }
  $('#editModal').modal('show');
}

function editComponents(){
  var checkedRows = getIdChecked($table);
  if (checkedRows.length === 1){
    editMode={
      insertMode: false,
      url: table_source + $table.bootstrapTable('getSelections')[0].id ,
      method: 'put',
    }
    $('#modal_components_id').val( $table.bootstrapTable('getSelections')[0].components_id)
    $('#modal_component_amount').val( $table.bootstrapTable('getSelections')[0].amount)
    $('#modal_components_serial').val( $table.bootstrapTable('getSelections')[0].serial_nr)
    $('#modal_components_id').trigger('change')
    $('#editModal').modal('show');
  }
}

function deleteComponents() {
  var checkedRows = getIdChecked($table);
  if (checkedRows.length  && confirm('Are you sure you want to detele from database?')) {
    for (var i = 0, len = checkedRows.length; i < len; i++) {
      $.ajax({
        url: '/api/components/'+checkedRows[i],
        type: 'DELETE',
        success: function(){ $table.bootstrapTable('refresh')}
      });
    }
  }
};
