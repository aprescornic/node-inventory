var $table = $('#dataTable');

var positionsMap = new Map();
var positionsObj = [""];

var departmentsMap = new Map();
var departmentsValues = [""];

//
// $.getJSON("/api/cpnt_type", null, function(data) {
//   if (data != null) {
//     for(var i=0;i< data.data.length; i++){
//       types += data.data[i].id + ":"+ data.data[i].name ;
//       if(i<data.data.length-1) types+=";";
//     }
//     setGridRequirentes();
//   }
// });



$(document).ready(function () {
  $('#modal_department_id').select2({
    dropdownAutoWidth: true,
    dropdownParent: $('#editModal')
  });
  $('#modal_position_id').select2({
    dropdownAutoWidth: true,
    dropdownParent: $('#editModal')
  });


  $.getJSON("/api/positions", null, function(data) {
    if (data != null) {
      for(var i=0;i< data.data.length; i++){
        positionsMap.set(data.data[i].id, data.data[i].name);
        positionsObj.push(data.data[i].name);
        $('#modal_position_id').append(new Option(data.data[i].name, data.data[i].id,false, false))
      }
      $.getJSON("/api/departments", null, function(data) {
        if (data != null) {
          for(var i=0;i< data.data.length; i++){
            departmentsMap.set(data.data[i].id, data.data[i].name);
            departmentsValues.push(data.data[i].name);
            $('#modal_department_id').append(new Option(data.data[i].name, data.data[i].id,false, false))
          }


          var table_columns = [
              {
                formatter: 'runningFormatter',
                class: 'min',
                title: "#",
              },
              {
                field: 'state',
                checkbox: 'true'
              },
              {
                field: "id",
                title: "ID",
                visible: false,
                class: 'min',
                sortable: true,
                // filterControl: 'input',
                // filterControlPlaceholder: "id",
                // filterStrictSearch: false
                filter: {
                  type: "input"
                }
              },{
                field:  "name",
                title: "Name",
                sortable:true,
                // filterControl: 'input',
                filterControlPlaceholder: "Name",
                // filterStrictSearch: false
                filter: {
                  type: "input",
                  filterControlPlaceholder: "Name",
                }
              },
              {
                field: "department_id",
                title: "Department",
                formatter: formatDepartments,
                sortable: true,
                // filterControl: 'input',
                // filterControlPlaceholder: "Position",
                filterStrictSearch: true,
                filter: {
                        type: "select",
                        data: departmentsValues
                      }
              },
              {
                field: "position_id",
                title: "Position",
                formatter: formatPositions,
                sortable: true,
                // filterControl: 'input',
                // filterControlPlaceholder: "Position",
                filterStrictSearch: true,
                filter: {
                        type: "select",
                        data: positionsObj
                      }
              },
              {
                field: "username",
                title: "User Name",
                class: 'min',
                sortable: true,
                // filterControl: 'input',
                // filterControlPlaceholder: "UserName",
                // filterStrictSearch: false
                filter: {
                  type: "input"
                }
              },
              {
                field: "allow_login",
                formatter: 'formatYesNo',
                title: "Is admin",
                class: 'min',
                sortable: true,
                // filterControl: 'select',
                // filterControlPlaceholder: "Admin",
                // filterStrictSearch: false
                filter: {
                        type: "select",
                        data: ["", "Yes", "No"]
                      }
              },
            ]



            $table.bootstrapTable({
                toolbar: "#toolbar",
                pagination: true,
                sidePagination: 'client',
                showColumns: true,
                showRefresh: true,
                showFullscreen: true,
                showExport: true,
                exportTypes: ['csv', 'excel', 'doc', 'txt','json'],
                pageList: [10, 25, 50, 100, 'All'],
                sortable: true,
                clickToSelect: true,
                showFooter: false,
                /* search: true, */
                filterControl: true,
                /* disableUnusedSelectOptions: true,  */
                url: table_source,
                columns: table_columns,
                filter: true,
                // filterTemplate: {
                //    input: function(bootstrapTable, column, isVisible) {
                //      return '<input type="text" class="form-control input-sm" data-filter-field="' + column.field + '" style="width: 100%; visibility:' + isVisible + '">';
                //    }
                //  }
              });

        }
      });
    }
  });


})


var editMode={
  insertMode: true,
  url: table_source,
  method: 'post',
}



/* $table.on('click-row.bs.table', function (e, row, $element) {
  if($($element).hasClass('success')) {
    $('.success').removeClass('success');
  }else{
    $('.success').removeClass('success');
    $($element).addClass('success');
  }
}); */

function runningFormatter(value, row, index) {
  return 1+index;
}

function formatYesNo(value,row,index){
  return value==0 ? 'No' : 'Yes';
}

function formatPositions(value,row,index){
  return positionsMap.get(value);
}

function formatDepartments(value,row,index){
  return departmentsMap.get(value);
}

$('#modalForm').submit(function(e){
      e.preventDefault();
      var url=$(this).closest('form').attr('action'),
          // data=$(this).closest('form').serialize();
          data = {};
          data.name = $('#modal_name').val();
          data.department_id = $('#modal_department_id').val();
          data.position_id = $('#modal_position_id').val();
          data.is_group=false;
          data.parrent_group=0;
          data.allow_login = $('#modal_allow_login').is(':checked')
          // console.log(data);
      $.ajax({
          url:editMode.url,
          type:editMode.method,
          data:data,
          success:function(){
              $('#editModal').modal('hide');
              //createTable();
              $table.bootstrapTable('refresh')
             }
          });
      });

$('#table_add').on('click', function(event) {
  editMode={
    insertMode: true,
    url: table_source,
    method: 'post',
  }
  $('#editModal').modal('show');
});

$('#table_edit').click(function () {
  var checkedRows = getIdChecked();
  if (checkedRows.length === 1){
    editMode={
      insertMode: false,
      url: table_source + $table.bootstrapTable('getSelections')[0].id ,
      method: 'put',
    }
    $('#modal_name').val( $table.bootstrapTable('getSelections')[0].name)
    $('#modal_department_id').val( $table.bootstrapTable('getSelections')[0].department_id)
    $('#modal_position_id').val( $table.bootstrapTable('getSelections')[0].position_id)
    $('#modal_allow_login').attr("checked", $table.bootstrapTable('getSelections')[0].allow_login)
    $('#modal_department_id').trigger('change')
    $('#modal_position_id').trigger('change')
    $('#editModal').modal('show');
  }
});

$('#table_delete').click(function () {
  var checkedRows = getIdChecked();
  /* if(getSelectedRow())
    checkedRows.push(getSelectedRow().id) */
  /* checkedRows = unique(checkedRows) */
  if (checkedRows.length  && confirm('Are you sure you want to detele from database?')) {
    for (var i = 0, len = checkedRows.length; i < len; i++) {
      /* console.log("Delete"+checkedRows[i]); */
      $.ajax({
        url: table_source+checkedRows[i],
        type: 'DELETE',
        success: function(){ $table.bootstrapTable('refresh')}
        //error: errorCallback || $.noop
      });
    }
  }
});

 /* function getSelectedRow() {
    var index = $table.find('tr.success').data('index');
    if(index)
      return $table.bootstrapTable('getData')[index];
    else return 0;
}  */
function getIdChecked() {
  var list = [];
  $table.bootstrapTable('getSelections').forEach(function(item){list.push(item.id)})
  return list;
}

function unique(arr) {
  var u = {}, a = [];
  for(var i = 0, l = arr.length; i < l; ++i){
    if(!u.hasOwnProperty(arr[i])) {
      a.push(arr[i]);
      u[arr[i]] = 1;
    }
  }
  return a;
}

$('#table_pdf').click(function () {
    var pdf = new jsPDF('p', 'pt', 'a4');
    source = $('#dataTable')[0];
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function (element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 522
    };
    var pdfHeader = function(data) {
      pdf.setFontSize(18);
      pdf.setTextColor(40);
      pdf.setFontStyle('normal');
      //doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
      pdf.text(title, data.settings.margin.left, 50);
    };

    var options = {
      beforePageContent: pdfHeader,
      margin: {
        top: 80
      },
      styles: {overflow: 'linebreak', columnWidth: 'wrap'},
      //startY: pdf.autoTableEndPosY() + 80
    };
    var res = pdf.autoTableHtmlToJson(source);
    pdf.autoTable(res.columns, res.data, options);
    var string = pdf.output('datauristring');

      var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"

      var x = window.open();
      x.document.open();
      x.document.write(iframe);
      x.document.close();
    //pdf.save('Test.pdf');
})

$('#table_import_user').click(function () {
  $.getJSON("/api/employees/import", null, function(data) {
    if (data != null) {
      $table.bootstrapTable('refresh')
    }
  });
})


$('#table_import_user_pc').click(function () {
  var checkedRows = getIdChecked();
  if (checkedRows.length === 1){
  var rowKey =  $table.bootstrapTable('getSelections')[0].id

  // var grid = $("#list");
  // var rowKey = grid.getGridParam("selrow");
  // if (rowKey) {
  //   // grid.delGridRow(rowKey, {mtype : "DELETE"});
  //   console.log("import "+rowKey);
    $.getJSON("/api/collected/importUserPC/"+rowKey, null, function(data) {
      if (data != null) {
         alert("UserPC IMported");
      }
    });
  }
  else {
    alert("No rows are selected");
  }
})
