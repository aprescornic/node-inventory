var $table = $('#dataTable');
var $componentsTable= $('#componentsTable');
var componentsURL='/api/devices/components/';
var currentComponentsURL;

var typesMap = new Map();
var typeValues = [""];
var workplacesMap = new Map();
var workplacesValues = [""];
var ownersMap = new Map();
var ownersValues = [""];

var typeComponetsMap = new Map();
var typeComponetsValues = [""];
var componetsMap = new Map();
var componetsValues = [""];

$(document).ready(function () {

  $('#modal_dvc_type_id').select2({
    dropdownAutoWidth: true,
    dropdownParent: $('#editModal')
  });
  $('#modal_workplaces_id').select2({
    dropdownAutoWidth: true,
    dropdownParent: $('#editModal')
  });
  $('#modal_partners_id').select2({
    dropdownAutoWidth: true,
    dropdownParent: $('#editModal')
  });
  $('#modal_components_id').select2({
    dropdownAutoWidth: true,
    dropdownParent: $('#editComponentsModal')
  });

  $.getJSON("/api/dvc_type", null, function(data) {
    if (data != null) {
      for(var i=0;i< data.data.length; i++){
        typesMap.set(data.data[i].id, data.data[i].name);
        typeValues.push(data.data[i].name);
        $('#modal_dvc_type_id').append(new Option(data.data[i].name, data.data[i].id,false, false))
      }

      $.getJSON("/api/workplaces", null, function(data) {
        if (data != null) {
          for(var i=0;i< data.data.length; i++){
            workplacesMap.set(data.data[i].id, data.data[i].name);
            workplacesValues.push(data.data[i].name);
            $('#modal_workplaces_id').append(new Option(data.data[i].name, data.data[i].id,false, false))
          }

          $.getJSON("/api/partners", null, function(data) {
            if (data != null) {
              for(var i=0;i< data.data.length; i++){
                ownersMap.set(data.data[i].id, data.data[i].name);
                ownersValues.push(data.data[i].name);
                $('#modal_partners_id').append(new Option(data.data[i].name, data.data[i].id,false, false))
              }

              var table_columns = [
                  {
                    formatter: 'runningFormatter',
                    class: 'min',
                    title: "#",
                  },
                  {
                    field: 'state',
                    checkbox: 'true'
                  },
                  {
                    field: "id",
                    title: "ID",
                    visible: false,
                    class: 'min',
                    sortable: true,
                    // filterControl: 'input',
                    // filterControlPlaceholder: "id",
                    // filterStrictSearch: false
                    filter: {
                      type: "input"
                    }
                  },{
                    field:  "dvc_type_id",
                    title: "Type",
                    sortable:true,
                    formatter: formatType,
                    class: 'min',
                    filter: {
                      type: "select",
                      data: typeValues
                    }
                  },{
                    field:  "name",
                    title: "Name",
                    sortable:true,
                    formatter: formatName,
                    filterStrictSearch: false,
                    filter: {
                      type: "input",
                      filterControlPlaceholder: "Name",
                    }
                  },
                  {
                    field: "workplaces_id",
                    title: "Workplace",
                    formatter: formatWorplace,
                    sortable: true,
                    class: 'min',
                    filterStrictSearch: true,
                    filter: {
                            type: "select",
                            data: workplacesValues
                          }
                  },
                  {
                    field: "inventory_nr",
                    title: "Inventory NR",
                    sortable: true,
                    clas: 'min',
                    filterStrictSearch: false,
                    filter: {
                            type: "input",
                          }
                  },
                  {
                    field: "partners_id",
                    title: "Owner",
                    formatter: formatOwner,
                    sortable: true,
                    class: 'min',
                    visible: false,
                    filterStrictSearch: true,
                    filter: {
                            type: "select",
                            data: ownersValues,
                          }
                  },
                ]



              $table.bootstrapTable({
                  toolbar: "#toolbar",
                  pagination: true,
                  sidePagination: 'client',
                  showColumns: true,
                  showRefresh: true,
                  showFullscreen: true,
                  showExport: true,
                  exportTypes: ['csv', 'excel', 'doc', 'txt','json'],
                  pageList: [10, 25, 50, 100, 'All'],
                  sortable: true,
                  clickToSelect: true,
                  showFooter: false,
                  /* search: true, */
                  filterControl: true,
                  /* disableUnusedSelectOptions: true,  */
                  url: table_source,
                  columns: table_columns,
                  filter: true,
                  // filterTemplate: {
                  //    input: function(bootstrapTable, column, isVisible) {
                  //      return '<input type="text" class="form-control input-sm" data-filter-field="' + column.field + '" style="width: 100%; visibility:' + isVisible + '">';
                  //    }
                  //  }
                });
            }
          });
      }
    });
  }
});
  $.getJSON("/api/cpnt_type", null, function(data) {
    if (data != null) {
      for(var i=0;i< data.data.length; i++){
        typeComponetsMap.set(data.data[i].id, data.data[i].name);
        typeComponetsValues.push(data.data[i].name);
        // $('#modal_dvc_type_id').append(new Option(data.data[i].name, data.data[i].id,false, false))
      }
    }
  });
  $.getJSON("/api/components", null, function(data) {
    if (data != null) {
      for(var i=0;i< data.data.length; i++){
        componetsMap.set(data.data[i].id, data.data[i].name);
        componetsValues.push(data.data[i].name);
        $('#modal_components_id').append(new Option(data.data[i].name, data.data[i].id,false, false))
      }
    }
  });

})


var editMode={
  insertMode: true,
  url: table_source,
  method: 'post',
}

function runningFormatter(value, row, index) {
  return 1+index;
}

function formatType(value,row,index){
  return typesMap.get(value);
}

function formatName(value, row, index){
  return "<a href=/hw/devices/"+row.id+">"+value+"</a>";
}

function formatComponentType(value,row,index){
  return typeComponetsMap.get(value);
}

function formatWorplace(value,row,index){
  return workplacesMap.get(value);
}

function formatOwner(value,row,index){
  return ownersMap.get(value);
}

$('#modalForm').submit(function(e){
      e.preventDefault();
      var url=$(this).closest('form').attr('action'),
      data=$(this).closest('form').serialize();
          // data = {};
          // data.name = $('#modal_name').val();
          // data.cpnt_type_id = $('#modal_cpnt_type_id').val();
          // data.manufacturers_id = $('#modal_manufacturers_id').val();
          // data.price = $('#modal_price').val();
          // data.comments = $('#modal_comments').val();
          // console.log(data);
      $.ajax({
          url:editMode.url,
          type:editMode.method,
          data:data,
          success:function(){
              $('#editModal').modal('hide');
              //createTable();
              $table.bootstrapTable('refresh')
             }
          });
      });

$('#table_add').on('click', function(event) {
  editMode={
    insertMode: true,
    url: table_source,
    method: 'post',
  }
  $('#editModal').modal('show');
});

$('#table_edit').click(function () {
  var checkedRows = getIdChecked($table);
  if (checkedRows.length === 1){
    editMode={
      insertMode: false,
      url: table_source + $table.bootstrapTable('getSelections')[0].id ,
      method: 'put',
    }
    $('#modal_name').val( $table.bootstrapTable('getSelections')[0].name)
    $('#modal_workplaces_id').val( $table.bootstrapTable('getSelections')[0].workplaces_id)
    $('#modal_partners_id').val( $table.bootstrapTable('getSelections')[0].partners_id)
    $('#modal_dvc_type_id').val( $table.bootstrapTable('getSelections')[0].dvc_type_id)
    $('#modal_inventory_nr').val( $table.bootstrapTable('getSelections')[0].inventory_nr)
    $('#modal_workplaces_id').trigger('change')
    $('#modal_partners_id').trigger('change')
    $('#modal_dvc_type_id').trigger('change')
    $('#editModal').modal('show');
  }
});

$('#table_delete').click(function () {
  var checkedRows = getIdChecked($table);
  /* if(getSelectedRow())
    checkedRows.push(getSelectedRow().id) */
  /* checkedRows = unique(checkedRows) */
  if (checkedRows.length  && confirm('Are you sure you want to detele from database?')) {
    for (var i = 0, len = checkedRows.length; i < len; i++) {
      /* console.log("Delete"+checkedRows[i]); */
      $.ajax({
        url: table_source+checkedRows[i],
        type: 'DELETE',
        success: function(){ $table.bootstrapTable('refresh')}
        //error: errorCallback || $.noop
      });
    }
  }
});

 /* function getSelectedRow() {
    var index = $table.find('tr.success').data('index');
    if(index)
      return $table.bootstrapTable('getData')[index];
    else return 0;
}  */
function getIdChecked(selectedTable) {
  var list = [];
  selectedTable.bootstrapTable('getSelections').forEach(function(item){list.push(item.id)})
  return list;
}

function unique(arr) {
  var u = {}, a = [];
  for(var i = 0, l = arr.length; i < l; ++i){
    if(!u.hasOwnProperty(arr[i])) {
      a.push(arr[i]);
      u[arr[i]] = 1;
    }
  }
  return a;
}


var componentsEditMode={
  insertMode: true,
  url: componentsURL+$table.bootstrapTable('getSelections')[0].id,
  method: 'post',
}
$('#modalEditComponentsForm').submit(function(e){
      e.preventDefault();
      var url=$(this).closest('form').attr('action');
      var component_data=$(this).closest('form').serialize();
      component_data = component_data+ '&devices_id=' +  $table.bootstrapTable('getSelections')[0].id;

      $.ajax({
          url:componentsEditMode.url,
          type:componentsEditMode.method,
          data:component_data,
          success:function(){
              $('#editComponentsModal').modal('hide');
              //createTable();
              $componentsTable.bootstrapTable('refresh')
             }
          });
      });



$('#componentsTable_add').on('click', function(event) {
  componentsEditMode={
    insertMode: true,
    url: '/api/devices_components',
    method: 'post',
  }
  $('#editComponentsModal').modal('show');
});

$('#componentsTable_edit').click(function () {
  var checkedRows = getIdChecked($componentsTable);
  if (checkedRows.length === 1){
    componentsEditMode={
      insertMode: false,
      url: '/api/devices_components/' + $componentsTable.bootstrapTable('getSelections')[0].id ,
      method: 'put',
    }
    // $('#modal_name').val( $table.bootstrapTable('getSelections')[0].name)
    $('#modal_components_id').val( $componentsTable.bootstrapTable('getSelections')[0].components_id)
    $('#modal_component_amount').val( $componentsTable.bootstrapTable('getSelections')[0].amount)
    $('#modal_components_serial').val( $componentsTable.bootstrapTable('getSelections')[0].serial_nr)
    $('#modal_components_id').trigger('change')
    $('#editComponentsModal').modal('show');
  }
});

$('#componentsTable_delete').click(function () {
  var checkedRows = getIdChecked($componentsTable);
  /* if(getSelectedRow())
    checkedRows.push(getSelectedRow().id) */
  /* checkedRows = unique(checkedRows) */
  if (checkedRows.length  && confirm('Are you sure you want to detele from database?')) {
    for (var i = 0, len = checkedRows.length; i < len; i++) {
      /* console.log("Delete"+checkedRows[i]); */
      $.ajax({
        url: '/api/devices_components/'+checkedRows[i],
        type: 'DELETE',
        success: function(){ $componentsTable.bootstrapTable('refresh')}
        //error: errorCallback || $.noop
      });
    }
  }
});

$('#table_components').click(showComponents)

function showComponents(){
  var checkedRows = getIdChecked($table);
  if (checkedRows.length === 1){
    $componentsTable.bootstrapTable('destroy');
    $componentsTable.bootstrapTable({
          toolbar: "#componentsToolbar",
          pagination: true,
          sidePagination: 'client',
          showColumns: true,
          showRefresh: true,
          showFullscreen: true,
          showExport: true,
          exportTypes: ['csv', 'excel', 'doc', 'txt','json'],
          pageList: [10, 25, 50, 100, 'All'],
          sortable: true,
          clickToSelect: true,
          showFooter: false,
          /* search: true, */
          filterControl: true,
          /* disableUnusedSelectOptions: true,  */
          url: componentsURL+$table.bootstrapTable('getSelections')[0].id,
          columns: [
              {
                formatter: 'runningFormatter',
                class: 'min',
                title: "#",
              },
              {
                field: 'state',
                checkbox: 'true'
              },
              {
                field: "id",
                title: "ID",
                visible: false,
                class: 'min',
                sortable: true,
                // filterControl: 'input',
                // filterControlPlaceholder: "id",
                // filterStrictSearch: false
                filter: {
                  type: "input"
                }
              },{
                field:  "cpnt_type_id",
                title: "Type",
                sortable:true,
                formatter: formatComponentType,
                filterControlPlaceholder: "Type",
                class: 'min',
                filterStrictSearch: true,
                filter: {
                  type: "select",
                  data: typeComponetsValues
                }
              },{
                field:  "component",
                title: "Component name",
                sortable:true,
                // filterControl: 'input',
                filterControlPlaceholder: "Name",
                // filterStrictSearch: false
                filter: {
                  type: "input",
                  filterControlPlaceholder: "Name",
                }
              },{
                field:  "amount",
                title: "Qtty",
                sortable:true,
                class: 'min',
                // filterControl: 'input',
                filterControlPlaceholder: "Name",
                // filterStrictSearch: false
                filter: {
                  type: "input",
                  filterControlPlaceholder: "Name",
                }
              },{
                field:  "serial_nr",
                title: "SerialNr",
                sortable:true,
                class: 'min',
                // filterControl: 'input',
                filterControlPlaceholder: "Name",
                // filterStrictSearch: false
                filter: {
                  type: "input",
                  filterControlPlaceholder: "Name",
                }
              },
            ],
          filter: true,
          // filterTemplate: {
          //    input: function(bootstrapTable, column, isVisible) {
          //      return '<input type="text" class="form-control input-sm" data-filter-field="' + column.field + '" style="width: 100%; visibility:' + isVisible + '">';
          //    }
          //  }
        }


    );
    $('#ComponentsModal').modal('show');
  }
}


$('#table_barcode').click(printLabels)

function printLabels() {
  var checkedRows = getIdChecked($table);
  $("#barcode").hide();
  // var grid = $("#list");
  // var rowKey = grid.getGridParam("selrow");
  var pdf = new jsPDF({
              orientation: 'landscape',
              unit: 'mm',
              format: [60, 27]
      });
  for (var i = 0, len = checkedRows.length; i < len; i++) {
  // if (rowKey) {
    if (i)
      pdf.addPage();
    // var labelName = grid.jqGrid ('getCell', rowKey, 'name');
    // var labelBarcode = grid.jqGrid ('getCell', rowKey, 'inventory_nr');
    var labelName = $table.bootstrapTable('getSelections')[i].name;
    var labelBarcode = $table.bootstrapTable('getSelections')[i].inventory_nr;

    pdf.rect(2, 1, 55, 25)

    if(labelBarcode){
      JsBarcode("#barcode", labelBarcode,{displayValue:true,fontSize:20,height:20,width:1,textPosition: "top"});
      var jpegUrl = $("#barcode")[0].toDataURL("image/jpeg");
      pdf.addImage(jpegUrl, 'JPEG', 8, 10);
    }
    pdf.text(labelName, 3, 10)
 }
  var string = pdf.output('datauristring');

  var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"
  var x = window.open();
  x.document.open();
  x.document.write(iframe);
x.document.close();

  // else {
  //   alert("No rows are selected");
  // }

}


$('#table_pdf').click( printTable)

function printTable() {
    var pdf = new jsPDF('p', 'pt', 'a4');
    source = $('#dataTable')[0];
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function (element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 522
    };
    var pdfHeader = function(data) {
      pdf.setFontSize(18);
      pdf.setTextColor(40);
      pdf.setFontStyle('normal');
      //doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
      pdf.text(title, data.settings.margin.left, 50);
    };

    var options = {
      beforePageContent: pdfHeader,
      margin: {
        top: 80
      },
      styles: {overflow: 'linebreak', columnWidth: 'wrap'},
      //startY: pdf.autoTableEndPosY() + 80
    };
    var res = pdf.autoTableHtmlToJson(source);
    pdf.autoTable(res.columns, res.data, options);
    var string = pdf.output('datauristring');

      var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"

      var x = window.open();
      x.document.open();
      x.document.write(iframe);
      x.document.close();
    //pdf.save('Test.pdf');
}
