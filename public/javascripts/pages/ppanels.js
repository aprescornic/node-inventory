// $(function () {
$(document).ready(function () {
    "use strict";
    $.jgrid.defaults.styleUI = 'Bootstrap';
    setValues();
});

var SGroups ="";
var $grid = $("#ppanels");
var $grid_sockets = $("#sockets");
var $port_list = $("#port_list");
var sockets_url = "/api/ppanels_sgroups/ppanels_id"
var ports_url =  "/api/port_list/";
var current_component = "0";

function setValues(){
  $.getJSON("/api/sgroups", null, function(data) {
    if (data != null) {
      for(var i=0;i< data.data.length; i++){
        SGroups += data.data[i].id + ":"+ data.data[i].name ;
        if(i<data.data.length-1) SGroups+=";";
      }
      setGridRequirentes();
    }
  });
  // getTypeValues();
}

function clearSelection() {
	jQuery("#sockets").jqGrid('setGridParam',{url: sockets_url+"/0", datatype: 'json'}); // the last setting is for demo purpose only
	jQuery("#sockets").jqGrid('setCaption', 'Sockets :: none');
	jQuery("#sockets").trigger("reloadGrid");			//jQuery("#jqGridDetails")
  current_component = "0"
}


function setGridRequirentes()
{
  //Devices
  $grid.jqGrid({
    // data: mydata,
    url: "/api/ppanels",
	  datatype: "json",
    jsonReader : {
        root: "data",
      },
    loadonce: true,
    // grouping:true,
    // groupingView : {
    //    groupField : ['workplaces_id'],
    //    groupDataSorted : true,
    //    groupColumnShow : [false],
    //    groupOrder: ["asc"]
    // },
    colNames: [ "ID", "Name","Size"],
    colModel: [
      { name: "id", width: 30, template: "integer", editable:false },
      { name: "name", width: 200, align: "left",  editrules: {required: true} },
      { name: "size", width:30, template: "integer", align: "right",},
    ],
    cmTemplate: { editable: true, autoResizable: true },
    guiStyle: "bootstrap",
    iconSet: "fontAwesome",
    rowNum: 10,
    autowidth: true,
    autoResizing: { compact: true },
    autowidth: true,
    rowList: [5, 10, 20, "10000:All"],
    viewrecords: true,
    // width:750,
    // pager: true,
    toppager: true,
    rownumbers: true,
    sortname: "name",
    sortorder: "asc",
    altRows: true,
    searching: {
      defaultSearch: "cn"
    },
    // subGrid: true,
    // subGridRowExpanded: function (subgridDivId, rowId) {
    //   $("#" + $.jgrid.jqID(subgridDivId)).html("<em>simple subgrid data for the row with id=" + rowId + "</em>");
    // },
    // inlineEditing: { keys: true },
    caption: "Patch panels",
    editurl: "/api/ppanels/",
    onSelectRow: function (id, selected) {
        var $this = $(this), gridIdSelector = '#' + $.jgrid.jqID(this.id);
        $this.jqGrid('setGridParam', {
            editurl: "/api/ppanels/"+id
        });
        if (selected) {
            current_component = id;
            jQuery("#sockets").jqGrid('setCaption', 'Sockets  ::'+$this.jqGrid("getLocalRow", current_component).name);
            jQuery("#port_list").jqGrid('setGridParam',{url: ports_url+"panel_id/"+current_component, datatype: 'json'});
            jQuery("#port_list").trigger("reloadGrid");
        }
        else {
          current_component = 0;
          jQuery("#sockets").jqGrid('setCaption', 'Sockets :: none');
          jQuery("#port_list").jqGrid('setGridParam',{url: ports_url, datatype: 'json'});
          jQuery("#port_list").trigger("reloadGrid");
        }
        jQuery("#sockets").jqGrid('setGridParam',{url: sockets_url+"/"+id, datatype: 'json'}); // the last setting is for demo only
				// jQuery("#sockets").jqGrid('setCaption', 'Sockets ::'+$this.jqGrid("getLocalRow", id).name);
				jQuery("#sockets").trigger("reloadGrid");

      },
    onSortCol : clearSelection,
		onPaging : clearSelection,
    gridComplete: initGrid
  }).jqGrid("navGrid",
            { view: false,edit:true,add:true,del:true,search:true },
            {mtype : "PUT",
                    afterSubmit: function (id) {jQuery("#ppanels").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');},
                    beforeShowForm: function ($form) {
                      var groupingView = $(this).jqGrid("getGridParam", "groupingView"), i,
                        l = (groupingView !== null || $.isArray(groupingView.groupField)) ? groupingView.groupField.length : 0;
                      for (i = 0; i < l; i++) {
                        if ($.isArray(groupingView.groupColumnShow) && groupingView.groupColumnShow[i] === false) {
                          $form.find("#tr_" + $.jgrid.jqID(groupingView.groupField[i])).show();
                          $form.find("#tr_" + $.jgrid.jqID(groupingView.groupField[i]))["0"].children["0"].style.visibility = "";
                          $form.find("#tr_" + $.jgrid.jqID(groupingView.groupField[i]))["0"].children["1"].style.visibility = "";
                        }
                      }
                    }
              },
            {url: "/api/ppanels",
                  afterSubmit: function (id) {jQuery("#ppanels").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');},
                  beforeShowForm: function ($form) {
                    var groupingView = $(this).jqGrid("getGridParam", "groupingView"), i,
                      l = (groupingView !== null || $.isArray(groupingView.groupField)) ? groupingView.groupField.length : 0;
                    for (i = 0; i < l; i++) {
                      if ($.isArray(groupingView.groupColumnShow) && groupingView.groupColumnShow[i] === false) {
                        $form.find("#tr_" + $.jgrid.jqID(groupingView.groupField[i])).show();
                        $form.find("#tr_" + $.jgrid.jqID(groupingView.groupField[i]))["0"].children["0"].style.visibility = "";
                        $form.find("#tr_" + $.jgrid.jqID(groupingView.groupField[i]))["0"].children["1"].style.visibility = "";
                      }
                    }
                  }
                },
            {mtype : "DELETE"})
  // .jqGrid("inlineNav")
  .jqGrid("filterToolbar")
  .jqGrid("gridResize")
  .jqGrid("navButtonAdd",
      {caption:"", buttonicon: "fa fa-print fa-2x", title: "Print", onClickButton: function () {PrinTable("ppanels");}
		})
  .jqGrid("navButtonAdd",
      {caption:"", buttonicon: "fa fa-barcode fa-2x", title: "Print Labels", onClickButton: function () {printLabels();}
            });
// });

  //Devices details
  $grid_sockets.jqGrid({
    // data: mydata,
    url: sockets_url+"/0",
	  datatype: "json",
    jsonReader : {
        root: "data",
      },
    loadonce: true,
    colNames: [ "ID", "PPanel", "Panel pos", "Socket Group", "Socket pos"],//"Component Name", "Qtty", "SerialNr"],
    colModel: [
      {name: "id",  hidden: true,},
      {name: "ppanels_id",   hidden: true,  },
      {name: "ppanels_pos",   hidden: false,  },
      // {name: "sgroups_id",   hidden: false,  },
      { name: "sgroups_id", align: "left", editable:true,
                formatter: "select", edittype: "select",
                editoptions: {
                  defaultValue: "1",
                  value: SGroups,
                    dataInit: function(element) {
                        $(element).width(100).select2({
                        });
                    }
                },
                stype: "select",
                searchoptions: {
                    value: ":All;"+SGroups,
                    // defaultValue: "",
                    dataInit: function(element) {
                        $(element).width(100).select2({
                            select2CssClass: "ui-widget ui-jqdialog",
                        });
                    }
                }
      },
      {name: "sgroups_pos",   hidden: false,  },

      //
      // { name: "components_id",  align: "left", editrules: {required: true},
      //           formatter: "select", edittype: "select",
      //           editoptions: {
      //             defaultValue: "1",
      //             value: components,
      //               dataInit: function(element) {
      //                   $(element).width(152).select2({
      //                   });
      //               }
      //           },
      //           stype: "select",
      //           searchoptions: {
      //               value: ":All;"+components,
      //               // defaultValue: "",
      //               dataInit: function(element) {
      //                   $(element).width(152).select2({
      //                       select2CssClass: "ui-widget ui-jqdialog",
      //                   });
      //               }
      //           }
      // },
      // { name: "amount", align: "right", width:40, template: "integer",
      //           editoptions:{
      //                 size: 15, maxlengh: 10, defaultValue: "1",
      //                 dataInit: function(element) {
      //                     $(element).keyup(function(){
      //                         var val1 = element.value;
      //                         var num = new Number(val1);
      //                         if(isNaN(num))
      //                         {alert("Please enter a valid number");}
      //                     })
      //                 }
      //             },
      //           editrules: {required: true} },
      // {name: "serial_nr",   align: "left", },
    ],
    cmTemplate: { editable: true, autoResizable: true },
    guiStyle: "bootstrap",
    iconSet: "fontAwesome",
    rowNum: 10,
    autowidth: true,
    autoResizing: { compact: true },
    autowidth: true,
    rowList: [5, 10, 20, "10000:All"],
    viewrecords: true,
    // width:750,
    // pager: true,
    toppager: true,
    rownumbers: true,
    sortname: "id",
    sortorder: "asc",
    altRows: true,
    searching: {
      defaultSearch: "cn"
    },
    // subGrid: true,
    // subGridRowExpanded: function (subgridDivId, rowId) {
    //   $("#" + $.jgrid.jqID(subgridDivId)).html("<em>simple subgrid data for the row with id=" + rowId + "</em>");
    // },
    // inlineEditing: { keys: true },
    caption: "Sockets :: none",
    editurl: sockets_url+"/",
    onSelectRow: function (id) {
        var $this = $(this), gridIdSelector = '#' + $.jgrid.jqID(this.id);
        $this.jqGrid('setGridParam', {
            editurl: "/api/ppanels_sgroups/"+id
        });
      },
    // gridComplete: initGrid
  }).jqGrid("navGrid",
            { view: false,edit:true,add:true,del:true,search:true },
            {mtype : "PUT",
                afterSubmit: function (id) {jQuery("#sockets").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');}
                // beforeSubmit: function (postdata, formid) {postdata.devices_id = current_component;},
              },
            {url: "/api/ppanels_sgroups",
                  afterSubmit: function (id) {jQuery("#sockets").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');},
                  beforeSubmit: function (postdata, formid) {postdata.ppanels_id = current_component;},
              },
            {mtype : "DELETE"})
  // .jqGrid("inlineNav")
  .jqGrid("filterToolbar")
  .jqGrid("gridResize")
  .jqGrid("navButtonAdd",
      {caption:"", buttonicon: "fa fa-print fa-2x", title: "Print", onClickButton: function () {PrinTable("sockets");}
		});

  //All ports
  $port_list.jqGrid({
    // data: mydata,
    url: ports_url,
    datatype: "json",
    jsonReader : {
        root: "data",
      },
    loadonce: true,
    colNames: ["SGroup", "Socket pos", "Patch Panel", "Panel pos"],//"Component Name", "Qtty", "SerialNr"],
    colModel: [
      {name: "group_name",  align: "left",  },
      {name: "group_port",   template: "integer", align: "right",  },
      // {name: "sgroups_id",   hidden: false,  },
      { name: "panel_name", align: "left",   },
      {name: "panel_port",   template: "integer", align: "right",  },
    ],
    cmTemplate: { editable: false, autoResizable: true },
    guiStyle: "bootstrap",
    iconSet: "fontAwesome",
    rowNum: 10,
    autowidth: true,
    autoResizing: { compact: true },
    autowidth: true,
    rowList: [5, 10, 20, "10000:All"],
    viewrecords: true,
    // width:750,
    // pager: true,
    toppager: true,
    rownumbers: true,
    sortname: "sgroup",
    sortorder: "asc",
    altRows: true,
    searching: {
      defaultSearch: "cn"
    },
    caption: "All ports",
  }).jqGrid("navGrid",{ edit: false, add: false, del: false, search: false, refresh: false})
  // .jqGrid("inlineNav")
  .jqGrid("filterToolbar")
  .jqGrid("gridResize")
  .jqGrid("navButtonAdd",
      {caption:"", buttonicon: "fa fa-print fa-2x", title: "Print", onClickButton: function () {PrinTable("port_list");}
    });

}

function initGrid() {
  $(this).contextMenu('contextMenu', {
    menuStyle :{
      width : "150px"
    },
    bindings: {
      'edit': function (t) {
        editRow();
      },
      'add': function (t) {
        addRow() ;
      },
      'del': function (t) {
        delRow();
      }
    },
    onContextMenu: function (event, menu) {
      var rowId = $(event.target).parent("tr").attr("id")
      var grid = $("#ppanels");
      grid.setSelection(rowId);

      return true;
    }
  });
}

function addRow() {
  var grid = $("#ppanels");
  grid.jqGrid('setGridParam', {
      editurl: "/api/ppanel"
  });
  grid.editGridRow("new",
                  { closeAfterAdd: true,
                    afterSubmit: function (id) {jQuery("#ppanels").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');},
                    beforeShowForm: function ($form) {
                      var groupingView = $(this).jqGrid("getGridParam", "groupingView"), i,
                        l = (groupingView !== null || $.isArray(groupingView.groupField)) ? groupingView.groupField.length : 0;
                      for (i = 0; i < l; i++) {
                        if ($.isArray(groupingView.groupColumnShow) && groupingView.groupColumnShow[i] === false) {
                          $form.find("#tr_" + $.jgrid.jqID(groupingView.groupField[i])).show();
                          $form.find("#tr_" + $.jgrid.jqID(groupingView.groupField[i]))["0"].children["0"].style.visibility = "";
                          $form.find("#tr_" + $.jgrid.jqID(groupingView.groupField[i]))["0"].children["1"].style.visibility = "";
                        }
                      }
                    }
                  })
}

function editRow() {
  var grid = $("#ppanels");
  var rowKey = grid.getGridParam("selrow");
  if (rowKey) {
    grid.editGridRow(rowKey,
                    {closeAfterEdit: true,
                      mtype : "PUT",
                      afterSubmit: function (id) {jQuery("#ppanels").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');} ,
                      beforeShowForm: function ($form) {
                        var groupingView = $(this).jqGrid("getGridParam", "groupingView"), i,
                          l = (groupingView !== null || $.isArray(groupingView.groupField)) ? groupingView.groupField.length : 0;
                        for (i = 0; i < l; i++) {
                          if ($.isArray(groupingView.groupColumnShow) && groupingView.groupColumnShow[i] === false) {
                            $form.find("#tr_" + $.jgrid.jqID(groupingView.groupField[i])).show();
                            $form.find("#tr_" + $.jgrid.jqID(groupingView.groupField[i]))["0"].children["0"].style.visibility = "";
                            $form.find("#tr_" + $.jgrid.jqID(groupingView.groupField[i]))["0"].children["1"].style.visibility = "";
                          }
                        }
                      }
                    });

  }
  else {
    alert("No rows are selected");
  }
}

function delRow() {
  var grid = $("#ppanels");
  var rowKey = grid.getGridParam("selrow");
  if (rowKey) {
    grid.delGridRow(rowKey, {mtype : "DELETE"});
  }
  else {
    alert("No rows are selected");
  }
}


function PrinTable(tableid){
  start=1;
  // if (multiselect===true){start=2};
  var newWin3=window.open('','Print workplaces','');
  newWin3.focus();
  newWin3.document.write('<html>');
  newWin3.document.write("<script>printable=true;\x3C/script>");
  newWin3.document.write($("#idheader").html());
  newWin3.document.write('<body>');
  colNames=jQuery("#"+tableid).jqGrid('getGridParam',"colNames");
  colModel=jQuery("#"+tableid).jqGrid('getGridParam',"colModel");
  dataids=$("#"+tableid).getDataIDs();

  table='<table class="table table-striped table-bordered table-condensed">';
  table=table+'<thead><tr><th></th>';
  for(i=start;i<colModel.length;i++){
    if (colModel[i].hidden==false){
      table=table+'<th>'+colNames[i]+'</th>';
    };
  };
  table=table+'</tr></thead>';
  table=table+ $("#"+tableid)["0"].innerHTML;
  table=table+"</table>";
  newWin3.document.write(table);
  newWin3.document.write('</body></html>');
  newWin3.document.close();
};

function printLabels() {
  var grid = $("#ppanels");
  var rowKey = grid.getGridParam("selrow");
  if (rowKey) {
    celValue = grid.jqGrid ('getCell', rowKey, 'name');
    var pdf = new jsPDF({
                orientation: 'landscape',
                unit: 'mm',
                format: [60, 27]
        });
    pdf.rect(2, 1, 55, 25)
    pdf.line(28,1,28,26)
    pdf.text(celValue, 3, 15)
    pdf.text(celValue, 29, 15)
    var string = pdf.output('datauristring');

      var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"

      var x = window.open();
      x.document.open();
      x.document.write(iframe);
      x.document.close();

 }
  else {
    alert("No rows are selected");
  }

}
