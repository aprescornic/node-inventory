var $detailsTable= $('#detailsTable');
var detailsURL='/api/components/devices/';
// var typeComponetsMap = new Map();

$(document).ready(function () {

  initSelect2Data("#cpnt_type_id","/api/cpnt_type")
  initSelect2Data("#manufacturers_id","/api/manufacturers")
  // initSelect2Data("#partners_id","/api/partners" )

  initPageData();
  initDetailsData();

})

function initPageData(){
  $.getJSON('/api/components/'+item_id).done(
    function(data){
      $("#name").val(data.data.name);
      $("#cpnt_type_id").val(data.data.cpnt_type_id).trigger('change');
      $("#manufacturers_id").val(data.data.manufacturers_id).trigger('change');
      $("#price").val(data.data.price);
      $("#comments").val(data.data.comments);
    }
  )
}

function initDetailsData(){
//
//   initSelect2Data("#modal_components_id","/api/components" , '#editComponentsModal')
//   // $('#componentsTable_delete').prop('disabled', true);
//   // $('#componentsTable_edit').prop('disabled', true);
//
//
//   initSelectFilterData("/api/cpnt_type");
  $detailsTable.bootstrapTable({
//         toolbar: "#componentsToolbar",
//         // pagination: true,
//         /* search: true, */
        filterControl: true,
//         /* disableUnusedSelectOptions: true,  */
        url: detailsURL+item_id,
        customToolbarButtons: [
                {
                  name: "table_pdf",
                  title: print,
                  icon: "glyphicon-print",
                  callback: function(){ printTable($detailsTable)},
                }
            ],
        columns: [
            {
              formatter: 'runningFormatter',
              class: 'min',
              title: "#",
            },
            {
              field: 'state',
              checkbox: 'true',
              class: 'min',
            },
            {
              field: "id",
              title: "ID",
              class: 'min',
              visible: false,
              filter: {
                type: "input"
              }
            },{
              field:  "device",
              title: "Device name",
              formatter: formatDeviceName,
              filter: {
                type: "input",
                filterControlPlaceholder: "Name",
              }
            },{
              field:  "inventory_nr",
              title: "Inv Nr",
              filter: {
                type: "input",
                filterControlPlaceholder: "Inv Nr",
              }
            },{
              field:  "amount",
              title: "Qtty",
              filterStrictSearch: true,
              class: 'min',
              filter: {
                type: "input",
                filterControlPlaceholder: "Qtty",
              }
            },{
              field:  "serial_nr",
              title: "SerialNr",
              class: 'min',
              filter: {
                type: "input",
                filterControlPlaceholder: "Serial",
              }
            },{
              field:  "workplace",
              title: "Workplace",
              class: 'min',
              filter: {
                type: "input",
                filterControlPlaceholder: "Workplace",
              }
            },{
              field:  "employees",
              title: "Employee",
              class: 'min',
              filter: {
                type: "input",
                filterControlPlaceholder: "Employee",
              }
            }
          ],
  });
}

$('#itemForm').submit(function(e){
      e.preventDefault();
      var url=$(this).closest('form').attr('action'),
      data=$(this).closest('form').serialize();
      $.ajax({
          url:'/api/components/'+item_id,
          type:'PUT',
          data:data,
          success:function(){
             }
          });
      });

function initSelectFilterData(url){
  $.getJSON(url).done(
    function( type_data ) {
      if (type_data.data != null) {
        for(var i=0;i< type_data.data.length; i++){
          typeComponetsMap.set(type_data.data[i].id, type_data.data[i].name);
        }
        var select2values = Array.from( typeComponetsMap.values());
        select2values.unshift("");
        $componentsTable.bootstrapTable("setSelect2Data", "cpnt_type_id", select2values);
      }
    })
}

function formatDeviceName(value, row, index){
  return "<a href=/hw/devices/"+row.devices_id+">"+value+"</a>";
}

//
// function formatComponentType(value,row,index){
//   return typeComponetsMap.get(value);
// }
//
// var componentsEditMode={
//   insertMode: true,
//   url: '/api/devices_components/',
//   method: 'post',
// }
//
// $('#modalEditComponentsForm').submit(function(e){
//   e.preventDefault();
//   var url=$(this).closest('form').attr('action');
//   var component_data=$(this).closest('form').serialize();
//   component_data = component_data+ '&devices_id=' +  element_id;
//
//   $.ajax({
//     url:componentsEditMode.url,
//     type:componentsEditMode.method,
//     data:component_data,
//     success:function(){
//       $('#editComponentsModal').modal('hide');
//       //createTable();
//       $componentsTable.bootstrapTable('refresh')
//     }
//   });
// });
//
// $componentsTable.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
//     $('#deleteItem').prop('disabled', !$componentsTable.bootstrapTable('getSelections').length);
//     $('#editItem').prop('disabled', !($componentsTable.bootstrapTable('getSelections').length===1));
//  });
//
// function addComponents(){
//   componentsEditMode={
//     insertMode: true,
//     url: '/api/devices_components/',
//     method: 'post',
//   }
//   $('#editComponentsModal').modal('show');
// }
//
// function editComponents(){
//   var checkedRows = getIdChecked($componentsTable);
//   if (checkedRows.length === 1){
//     componentsEditMode={
//       insertMode: false,
//       url: '/api/devices_components/' + $componentsTable.bootstrapTable('getSelections')[0].id ,
//       method: 'put',
//     }
//     // $('#modal_name').val( $table.bootstrapTable('getSelections')[0].name)
//     $('#modal_components_id').val( $componentsTable.bootstrapTable('getSelections')[0].components_id)
//     $('#modal_component_amount').val( $componentsTable.bootstrapTable('getSelections')[0].amount)
//     $('#modal_components_serial').val( $componentsTable.bootstrapTable('getSelections')[0].serial_nr)
//     $('#modal_components_id').trigger('change')
//     $('#editComponentsModal').modal('show');
//   }
// }
//
// function deleteComponents() {
//   var checkedRows = getIdChecked($componentsTable);
//   /* if(getSelectedRow())
//     checkedRows.push(getSelectedRow().id) */
//   /* checkedRows = unique(checkedRows) */
//   if (checkedRows.length  && confirm('Are you sure you want to detele from database?')) {
//     for (var i = 0, len = checkedRows.length; i < len; i++) {
//       /* console.log("Delete"+checkedRows[i]); */
//       $.ajax({
//         url: '/api/devices_components/'+checkedRows[i],
//         type: 'DELETE',
//         success: function(){ $componentsTable.bootstrapTable('refresh')}
//         //error: errorCallback || $.noop
//       });
//     }
//   }
// };
