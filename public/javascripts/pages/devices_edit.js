var $componentsTable= $('#componentsTable');
var componentsURL='/api/devices/components/';
var typeComponetsMap = new Map();

$(document).ready(function () {

  initSelect2Data("#dvc_type_id","/api/dvc_type")
  initSelect2Data("#workplaces_id","/api/workplaces")
  initSelect2Data("#partners_id","/api/partners" )

  initPageData();
  initComponentsData();

})

function initPageData(){
  $.getJSON('/api/devices/'+element_id).done(
    function(data){
      $("#dvc_type_id").val(data.data.dvc_type_id).trigger('change');
      $("#device_name").val(data.data.name);
      $("#workplaces_id").val(data.data.workplaces_id).trigger('change');
      $("#inventory_nr").val(data.data.inventory_nr);
      $("#partners_id").val(data.data.partners_id).trigger('change');
    }
  )
}
function initComponentsData(){

  initSelect2Data("#modal_components_id","/api/components" , '#editComponentsModal')
  // $('#componentsTable_delete').prop('disabled', true);
  // $('#componentsTable_edit').prop('disabled', true);


  initSelectFilterData("/api/cpnt_type");
  $componentsTable.bootstrapTable({
        toolbar: "#componentsToolbar",
        // pagination: true,
        /* search: true, */
        filterControl: true,
        /* disableUnusedSelectOptions: true,  */
        url: componentsURL+element_id,
        customToolbarButtons: [
                {
                    name: "addItem",
                    title: "Add",
                    icon: "glyphicon-plus",
                    callback: addComponents,
                },
                {
                    name: "editItem",
                    title: "Edit",
                    icon: "glyphicon-edit",
                    callback: editComponents,
                    disabled: true,
                },
                {
                    name: "deleteItem",
                    title: "Delete",
                    icon: "glyphicon-trash",
                    callback: deleteComponents,
                    disabled: true,
                },
                {
                  name: "table_pdf",
                  title: print,
                  icon: "glyphicon-print",
                  callback: function(){ printTable($componentsTable)},
                }
            ],
        columns: [
            {
              formatter: 'runningFormatter',
              class: 'min',
              title: "#",
            },
            {
              field: 'state',
              checkbox: 'true',
              class: 'min',
            },
            {
              field: "id",
              title: "ID",
              class: 'min',
              visible: false,
              filter: {
                type: "input"
              }
            },{
              field:  "cpnt_type_id",
              title: "Type",
              formatter: formatComponentType,
              filterControlPlaceholder: "Type",
              filterStrictSearch: true,
              class: 'min',
              // filterType: 'select',
							// filterSelectData: 'var:typeComponetsMap'
              filter: {
                type: "select",
                // data: Array.from( typeComponetsMap.values()),
                data: [],
              }
            },{
              field:  "component",
              title: "Component name",
              formatter : formatComponentName,
              filter: {
                type: "input",
                filterControlPlaceholder: "Name",
              }
            },{
              field:  "amount",
              title: "Qtty",
              filterStrictSearch: true,
              class: 'min',
              filter: {
                type: "input",
                filterControlPlaceholder: "Qtty",
              }
            },{
              field:  "serial_nr",
              title: "SerialNr",
              class: 'min',
              filter: {
                type: "input",
                filterControlPlaceholder: "Serial",
              }
            },
          ],
        // filterTemplate: {
        //    input: function(bootstrapTable, column, isVisible) {
        //      return '<input type="text" class="form-control input-sm" data-filter-field="' + column.field + '" style="width: 100%; visibility:' + isVisible + '">';
        //    }
        //  }
      }
  );
}

$('#deviceForm').submit(function(e){
      e.preventDefault();
      var url=$(this).closest('form').attr('action'),
      data=$(this).closest('form').serialize();
          // data = {};
          // data.name = $('#modal_name').val();
          // data.cpnt_type_id = $('#modal_cpnt_type_id').val();
          // data.manufacturers_id = $('#modal_manufacturers_id').val();
          // data.price = $('#modal_price').val();
          // data.comments = $('#modal_comments').val();
          // console.log(data);
      $.ajax({
          url:'/api/devices/'+element_id,
          type:'PUT',
          data:data,
          success:function(){
              // $('#editModal').modal('hide');
              // //createTable();
              // $table.bootstrapTable('refresh')
             }
          });
      });

function initSelectFilterData(url){
  $.getJSON(url).done(
    function( type_data ) {
      if (type_data.data != null) {
        for(var i=0;i< type_data.data.length; i++){
          typeComponetsMap.set(type_data.data[i].id, type_data.data[i].name);
        }
        var select2values = Array.from( typeComponetsMap.values());
        select2values.unshift("");
        $componentsTable.bootstrapTable("setSelect2Data", "cpnt_type_id", select2values);
      }
    })
}

function formatComponentType(value,row,index){
  return typeComponetsMap.get(value);
}

function formatComponentName(value, row, index){
  return "<a href=/hw/components/"+row.components_id+">"+value+"</a>";
}

var componentsEditMode={
  insertMode: true,
  url: '/api/devices_components/',
  method: 'post',
}

$('#modalEditComponentsForm').submit(function(e){
  e.preventDefault();
  var url=$(this).closest('form').attr('action');
  var component_data=$(this).closest('form').serialize();
  component_data = component_data+ '&devices_id=' +  element_id;

  $.ajax({
    url:componentsEditMode.url,
    type:componentsEditMode.method,
    data:component_data,
    success:function(){
      $('#editComponentsModal').modal('hide');
      //createTable();
      $componentsTable.bootstrapTable('refresh')
    }
  });
});

$componentsTable.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
    $('#deleteItem').prop('disabled', !$componentsTable.bootstrapTable('getSelections').length);
    $('#editItem').prop('disabled', !($componentsTable.bootstrapTable('getSelections').length===1));
 });

function addComponents(){
  componentsEditMode={
    insertMode: true,
    url: '/api/devices_components/',
    method: 'post',
  }
  $('#editComponentsModal').modal('show');
}

function editComponents(){
  var checkedRows = getIdChecked($componentsTable);
  if (checkedRows.length === 1){
    componentsEditMode={
      insertMode: false,
      url: '/api/devices_components/' + $componentsTable.bootstrapTable('getSelections')[0].id ,
      method: 'put',
    }
    // $('#modal_name').val( $table.bootstrapTable('getSelections')[0].name)
    $('#modal_components_id').val( $componentsTable.bootstrapTable('getSelections')[0].components_id)
    $('#modal_component_amount').val( $componentsTable.bootstrapTable('getSelections')[0].amount)
    $('#modal_components_serial').val( $componentsTable.bootstrapTable('getSelections')[0].serial_nr)
    $('#modal_components_id').trigger('change')
    $('#editComponentsModal').modal('show');
  }
}

function deleteComponents() {
  var checkedRows = getIdChecked($componentsTable);
  /* if(getSelectedRow())
    checkedRows.push(getSelectedRow().id) */
  /* checkedRows = unique(checkedRows) */
  if (checkedRows.length  && confirm('Are you sure you want to detele from database?')) {
    for (var i = 0, len = checkedRows.length; i < len; i++) {
      /* console.log("Delete"+checkedRows[i]); */
      $.ajax({
        url: '/api/devices_components/'+checkedRows[i],
        type: 'DELETE',
        success: function(){ $componentsTable.bootstrapTable('refresh')}
        //error: errorCallback || $.noop
      });
    }
  }
};
