// $(function () {
$(document).ready(function () {
    "use strict";
    $.jgrid.defaults.styleUI = 'Bootstrap';

    $.getJSON("/api/employees", null, function(data) {
      if (data != null) {
        for(var i=0;i< data.data.length; i++){
          aemployees.push({id : data.data[i].id, text : data.data[i].name})
        }
        $('#iname').select2({
          data: aemployees,
        });
      }
    });
    setGridRequirentes();
    // $('#iname').on("select2:selecting", function(e) {
    //   // $('#generate').prop('disabled', false);
    //   console.log("selected "+  e)
    // });

});

// $('.name').select2({
//   ajax: {
//     url: '/api/employees',
//     dataType: 'json'
//     processResults: function (data) {
//       // Tranforms the top-level key of the response object from 'items' to 'results'
//       return {
//         results: data.data
//       };
//     }
//   }
// });

//
var workplaces="";
var employees="{ results: [ ";
var aemployees = [];
var types ="";
var components="";
var partners="";
var device_type="";
var $grid = $("#list");
var $grid_details = $("#list_details");
var details_url =  "/api/workplaces/employees/";
var current_component = "0";

function generateRepport(){
  ReloadTable();
  console.log($('#iname').val())
  // details_url = "/api/workplaces/employees/" + $('#iname').val()
  //     $.getJSON("/api/cpnt_type", null, function(data) {
  //       if (data != null) {
  //         for(var i=0;i< data.data.length; i++){
  //           types += data.data[i].id + ":"+ data.data[i].name ;
  //           if(i<data.data.length-1) types+=";";
  //         }
  //         $.getJSON("/api/components", null, function(data) {
  //           if (data != null) {
  //             for(var i=0;i< data.data.length; i++){
  //               components += data.data[i].id + ":"+ data.data[i].name ;
  //               if(i<data.data.length-1) components+=";";
  //             }
  //             $.getJSON("/api/partners", null, function(data) {
  //               if (data != null) {
  //                 for(var i=0;i< data.data.length; i++){
  //                   partners += data.data[i].id + ":"+ data.data[i].name ;
  //                   if(i<data.data.length-1) partners+=";";
  //                 }
  //                 $.getJSON("/api/dvc_type", null, function(data) {
  //                   if (data != null) {
  //                     for(var i=0;i< data.data.length; i++){
  //                       device_type += data.data[i].id + ":"+ data.data[i].name ;
  //                       if(i<data.data.length-1) device_type+=";";
  //                     }

  //                   }
  //                 });
  //               }
  //             });
  //           }
  //         });
  //       }
  //     });
  //   }
  // });
  // getTypeValues();
}

function ReloadTable() {
  details_url = "/api/workplaces/employees/" + $('#iname').val()
	jQuery("#list").jqGrid('setGridParam',{url: details_url, datatype: 'json'}); // the last setting is for demo purpose only
	// jQuery("#list").jqGrid('setCaption', 'Components :: none');
	jQuery("#list").trigger("reloadGrid");			//jQuery("#jqGridDetails")
  current_component = "0"
}


function setGridRequirentes()
{
  //Devices
  $grid.jqGrid({
    // data: mydata,
    url: details_url,
	  datatype: "json",
    jsonReader : {
        root: "data",
      },
    loadonce: true,
    grouping:true,
    groupingView : {
       groupField : ['workplace', "device"],
       groupDataSorted : true,
       groupColumnShow : [false, false],
       groupOrder: ["asc", "asc"]
    },
    colNames: [ "Workplace", "wp type", "Room","Phone", "IP", "Comments", "Dvc type", "Device", "Inv Nr", "Owner","Comp Type", "Component", "Qtty", "Serial Nr"],
    colModel: [
      { name: "workplace", align:"left", },
      {name: "wp_type", align: "left", },
      { name: "room", align: "left",},
      {name: "phone", align: "left", },
      { name: "ip", width:70, align: "left",},
      {name: "wp_comments", align: "left", },
      {name: "dvc_type", align: "left", },
      {name: "device", align: "left", },
      {name: "inventory_nr", align: "left", },
      {name: "partners_owner", align: "left", },
      {name: "cpn_type", align: "left", },
      {name: "component", align: "left", },
      {name: "qtty", align: "right", },
      {name: "serial_nr", align: "left", },
    ],
    cmTemplate: { editable: true, autoResizable: true },
    guiStyle: "bootstrap",
    iconSet: "fontAwesome",
    rowNum: 10,
    autowidth: true,
    autoResizing: { compact: true },
    autowidth: true,
    rowList: [5, 10, 20, "10000:All"],
    viewrecords: true,
    // width:750,
    // pager: true,
    toppager: true,
    rownumbers: true,
    sortname: "workplace",
    sortorder: "asc",
    altRows: true,
    // beforeProcessing: function (data) {
    //     var rows = data.table.rows, length = rows.length, i = 0, row;
    //     data.page = 1;
    //     data.total = 1;
    //     data.records = length;
    //     data.rows = [];
    //     for (; i < length; i += 1) {
    //         row = rows[i];
    //         data.rows.push({
    //             id: row[0],
    //             cell: [row[1]+row[2], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10],row[11],row[12],row[13],row[14]]
    //         });
    //     }
    //     delete data.table;
    // },
    searching: {
      defaultSearch: "cn"
    },
    caption: "Devices",
  }).jqGrid("navGrid",
            { view: false,edit:false,add:false,del:false,search:true },
            )
  // .jqGrid("inlineNav")
  .jqGrid("filterToolbar")
  .jqGrid("gridResize")
  .jqGrid("navButtonAdd",
      {caption:"", buttonicon: "fa fa-print fa-2x", title: "Print", onClickButton: function () {PrinTable("list");}
		});
// });


}


function PrinTable(tableid){
  start=1;
  // if (multiselect===true){start=2};
  var newWin3=window.open('','Print workplaces','');
  newWin3.focus();
  newWin3.document.write('<html>');
  newWin3.document.write("<script>printable=true;\x3C/script>");
  newWin3.document.write($("#idheader").html());
  newWin3.document.write('<body>');
  colNames=jQuery("#"+tableid).jqGrid('getGridParam',"colNames");
  colModel=jQuery("#"+tableid).jqGrid('getGridParam',"colModel");
  dataids=$("#"+tableid).getDataIDs();

  table='<table class="table table-striped table-bordered table-condensed">';
  table=table+'<thead><tr><th></th>';
  for(i=start;i<colModel.length;i++){
    if (colModel[i].hidden==false){
      table=table+'<th>'+colNames[i]+'</th>';
    };
  };
  table=table+'</tr></thead>';
  table=table+ $("#"+tableid)["0"].innerHTML;
  table=table+"</table>";
  newWin3.document.write(table);
  newWin3.document.write('</body></html>');
  newWin3.document.close();
};
