mxBasePath = '/vendor/mxgraph/javascript/src/';

// var toolbar = document.getElementById('toolbarContainer')

$(document).ready(function () {
  initMap(document.getElementById('graphContainer'))
})

function initMap(container)
{
	if (!mxClient.isBrowserSupported())
	{
		mxUtils.error('Browser is not supported!', 200, false);
	}
	else
	{
		mxEvent.disableContextMenu(container);
	  var graph = new mxGraph(container);
    graph.setHtmlLabels(true)

		new mxRubberband(graph);
		var parent = graph.getDefaultParent();

		graph.getModel().beginUpdate();
		try
		{
       $.getJSON('/api/wp_map/')
       .done(function(response){
          for(var i=0;i< response.data.length; i++){
            var v1 = graph.insertVertex(parent, null, response.data[i].workplaces_id, response.data[i].x_pos, response.data[i].y_pos, response.data[i].x_size, response.data[i].y_size,'defaultVertex;fillColor=blue;movable=0;resizable=0;');
          }
        })
		}
		finally
		{
			// Updates the display
			graph.getModel().endUpdate();
		}
    //
    // // Adds a button to execute the layout
		// toolbar.appendChild(mxUtils.button('Arrange',function(evt)
		// {
    //   var preview = new mxPrintPreview(graph, 1);
    //   preview.printBackgroundImage = true;
		// 			preview.open();
		// }));

    // addToolbarButton(graph, toolbar, 'print', 'Print', 'images/printer.png');
    // var content = document.createElement('div');
		// content.style.padding = '4px';
    //
		// var tb = new mxToolbar(content);
    // tb.addItem('Print', 'images/print32.png',function(evt)
		// 		{
		// 			var preview = new mxPrintPreview(graph, 1);
		// 			preview.open();
		// 		});
    // wnd = new mxWindow('Tools', content, 100, 100, 200, 66, false);
		// 		wnd.setMaximizable(false);
		// 		wnd.setScrollable(false);
		// 		wnd.setResizable(false);
		// 		wnd.setVisible(true);
	}
};
//
// function addToolbarButton(editor, toolbar, action, label, image, isTransparent)
// 		{
// 			var button = document.createElement('button');
// 			button.style.fontSize = '10';
// 			if (image != null)
// 			{
// 				var img = document.createElement('img');
// 				img.setAttribute('src', image);
// 				img.style.width = '16px';
// 				img.style.height = '16px';
// 				img.style.verticalAlign = 'middle';
// 				img.style.marginRight = '2px';
// 				button.appendChild(img);
// 			}
// 			if (isTransparent)
// 			{
// 				button.style.background = 'transparent';
// 				button.style.color = '#FFFFFF';
// 				button.style.border = 'none';
// 			}
// 			mxEvent.addListener(button, 'click', function(evt)
// 			{
// 				editor.execute(action);
// 			});
// 			mxUtils.write(button, label);
// 			toolbar.appendChild(button);
// 		};

$( "#printMap" ).click(function() {
           // var divToPrint = document.getElementById('mainGraphWrapper');
           // var popupWin = window.open('', '_blank', 'width=1300,height=1204');
           var content = document.getElementById('mainGraphWrapper').innerHTML;
    var mywindow = window.open('', 'Print', 'width=1300,height=1804');

    mywindow.document.write('<html><head><title>Print</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write(content);
    mywindow.document.write('</body></html>');

    mywindow.document.close();
    mywindow.focus()
    // mywindow.print();
    // mywindow.close();
    return true;
          })
