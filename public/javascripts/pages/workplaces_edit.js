var $detailsTable= $('#detailsTable');
var detailsURL='/api/workplaces/devices/';
// var typeComponetsMap = new Map();
var workplace = {};
mxBasePath = '../vendor/mxgraph/javascript/src/';

$(document).ready(function () {

  initSelect2Data("#wp_type_id","/api/wp_type");
  initSelect2Data("#departments_id","/api/departments");
  initSelect2Data("#employees_id","/api/employees" );

  initPageData();
  initDetailsData();

})

function initPageData(){
  $.getJSON('/api/workplaces/'+item_id).done(
    function(data){
      workplace.id = item_id;
      workplace.name = data.data.name;
      $("#name").val(data.data.name);
      $("#wp_type_id").val(data.data.wp_type_id).trigger('change');
      $("#departments_id").val(data.data.departments_id).trigger('change');
      $("#employees_id").val(data.data.employees_id).trigger('change');
      $("#room").val(data.data.room);
      $("#ip").val(data.data.ip);
      $("#phone").val(data.data.phone);
      $("#comments").val(data.data.comments);
      initMap(document.getElementById('graphContainer'))
    }
  )
}

function initDetailsData(){
//
//   initSelect2Data("#modal_components_id","/api/components" , '#editComponentsModal')
//   // $('#componentsTable_delete').prop('disabled', true);
//   // $('#componentsTable_edit').prop('disabled', true);
//
//
//   initSelectFilterData("/api/cpnt_type");
  $detailsTable.bootstrapTable({
//         toolbar: "#componentsToolbar",
//         // pagination: true,
//         /* search: true, */
        filterControl: true,
//         /* disableUnusedSelectOptions: true,  */
        groupBy: true,
        groupByField: 'device',
        url: detailsURL+item_id,
        customToolbarButtons: [
                {
                  name: "table_pdf",
                  title: print,
                  icon: "glyphicon-print",
                  callback: function(){ printTable($detailsTable)},
                }
            ],
        columns: [
            {
              field: 'state',
              checkbox: 'true',
              class: 'min',
            },
            {
              formatter: 'runningFormatter',
              class: 'min',
              title: "#",
            },
            {
              field: "id",
              title: "ID",
              class: 'min',
              visible: false,
              filter: {
                type: "input"
              }
            },{
              field:  "dvc_type",
              title: "Device Type",
              visible: false,
              filter: {
                type: "input",
                filterControlPlaceholder: "type",
              }
            },{
              field:  "device",
              title: "Device name",
              formatter: formatDeviceName,
              // visible: false,
              filter: {
                type: "input",
                filterControlPlaceholder: "Name",
              }
            },{
              field:  "cpnt_type",
              title: "Component Type",
              filterStrictSearch: true,
              class: 'min',
              filter: {
                type: "input",
                filterControlPlaceholder: "type",
              }
            },{
              field:  "component",
              title: "Component",
              class: 'min',
              filter: {
                type: "input",
                filterControlPlaceholder: "Component",
              }
            },{
              field:  "qtty",
              title: "Qtty",
              class: 'min',
              filter: {
                type: "input",
                filterControlPlaceholder: "Qtty",
              }
            },{
              field:  "sn",
              title: "SerialNr",
              class: 'min',
              filter: {
                type: "input",
                filterControlPlaceholder: "SerialNr",
              }
            }
          ],
  });
}

$('#itemForm').submit(function(e){
      e.preventDefault();
      var url=$(this).closest('form').attr('action'),
      data=$(this).closest('form').serialize();
      $.ajax({
          url:'/api/components/'+item_id,
          type:'PUT',
          data:data,
          success:function(){
             }
          });
      });

function initSelectFilterData(url){
  $.getJSON(url).done(
    function( type_data ) {
      if (type_data.data != null) {
        for(var i=0;i< type_data.data.length; i++){
          typeComponetsMap.set(type_data.data[i].id, type_data.data[i].name);
        }
        var select2values = Array.from( typeComponetsMap.values());
        select2values.unshift("");
        $componentsTable.bootstrapTable("setSelect2Data", "cpnt_type_id", select2values);
      }
    })
}

function formatDeviceName(value, row, index){
  return "<a href=/hw/devices/"+row.devices_id+">"+row.dvc_type +'/'+value+"</a>";
}

$( "#saveMap" ).click(function() {
  // workplace.vertex.geometry mxGeometry {x: 80, y: 150, width: 20, height: 30, relative: false, …}
  $.ajax({
      url:'/api/wp_map/'+workplace.id,
      type:'PUT',
      data:{
        workplaces_id: workplace.id,
        x_pos: workplace.vertex.geometry.x,
        y_pos: workplace.vertex.geometry.y,
        x_size: workplace.vertex.geometry.width,
        y_size: workplace.vertex.geometry.height,
      },
      success:function(){
        alert( "Save Map position done" );
         }
      });

});



// Program starts here. Creates a sample graph in the
// DOM node with the specified ID. This function is invoked
// from the onLoad event handler of the document (see below).
function initMap(container)
{
	// Checks if the browser is supported
	if (!mxClient.isBrowserSupported())
	{
		// Displays an error message if the browser is not supported.
		mxUtils.error('Browser is not supported!', 200, false);
	}
	else
	{
		// Disables the built-in context menu
		mxEvent.disableContextMenu(container);

		// Creates the graph inside the given container
		var graph = new mxGraph(container);
    graph.setHtmlLabels(true)

		// Enables rubberband selection
		new mxRubberband(graph);

		// Gets the default parent for inserting new cells. This
		// is normally the first child of the root (ie. layer 0).
		var parent = graph.getDefaultParent();

		// Adds cells to the model in a single step
		graph.getModel().beginUpdate();
		try
		{
      $.getJSON('/api/wp_map/'+workplace.id)
      .done(function(response){
            workplace.vertex = graph.insertVertex(parent, null, workplace.name, response.data.x_pos, response.data.y_pos, response.data.x_size, response.data.y_size,'defaultVertex;fillColor=red');
        })
      .fail(function() {
        workplace.vertex = graph.insertVertex(parent, null, workplace.name, 20, 20, 20, 30,'defaultVertex;fillColor=red');
       })

       $.getJSON('/api/wp_map/')
       .done(function(response){
          for(var i=0;i< response.data.length; i++){
            if(response.data[i].workplaces_id != workplace.id)
              var v1 = graph.insertVertex(parent, null, response.data[i].workplaces_id, response.data[i].x_pos, response.data[i].y_pos, response.data[i].x_size, response.data[i].y_size,'defaultVertex;fillColor=blue;movable=0;resizable=0;');
          }
             // workplace.vertex = graph.insertVertex(parent, null, workplace.name, response.data.x_pos, response.data.y_pos, response.data.x_size, response.data.y_size,'defaultVertex;fillColor=red');
         })
      // var e1 = graph.insertEdge(parent, null, '', v1, v2);
		}
		finally
		{
			// Updates the display
			graph.getModel().endUpdate();
		}
	}
};
