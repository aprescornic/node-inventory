

$(document).ready(function () {
    "use strict";
    setSettingsValues();
});
var setting_values;

var wp_type="";

function setSettingsValues(){

  $.getJSON("/api/configs", function(json){
      setting_values = json;
      console.log("Set default values");
      $("#company_name").val(getSettingsValue('company_name'));
      $("#admin_username").val(getSettingsValue('admin_username'));
      $("#admin_password").val(getSettingsValue('admin_password'));
      $("#show_column_waranty").prop('checked', (getSettingsValue('show_column_waranty') == '1'));
      $("#show_column_serial").prop('checked',(getSettingsValue('show_column_serial') == '1'));
      $("#show_column_state").prop('checked',(getSettingsValue('show_column_state') == '1'));
      $("#show_column_country").prop('checked',(getSettingsValue('show_column_country') == '1'));
      $("#doc_prefix").val(getSettingsValue('doc_prefix'));
      $("#unique_ip").prop('checked', (getSettingsValue('unique_ip') == '1'));
      $("#default_start_page").val(getSettingsValue('default_start_page'));

      $.getJSON("/api/wp_type", null, function(data) {
        if (data != null) {
          for(var i=0;i< data.data.length; i++){
            wp_type += data.data[i].id + ":"+ data.data[i].name ;
            if(i<data.data.length-1) wp_type+=";";
          }
        }
      })


      // var x = document.getElementById("#company_name").rows[0].cells;
      // x[0].innerHTML = "NEW CONTENT";
  });
}


function getSettingsValue(name){
  return setting_values.data.find(item => {return item.name == name}).value
}

function getSettingsDescription(name){
  return setting_values.data.find(item => {return item.name == name}).description
}
