var $table = $('#dataTable');

var table_columns = [
    {
      formatter: 'runningFormatter',
      class: 'min',
      title: "#",
    },
    {
      field: 'state',
      checkbox: 'true'
    },
    {
      field: "id",
      title: "ID",
      visible: false,
      class: 'min',
      sortable: true,
      filterControl: 'input',
      filterControlPlaceholder: "id",
      filterStrictSearch: false
    },{
      field:  "name",
      title: "Name",
      sortable:true,
      filterControl: 'input',
      filterControlPlaceholder: "Name",
      filterStrictSearch: false
    }
  ]

$(document).ready(function () {
  $table.bootstrapTable({
      toolbar: "#toolbar",
      pagination: true,
      sidePagination: 'client',
      showColumns: true,
      showRefresh: true,
      showFullscreen: true,
      showExport: true,
      exportTypes: ['csv', 'excel', 'doc', 'txt','json'],

      sortable: true,
      clickToSelect: true,
      showFooter: false,
      /* search: true, */
      filterControl: true,
      /* disableUnusedSelectOptions: true,  */
      url: table_source,
      columns: table_columns
    });
})
var editMode={
  insertMode: true,
  url: table_source,
  method: 'post',
}

$('#modalForm').submit(function(e){
      e.preventDefault();
      var url=$(this).closest('form').attr('action'),
          data=$(this).closest('form').serialize();
      $.ajax({
          url:editMode.url,
          type:editMode.method,
          data:data,
          success:function(){
              $('#editModal').modal('hide');
              //createTable();
              $table.bootstrapTable('refresh')
             }
          });
      });

/* $table.on('click-row.bs.table', function (e, row, $element) {
  if($($element).hasClass('success')) {
    $('.success').removeClass('success');
  }else{
    $('.success').removeClass('success');
    $($element).addClass('success');
  }
}); */

function runningFormatter(value, row, index) {
  return 1+index;
}


$('#table_add').on('click', function(event) {
  editMode={
    insertMode: true,
    url: table_source,
    method: 'post',
  }
  $('#editModal').modal('show');
});

$('#table_edit').click(function () {
  var checkedRows = getIdChecked();
  if (checkedRows.length === 1){
    editMode={
      insertMode: false,
      url: table_source + $table.bootstrapTable('getSelections')[0].id ,
      method: 'put',
    }
    $('#modal_name').val( $table.bootstrapTable('getSelections')[0].name)
    //alert('Edit row: ' + $table.bootstrapTable('getSelections')[0].id);

    $('#editModal').modal('show');
  }
});

$('#table_delete').click(function () {
  var checkedRows = getIdChecked();
  /* if(getSelectedRow())
    checkedRows.push(getSelectedRow().id) */
  /* checkedRows = unique(checkedRows) */
  if (checkedRows.length  && confirm('Are you sure you want to detele from database?')) {
    for (var i = 0, len = checkedRows.length; i < len; i++) {
      /* console.log("Delete"+checkedRows[i]); */
      $.ajax({
        url: table_source+checkedRows[i],
        type: 'DELETE',
        success: function(){ $table.bootstrapTable('refresh')}
        //error: errorCallback || $.noop
      });
    }
  }
});

 /* function getSelectedRow() {
    var index = $table.find('tr.success').data('index');
    if(index)
      return $table.bootstrapTable('getData')[index];
    else return 0;
}  */
function getIdChecked() {
  var list = [];
  $table.bootstrapTable('getSelections').forEach(function(item){list.push(item.id)})
  return list;
}

function unique(arr) {
  var u = {}, a = [];
  for(var i = 0, l = arr.length; i < l; ++i){
    if(!u.hasOwnProperty(arr[i])) {
      a.push(arr[i]);
      u[arr[i]] = 1;
    }
  }
  return a;
}

$('#table_pdf').click(function () {
    var pdf = new jsPDF('p', 'pt', 'a4');
    source = $('#dataTable')[0];
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function (element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 522
    };
    var pdfHeader = function(data) {
      pdf.setFontSize(18);
      pdf.setTextColor(40);
      pdf.setFontStyle('normal');
      //doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
      pdf.text(title, data.settings.margin.left, 50);
    };

    var options = {
      beforePageContent: pdfHeader,
      margin: {
        top: 80
      },
      styles: {overflow: 'linebreak', columnWidth: 'wrap'},
      //startY: pdf.autoTableEndPosY() + 80
    };
    var res = pdf.autoTableHtmlToJson(source);
    pdf.autoTable(res.columns, res.data, options);
    var string = pdf.output('datauristring');

      var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"

      var x = window.open();
      x.document.open();
      x.document.write(iframe);
      x.document.close();
    //pdf.save('Test.pdf');
})
