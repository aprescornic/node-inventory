const wp_type = require('../models').wp_type;

module.exports = {
  list() {
    return new Promise(function(resolve, reject) {
    wp_type
      .findAll({
        order: [
          ['name', 'DESC'],
        ],
      })
      .then((wp_type) => resolve(wp_type))
      .catch((error) => { reject(error); });
    })
  },

  getById(id) {
    return new Promise(function(resolve, reject) {
    wp_type
      .findById(id, {})
      .then((wp_type) => {
        if (!wp_type) {
          return reject({error: 'Reccord Not Found',});
        }
        resolve(wp_type);
      })
      .catch((error) => reject(error));
    });
  },

  add(body) {
    return new Promise(function(resolve, reject) {
    wp_type
      .create({
        name: body.name.trim(),
      })
      .then((wp_type) => resolve(wp_type))
      .catch((error) => reject(error));
    });
  },

  update(id, body) {
    return new Promise(function(resolve, reject) {
    wp_type
      .findById(id, {})
      .then(wp_type => {
        if (!wp_type) {
          return reject({message: 'Reccord Not Found',});
        }
        return wp_type
          .update({
            name: body.name.trim() ,
          })
          .then(() => resolve(wp_type))
          .catch((error) => reject(error));
      })
      .catch((error) => reject(error));
    });
  },

  delete(id) {
    return new Promise(function(resolve, reject) {
      wp_type.findById(id)
      .then(wp_type => {
        if (!wp_type) {
          return reject({message: 'Reccord Not Found',});
        }
        return wp_type
          .destroy()
          .then(() => resolve(id))
          .catch((error) => reject(error));
      })
      .catch((error) => reject(error));
    });
  },

  // addWithCourse(body) {
  //   return new Promise(function(resolve, reject) {
  //     Lecturer
  //     .create({
  //       lecturer_name: body.lecturer_name,
  //       course: body.course
  //     }, {
  //       include: [{
  //         model: Course,
  //         as: 'course'
  //       }]
  //     })
  //     .then((lecturer) => resolve(lecturer))
  //     .catch((error) => reject(error));
  //   });
  // },
};
