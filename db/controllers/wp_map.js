const wp_map = require('../models').wp_map;

module.exports = {
  list() {
    return new Promise(function(resolve, reject) {
    wp_map
      .findAll({
        order: [
          ['workplaces_id', 'DESC'],
        ],
      })
      .then((wp_map) => resolve(wp_map))
      .catch((error) => { reject(error); });
    })
  },

  getById(id) {
    return new Promise(function(resolve, reject) {
    wp_map
      // .findById(id, {})
      .findOne({ where: {workplaces_id: id} })
      .then((wp_map) => {
        if (!wp_map) {
          return reject({error: 'Reccord Not Found',});
        }
        resolve(wp_map);
      })
      .catch((error) => reject(error));
    });
  },

  add(body) {
    return new Promise(function(resolve, reject) {
    wp_map
      .create({
        workplaces_id: body.workplaces_id,
        x_pos: body.x_pos,
        y_pos: body.y_pos,
        x_size: body.x_size,
        y_size: body.y_size,
      })
      .then((wp_map) => resolve(wp_map))
      .catch((error) => reject(error));
    });
  },

//Update or create
  update(id, body) {
    return new Promise(function(resolve, reject) {
    wp_map
      // .findById(id, {})
      .findOne({ where: {workplaces_id: id} })
      .then(wp_map_toUpdate => {
        if (!wp_map_toUpdate) {
          // return reject({message: 'Reccord Not Found',});
          return wp_map.create({
              workplaces_id: body.workplaces_id,
              x_pos: body.x_pos,
              y_pos: body.y_pos,
              x_size: body.x_size,
              y_size: body.y_size,
            })
            .then((wp_map) => resolve(wp_map))
            .catch((error) => reject(error));
        }
        return wp_map_toUpdate
          .update({
            workplaces_id: body.workplaces_id,
            x_pos: body.x_pos,
            y_pos: body.y_pos,
            x_size: body.x_size,
            y_size: body.y_size,
          })
          .then(() => resolve(wp_map_toUpdate))
          .catch((error) => reject(error));
      })
      .catch((error) => reject(error));
    });
  },

  delete(id) {
    return new Promise(function(resolve, reject) {
      // wp_map.findById(id)
      wp_map.findOne({ where: {workplaces_id: id} })
      .then(wp_map => {
        if (!wp_map) {
          return reject({message: 'Reccord Not Found',});
        }
        return wp_map
          .destroy()
          .then(() => resolve(id))
          .catch((error) => reject(error));
      })
      .catch((error) => reject(error));
    });
  },

  // addWithCourse(body) {
  //   return new Promise(function(resolve, reject) {
  //     Lecturer
  //     .create({
  //       lecturer_name: body.lecturer_name,
  //       course: body.course
  //     }, {
  //       include: [{
  //         model: Course,
  //         as: 'course'
  //       }]
  //     })
  //     .then((lecturer) => resolve(lecturer))
  //     .catch((error) => reject(error));
  //   });
  // },
};
