'use strict';

// Renaming and exporting all repository classes:

module.exports = {
    // Users: require('./users'),
    Collected: require('./collected'),
    Configs: require('./configs'),
    Components: require('./components'),
    Cpnt_status: require('./cpnt_status'),
    Cpnt_type: require('./cpnt_type'),
    Departments: require('./departments'),
    Devices: require('./devices'),
    Devices_components: require('./devices_components'),
    Dvc_type: require('./dvc_type'),
    Employees: require('./employees'),
    Entrances: require('./entrances'),
    Manufacturers: require('./manufacturers'),
    Partners: require('./partners'),
    Ports: require('./ports'),
    Positions: require('./positions'),
    PPanels_SGroups: require('./ppanels_sgroups'),
    PPanels: require('./ppanels'),
    SGroups: require('./sgroups'),
    Switches: require('./switches'),
    VLANs: require('./vlans'),
    Workplaces: require('./workplaces'),
    Wp_type: require('./wp_type')
};
