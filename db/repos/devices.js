'use strict';

const sql = require('../sql').devices;

/*
 This repository mixes hard-coded and dynamic SQL, primarily to show a diverse example of using both.
 */

class DevicesRepository {
    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;

        // set-up all ColumnSet objects, if needed:
        this.cs = createColumnsets(pgp);
    }

    // Creates the table;
    create() {
        return this.db.none(sql.create);
    }

    // Initializes the table with some user records, and return their id-s;
    init() {
        return this.db.map(sql.init, [], row => row.id);
    }

    // Drops the table;
    drop() {
        return this.db.none(sql.drop);
    }

    // Removes all records from the table;
    empty() {
        return this.db.none(sql.empty);
    }

    // Adds a new user, and returns the new object;
    add(name, workplaces_id, partners_id, dvc_type_id, inventory_nr) {
        return this.db.one(sql.add, [name, workplaces_id, partners_id, dvc_type_id, inventory_nr]);
    }

    // Adds a new user, and returns the new object;
    update(id, name, workplaces_id, partners_id, dvc_type_id, inventory_nr) {
        return this.db.one(sql.update, [id, name, workplaces_id, partners_id, dvc_type_id, inventory_nr]);
    }

    // Tries to delete a user by id, and returns the number of records deleted;
    remove(id) {
        return this.db.result('DELETE FROM devices WHERE id = $1', +id, r => r.rowCount);
    }

    findComponents(id){
      return this.db.any('SELECT devices_components.id as id, devices_components.devices_id as devices_id, devices_components.components_id as components_id, components.cpnt_type_id as cpnt_type_id, components.name AS component, devices_components.amount AS amount, devices_components.serial_nr AS serial_nr FROM devices_components, devices, components, cpnt_type WHERE devices_components.devices_id=devices.id AND devices_components.components_id = components.id AND cpnt_type_id = cpnt_type.id AND devices.id=$1', +id);
    }

    // Tries to find a user from id;
    findById(id) {
        return this.db.oneOrNone('SELECT * FROM devices WHERE id = $1', +id);
    }

    // Tries to find a user from name;
    findByName(name) {
        return this.db.oneOrNone('SELECT * FROM devices WHERE name = $1', name);
    }

    // Returns all user records;
    all() {
        return this.db.any('SELECT * FROM devices');
    }

    all_prety(){
      return this.db.any('SELECT devices.id AS id, devices.name AS name, workplaces.name AS workplace, partners.name as partner, dvc_type.name as type, devices.inventory_nr AS inventory_nr FROM devices, workplaces, partners, dvc_type WHERE devices.workplaces_id=workplaces.id AND devices.partners_id = partners.id AND device.dvc_type_id=dvc_type.id');
    }

    // Returns the total number of users;
    total() {
        return this.db.one('SELECT count(*) FROM devices', [], a => +a.count);
    }
}

//////////////////////////////////////////////////////////
// showing how to statically initialize ColumnSet objects

let cs; // ColumnSet objects static namespace

function createColumnsets(pgp) {
    // create all ColumnSet objects only once:
    if (!cs) {
        cs = {};
        // Type TableName is useful when schema isn't default, otherwise you can
        // just pass in a string for the table name;
        const table = new pgp.helpers.TableName({table: 'devices', schema: 'public'});

        cs.insert = new pgp.helpers.ColumnSet(['name'], {table});
        cs.update = cs.insert.extend(['?id']);
    }
    return cs;
}

module.exports = DevicesRepository;
