'use strict';


class PortsRepository {
    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;


    }

    all() {
      return this.db.any('SELECT pg.id AS panel_id, pg.name AS panel_name, pg.pg_port AS panel_port, sg_pgs.sgroups_id AS group_id, sg_pgs.group_name, sg_pgs.group_port FROM ( \
	SELECT sg.name AS group_name , sg.sg_port AS group_port, sg.size AS group_size, pgs.*  \
	FROM ppanels_sgroups AS pgs \
	FULL OUTER JOIN ( \
		SELECT id, name, size, generate_series(1, size) AS sg_port \
		FROM sgroups) AS sg \
	ON (sg.id = pgs.sgroups_id AND pgs.sgroups_pos = sg.sg_port) \
) as sg_pgs \
FULL OUTER JOIN ( \
	SELECT id, name, size, generate_series(1, size) AS pg_port \
	FROM ppanels ) as pg \
	ON (pg.id = sg_pgs.ppanels_id AND pg.pg_port = sg_pgs.ppanels_pos) \
ORDER BY pg.name, pg.pg_port, sg_pgs.group_name, sg_pgs.group_port');
    }
    // Tries to find a user from id;
findBy(column, value) {
      return this.db.any('SELECT * from (SELECT pg.id AS panel_id, pg.name AS panel_name, pg.pg_port AS panel_port, sg_pgs.sgroups_id AS group_id, sg_pgs.group_name, sg_pgs.group_port FROM ( \
  SELECT sg.name AS group_name , sg.sg_port AS group_port, sg.size AS group_size, pgs.*  \
  FROM ppanels_sgroups AS pgs \
  FULL OUTER JOIN ( \
    SELECT id, name, size, generate_series(1, size) AS sg_port \
    FROM sgroups) AS sg \
  ON (sg.id = pgs.sgroups_id AND pgs.sgroups_pos = sg.sg_port) \
  ) as sg_pgs \
  FULL OUTER JOIN ( \
  SELECT id, name, size, generate_series(1, size) AS pg_port \
  FROM ppanels ) as pg \
  ON (pg.id = sg_pgs.ppanels_id AND pg.pg_port = sg_pgs.ppanels_pos) \
  ORDER BY pg.name, pg.pg_port, sg_pgs.group_name, sg_pgs.group_port \
) as all_lines \
where all_lines.'+column+'=' +value);
}
    // Returns the total number of users;
    total() {
        return this.db.one('SELECT count(*) FROM positions', [], a => +a.count);
    }
}




module.exports = PortsRepository;
