'use strict';

const sql = require('../sql').components;

/*
 This repository mixes hard-coded and dynamic SQL, primarily to show a diverse example of using both.
 */

class ComponentsRepository {
    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;

        // set-up all ColumnSet objects, if needed:
        this.cs = createColumnsets(pgp);
    }

    // Creates the table;
    create() {
        return this.db.none(sql.create);
    }

    // Initializes the table with some user records, and return their id-s;
    init() {
        return this.db.map(sql.init, [], row => row.id);
    }

    // Drops the table;
    drop() {
        return this.db.none(sql.drop);
    }

    // Removes all records from the table;
    empty() {
        return this.db.none(sql.empty);
    }

    // Adds a new user, and returns the new object;
    add(name, cpnt_type_id, manufacturers_id, price, comments) {
        return this.db.one(sql.add, [name, cpnt_type_id, manufacturers_id, price, comments]);
    }

    // Adds a new user, and returns the new object;
    update(id, name, cpnt_type_id, manufacturers_id, price, comments) {
        return this.db.one(sql.update, [id, name, cpnt_type_id, manufacturers_id, price, comments]);
    }

    // Tries to delete a user by id, and returns the number of records deleted;
    remove(id) {
        return this.db.result('DELETE FROM components WHERE id = $1', +id, r => r.rowCount);
    }

    // Tries to find a user from id;
    findById(id) {
        return this.db.oneOrNone('SELECT * FROM components WHERE id = $1', +id);
    }

    // Tries to find a user from name;
    findByName(name) {
        return this.db.oneOrNone('SELECT * FROM components WHERE name = $1', name);
    }

    findDevices(id){
      return this.db.any('SELECT \
      	devices_components.id AS id, \
      	devices_components.devices_id AS devices_id, \
      	devices_components.components_id AS components_id, \
      	components.cpnt_type_id AS cpnt_type_id, \
      	devices.name AS device, \
      	devices_components.amount AS amount, \
      	devices_components.serial_nr AS serial_nr, \
      	devices.workplaces_id AS workplaces_id, \
      	devices.inventory_nr AS inventory_nr, \
      	workplaces.name	AS workplace, \
      	workplaces.employees_id AS employees_id, \
      	employees.name AS employees \
      FROM  \
      	devices_components, \
      	devices, \
      	components, \
      	cpnt_type, \
      	workplaces, \
      	employees \
      WHERE \
      	devices_components.devices_id=devices.id AND \
      	devices_components.components_id = components.id AND \
      	cpnt_type_id = cpnt_type.id AND \
      	devices.workplaces_id = workplaces.id AND \
      	workplaces.employees_id = employees.id AND \
	      components.id=$1', +id);
    }

    // Returns all records;
    all() {
        return this.db.any('SELECT * FROM components');
    }

    all_prety(){
      return this.db.any('SELECT components.id as id, components.name as name,  components.cpnt_type_id as cpnt_type_id, cpnt_type.name AS type, components.manufacturers_id as manufacturers_id, manufacturers.name as manufacturer, price, comments from components, manufacturers, cpnt_type  where components.manufacturers_id=manufacturers.id and components.cpnt_type_id = cpnt_type.id');
    }

    // Returns the total number of users;
    total() {
        return this.db.one('SELECT count(*) FROM components', [], a => +a.count);
    }
}

//////////////////////////////////////////////////////////
// showing how to statically initialize ColumnSet objects

let cs; // ColumnSet objects static namespace

function createColumnsets(pgp) {
    // create all ColumnSet objects only once:
    if (!cs) {
        cs = {};
        // Type TableName is useful when schema isn't default, otherwise you can
        // just pass in a string for the table name;
        const table = new pgp.helpers.TableName({table: 'components', schema: 'public'});

        cs.insert = new pgp.helpers.ColumnSet(['name'], {table});
        cs.update = cs.insert.extend(['?id']);
    }
    return cs;
}

module.exports = ComponentsRepository;
