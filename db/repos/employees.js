'use strict';

const sql = require('../sql').employees;

/*
 This repository mixes hard-coded and dynamic SQL, primarily to show a diverse example of using both.
 */

class EmployeesRepository {
    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;

        // set-up all ColumnSet objects, if needed:
        this.cs = createColumnsets(pgp);
    }

    // Creates the table;
    create() {
        return this.db.none(sql.create);
    }

    // Initializes the table with some user records, and return their id-s;
    init() {
        return this.db.map(sql.init, [], row => row.id);
    }

    // Drops the table;
    drop() {
        return this.db.none(sql.drop);
    }

    // Removes all records from the table;
    empty() {
        return this.db.none(sql.empty);
    }

    // Adds a new user, and returns the new object;
    add(name, position, department, is_group, parrent_group, allow_login) {
        return this.db.one(sql.add, [name, position, department, is_group, parrent_group, allow_login]);
    }

    // Adds a new user, and returns the new object;
    update(id, name, position, department, is_group, parrent_group, allow_login) {
        return this.db.one(sql.update, [id, name, position, department, is_group, parrent_group, allow_login]);
    }

    // Tries to delete a user by id, and returns the number of records deleted;
    remove(id) {
        return this.db.result('DELETE FROM employees WHERE id = $1', +id, r => r.rowCount);
    }

    // Tries to find a user from id;
    findById(id) {
        return this.db.oneOrNone('SELECT * FROM employees WHERE id = $1', +id);
    }

    // Tries to find a user from name;
    findByName(name) {
        return this.db.oneOrNone('SELECT * FROM employees WHERE name = $1', name);
    }

    // Returns all user records;
    all() {
        return this.db.any('SELECT * FROM employees WHERE is_group=false');
    }

    all_prety(){
      return this.db.any('select employees.id as id, employees.name as name, positions.name as position, departments.name as department from employees, positions, departments where employees.department_id=departments.id and employees.position_id=positions.id');
    }

    all_by_group(group_id){
      return this.db.any('select employees.id as id, employees.name as name, positions.name as position, departments.name as department , employees.is_group AS is_group, employees.parrent_group AS parrent_group from employees, positions, departments where employees.department_id=departments.id and employees.position_id=positions.id AND employees.parrent_group = $1', group_id);
    }

    import(name , username, ad_id){
      return this.db.oneOrNone("INSERT INTO employees (name, position_id, department_id, parrent_group, username, ad_id) SELECT '" + name+ "', 0, 0, 6, '"+username+"', '"+ad_id+"' WHERE NOT EXISTS ( SELECT id FROM employees WHERE name = '"+ name +"' );")
    }

    checkLogin(name){
      return this.db.oneOrNone("SELECT id FROM employees WHERE username = '"+name+"' AND allow_login");
    }

    // Returns the total number of users;
    total() {
        return this.db.one('SELECT count(*) FROM employees', [], a => +a.count);
    }
}

//////////////////////////////////////////////////////////
// showing how to statically initialize ColumnSet objects

let cs; // ColumnSet objects static namespace

function createColumnsets(pgp) {
    // create all ColumnSet objects only once:
    if (!cs) {
        cs = {};
        // Type TableName is useful when schema isn't default, otherwise you can
        // just pass in a string for the table name;
        const table = new pgp.helpers.TableName({table: 'employees', schema: 'public'});

        cs.insert = new pgp.helpers.ColumnSet(['name'], {table});
        cs.update = cs.insert.extend(['?id']);
    }
    return cs;
}

module.exports = EmployeesRepository;
