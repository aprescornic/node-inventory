'use strict';

const sql = require('../sql').entrances;

/*
 This repository mixes hard-coded and dynamic SQL, primarily to show a diverse example of using both.
 */

class EntrancesRepository {
    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;

        // set-up all ColumnSet objects, if needed:
        this.cs = createColumnsets(pgp);
    }

    // Creates the table;
    create() {
        return this.db.none(sql.create);
    }

    // Initializes the table with some user records, and return their id-s;
    init() {
        return this.db.map(sql.init, [], row => row.id);
    }

    // Drops the table;
    drop() {
        return this.db.none(sql.drop);
    }

    // Removes all records from the table;
    empty() {
        return this.db.none(sql.empty);
    }

    // Adds a new user, and returns the new object;
    add(name, op_date, partners_id, invoice, devices_id, price) {
        return this.db.one(sql.add, [name, op_date, partners_id, invoice, devices_id, price]);
    }

    // Adds a new user, and returns the new object;
    update(id, name, op_date, partners_id, invoice, devices_id, price) {
        return this.db.one(sql.update, [id, name, op_date, partners_id, invoice, devices_id, price]);
    }

    // Tries to delete a user by id, and returns the number of records deleted;
    remove(id) {
        return this.db.result('DELETE FROM entrances WHERE id = $1', +id, r => r.rowCount);
    }

    // Tries to find a user from id;
    findById(id) {
        return this.db.oneOrNone('SELECT * FROM entrances WHERE id = $1', +id);
    }

    // Tries to find a user from name;
    findByName(name) {
        return this.db.oneOrNone('SELECT * FROM entrances WHERE name = $1', name);
    }

    // Returns all user records;
    all() {
        return this.db.any('SELECT * FROM entrances');
    }

    all_prety(){
      return this.db.any('select entrances.id as id, entrances.name as name, to_char(entrances.op_date, \'YYYY/MM/DD\') as date, partners.name as partner, entrances.invoice as invoice, devices.name as device, entrances.price as price from devices, entrances, partners where devices.id=entrances.devices_id and entrances.partners_id=partners.id ');
    }

    // Returns the total number of users;
    total() {
        return this.db.one('SELECT count(*) FROM entrances', [], a => +a.count);
    }
}

//////////////////////////////////////////////////////////
// showing how to statically initialize ColumnSet objects

let cs; // ColumnSet objects static namespace

function createColumnsets(pgp) {
    // create all ColumnSet objects only once:
    if (!cs) {
        cs = {};
        // Type TableName is useful when schema isn't default, otherwise you can
        // just pass in a string for the table name;
        const table = new pgp.helpers.TableName({table: 'entrances', schema: 'public'});

        cs.insert = new pgp.helpers.ColumnSet(['name'], {table});
        cs.update = cs.insert.extend(['?id']);
    }
    return cs;
}

module.exports = EntrancesRepository;
