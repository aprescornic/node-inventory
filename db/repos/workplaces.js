'use strict';

const sql = require('../sql').workplaces;

/*
 This repository mixes hard-coded and dynamic SQL, primarily to show a diverse example of using both.
 */

class WorkplacesRepository {
    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;

        // set-up all ColumnSet objects, if needed:
        this.cs = createColumnsets(pgp);
    }

    // Creates the table;
    create() {
        return this.db.none(sql.create);
    }

    // Initializes the table with some user records, and return their id-s;
    init() {
        return this.db.map(sql.init, [], row => row.id);
    }

    // Drops the table;
    drop() {
        return this.db.none(sql.drop);
    }

    // Removes all records from the table;
    empty() {
        return this.db.none(sql.empty);
    }

    // Adds a new user, and returns the new object;
    add(name, wp_type_id, departments_id, employees_id, room, phone, ip, comments) {
        return this.db.one(sql.add, [name, wp_type_id, departments_id, employees_id, room, phone, ip, comments]);
    }

    // Adds a new user, and returns the new object;
    update(id, name, wp_type_id, departments_id, employees_id, room, phone, ip, comments) {
        return this.db.one(sql.update, [id, name, wp_type_id, departments_id, employees_id, room, phone, ip, comments]);
    }

    // Tries to delete a user by id, and returns the number of records deleted;
    remove(id) {
        return this.db.result('DELETE FROM workplaces WHERE id = $1', +id, r => r.rowCount);
    }

    // Tries to find a user from id;
    findById(id) {
        return this.db.oneOrNone('SELECT * FROM workplaces WHERE id = $1', +id);
    }

    // Tries to find a user from name;
    findByName(name) {
        return this.db.oneOrNone('SELECT * FROM workplaces WHERE name = $1', name);
    }

    // Tries to find a user from name;
    findDevices(workplace_id) {
        return this.db.any("SELECT devices.id AS id, coalesce(devices.name, '') || ' - ' || coalesce(devices.inventory_nr, '') AS device, devices.id AS devices_id,  dvc_type.name AS dvc_type, cpnt_type.name AS cpnt_type, components.name as component, devices_components.amount as qtty, devices_components.serial_nr as sn "+
                            'FROM devices, devices_components, components, cpnt_type, dvc_type '+
                            'WHERE devices_components.devices_id = devices.id AND devices_components.components_id = components.id AND components.cpnt_type_id = cpnt_type.id AND devices.dvc_type_id = dvc_type.id AND '+
                            'devices.workplaces_id =  $1', workplace_id);
    }

    findByEmployee(employees_id) {
        return this.db.any("SELECT "+
                          	"workplaces.name AS workplace,  "+
                          	"wp_type.name AS wp_type, "+
                          	"workplaces.room AS room, "+
                          	"workplaces.phone AS phone, "+
                          	"workplaces.ip AS IP, "+
                          	"workplaces.comments as wp_comments, "+
                            "dvc_type.name AS dvc_type, "+
                          	"devices.name AS device, "+
                          	"devices.inventory_nr AS inventory_nr, "+
                          	"partners.name AS partners_owner, "+
                            "cpnt_type.name AS cpn_type, "+
                            "components.name AS component, "+
	                          "devices_components.amount as qtty, "+
	                           "devices_components.serial_nr as serial_nr "+
                          "FROM "+
                          	"workplaces, wp_type, devices, partners, dvc_type, devices_components, components, cpnt_type "+
                          "WHERE "+
                          	"workplaces.wp_type_id = wp_type.id AND "+
                            "devices.workplaces_id = workplaces.id AND "+
                          	"devices.partners_id = partners.id AND "+
                          	"devices.dvc_type_id = dvc_type.id AND "+
                            "devices_components.devices_id = devices.id AND "+
                            "devices_components.components_id  = components.id AND "+
                            "components.cpnt_type_id = cpnt_type.id AND "+
                          	"workplaces.employees_id = $1	", employees_id);
    }

    // Returns all user records;
    all() {
        return this.db.any('SELECT * FROM workplaces');
    }

    all_prety(){
      return this.db.any('select workplaces.id as id, workplaces.name as name, wp_type.name as type, departments.name as department, employees.name as employe, workplaces.room as room, workplaces.phone as phone,  workplaces.ip as ip, workplaces.comments as comments from workplaces, employees, departments, wp_type where workplaces.departments_id=departments.id and workplaces.employees_id=employees.id and workplaces.wp_type_id=wp_type.id');
    }

    // Returns the total number of users;
    total() {
        return this.db.one('SELECT count(*) FROM workplaces', [], a => +a.count);
    }
}

//////////////////////////////////////////////////////////
// showing how to statically initialize ColumnSet objects

let cs; // ColumnSet objects static namespace

function createColumnsets(pgp) {
    // create all ColumnSet objects only once:
    if (!cs) {
        cs = {};
        // Type TableName is useful when schema isn't default, otherwise you can
        // just pass in a string for the table name;
        const table = new pgp.helpers.TableName({table: 'workplaces', schema: 'public'});

        cs.insert = new pgp.helpers.ColumnSet(['name'], {table});
        cs.update = cs.insert.extend(['?id']);
    }
    return cs;
}

module.exports = WorkplacesRepository;
