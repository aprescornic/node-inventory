'use strict';

const sql = require('../sql').ppanels_sgroups;

/*
 This repository mixes hard-coded and dynamic SQL, primarily to show a diverse example of using both.
 */

class ppanels_sgroupsRepository {
    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;

        // set-up all ColumnSet objects, if needed:
        this.cs = createColumnsets(pgp);
    }

    // Creates the table;
    create() {
        return this.db.none(sql.create);
    }

    // Initializes the table with some user records, and return their id-s;
    init() {
        return this.db.map(sql.init, [], row => row.devices_id);
    }

    // Drops the table;
    drop() {
        return this.db.none(sql.drop);
    }

    // Removes all records from the table;
    empty() {
        return this.db.none(sql.empty);
    }

    // Adds a new user, and returns the new object;
    add(ppanels_id, sgroups_id, ppanels_pos, sgroups_pos) {
        return this.db.one(sql.add, [ppanels_id, sgroups_id, ppanels_pos, sgroups_pos]);
    }

    // Adds a new user, and returns the new object;
    update(id, ppanels_id, sgroups_id, ppanels_pos, sgroups_pos) {
        return this.db.one(sql.update, [id, ppanels_id, sgroups_id, ppanels_pos, sgroups_pos]);
    }

    // Tries to delete a user by id, and returns the number of records deleted;
    remove(id) {
        return this.db.result('DELETE FROM ppanels_sgroups WHERE id = $1 ', +id, r => r.rowCount);
    }

    // Tries to find a user from id;
    findById(devices_id, components_id) {
        return this.db.oneOrNone('SELECT * FROM ppanels_sgroups WHERE devices_id = $1, AND components_id = $2', +devices_id, +components_id);
    }

    filterBy(column, value) {
      return this.db.any('SELECT * FROM ppanels_sgroups WHERE '+column+' = '+value);
    }

    // Returns all user records;
    all() {
        return this.db.any('SELECT * FROM ppanels_sgroups');
    }

    all_prety(){
      return this.db.any('SELECT ppanels_sgroups.id as id, ppanels.name AS ppanel, sgroups.name AS sgroup, ppanels_sgroups.ppanels_pos AS panel_pos, ppanels_sgroups.sgroups_pos as socket_pos\
      FROM ppanels_sgroups, ppanels, sgroups \
      WHERE ppanels_sgroups.ppanels_id=ppanels.id AND ppanels_sgroups.sgroups_id = sgroups.id');
    }

    // Returns the total number of users;
    total() {
        return this.db.one('SELECT count(*) FROM ppanels_sgroups', [], a => +a.count);
    }
}

//////////////////////////////////////////////////////////
// showing how to statically initialize ColumnSet objects

let cs; // ColumnSet objects static namespace

function createColumnsets(pgp) {
    // create all ColumnSet objects only once:
    if (!cs) {
        cs = {};
        // Type TableName is useful when schema isn't default, otherwise you can
        // just pass in a string for the table name;
        const table = new pgp.helpers.TableName({table: 'ppanels_sgroups', schema: 'public'});

        cs.insert = new pgp.helpers.ColumnSet(['name'], {table});
        cs.update = cs.insert.extend(['?devices_id']);
    }
    return cs;
}

module.exports = ppanels_sgroupsRepository;
