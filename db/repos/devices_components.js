'use strict';

const sql = require('../sql').devices_components;

/*
 This repository mixes hard-coded and dynamic SQL, primarily to show a diverse example of using both.
 */

class Devices_componentsRepository {
    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;

        // set-up all ColumnSet objects, if needed:
        this.cs = createColumnsets(pgp);
    }

    // Creates the table;
    create() {
        return this.db.none(sql.create);
    }

    // Initializes the table with some user records, and return their id-s;
    init() {
        return this.db.map(sql.init, [], row => row.devices_id);
    }

    // Drops the table;
    drop() {
        return this.db.none(sql.drop);
    }

    // Removes all records from the table;
    empty() {
        return this.db.none(sql.empty);
    }

    // Adds a new user, and returns the new object;
    add(devices_id, components_id, amount, serial_nr) {
        return this.db.one(sql.add, [devices_id, components_id, amount, serial_nr]);
    }

    // Adds a new user, and returns the new object;
    update(id, devices_id, components_id, amount, serial_nr) {
        return this.db.one(sql.update, [id, devices_id, components_id, amount, serial_nr]);
    }

    // Tries to delete a user by id, and returns the number of records deleted;
    remove(id) {
        return this.db.result('DELETE FROM devices_components WHERE id = $1 ', +id, r => r.rowCount);
    }

    // Tries to find a user from id;
    findById(devices_id, components_id) {
        return this.db.oneOrNone('SELECT * FROM devices_components WHERE devices_id = $1, AND components_id = $2', +devices_id, +components_id);
    }

    // Tries to find a user from name;
    findBySerial(serial_nr) {
        return this.db.oneOrNone('SELECT * FROM devices_components WHERE serial_nr = $1', serial_nr);
    }

    // Returns all user records;
    all() {
        return this.db.any('SELECT * FROM devices_components');
    }

    all_prety(){
      return this.db.any('SELECT devices.name AS device, components.name AS component, devices_components.amount AS amount, devices_components.serial_nr AS serial_nr FROM devices_components, devices, components WHERE devices_components.devices_id=devices.id AND devices_components.components_id = components.id');
    }

    // Returns the total number of users;
    total() {
        return this.db.one('SELECT count(*) FROM devices_components', [], a => +a.count);
    }
}

//////////////////////////////////////////////////////////
// showing how to statically initialize ColumnSet objects

let cs; // ColumnSet objects static namespace

function createColumnsets(pgp) {
    // create all ColumnSet objects only once:
    if (!cs) {
        cs = {};
        // Type TableName is useful when schema isn't default, otherwise you can
        // just pass in a string for the table name;
        const table = new pgp.helpers.TableName({table: 'devices_components', schema: 'public'});

        cs.insert = new pgp.helpers.ColumnSet(['name'], {table});
        cs.update = cs.insert.extend(['?devices_id']);
    }
    return cs;
}

module.exports = Devices_componentsRepository;
