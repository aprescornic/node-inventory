module.exports =  {
  "development": {
    "username": "devel",
    "password": "742312",
    "database": "inventory",
    "host": "127.0.0.1",
    "dialect": "postgres"
  },
  "test": {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "postgres"
  },
  "production": {
    "username": process.env.PG_USER   || "postgres",
    "password": process.env.PG_PASS   ||  "null",
    "database": process.env.PG_DB     || "database_production",
    "host": process.env.PG_HOST   || "127.0.0.1",
    "dialect": "postgres",
    // "use_env_variable": "DATABASE_URL"
  }
}
