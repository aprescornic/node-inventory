'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('wp_map', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      workplaces_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        unique: true
      },
      x_pos:{
        type: Sequelize.INTEGER,
        allowNull: false,},
      y_pos:{
        type: Sequelize.INTEGER,
        allowNull: false,},
      x_size:{
        type: Sequelize.INTEGER,
        allowNull: false,},
      y_size:{
        type: Sequelize.INTEGER,
        allowNull: false,},
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deleted_at: {
          type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('wp_map');
  }
};
