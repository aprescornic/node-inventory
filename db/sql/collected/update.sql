/*
    Update a record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
UPDATE ${schema~}.collected
SET empl_id=$2, data=$3
WHERE ID = $1
RETURNING *
