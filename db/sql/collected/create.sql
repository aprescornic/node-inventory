/*
    Creates table Configs.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/

CREATE TABLE ${schema~}.collected
(
  ID SERIAL PRIMARY KEY,
  empl_id VARCHAR,
  data JSON,
  imported boolean not null default false
);
