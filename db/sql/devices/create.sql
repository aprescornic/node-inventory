/*
    Creates table Configs.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/

CREATE TABLE ${schema~}.devices
(
  ID SERIAL PRIMARY KEY,
  name VARCHAR,
  workplaces_id int not null references workplaces(ID),
  partners_id int not null references partners(ID) ON UPDATE CASCADE ON DELETE CASCADE,,
  dvc_type_id int not null references dvc_type(ID) ON UPDATE CASCADE ON DELETE CASCADE,,
  inventory_nr varchar ,
  CONSTRAINT devices_name_key UNIQUE (name)
);

CREATE UNIQUE INDEX unique_inventory_nr_when_not_null
    ON devices
       (inventory_nr)
 WHERE LENGTH(inventory_nr) > 0;
