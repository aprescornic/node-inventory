INSERT INTO ${schema~}.devices(name, workplaces_id, partners_id, dvc_type_id, inventory_nr)  VALUES
('PC1', 1, 1, 2, 'PC0001'),
('PC2', 1, 1, 2, 'PC002'),
('Monitor1', 1, 1, 1, 'MT001'),
('Monitor2',1, 1, 1, 'MT002'),
('Monitor3',1, 1, 1, 'MT003')
RETURNING id;
