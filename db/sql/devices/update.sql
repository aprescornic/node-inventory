/*
    Update a record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
UPDATE ${schema~}.devices
SET name=$2, workplaces_id=$3, partners_id=$4, dvc_type_id=$5, inventory_nr=$6
WHERE ID = $1
RETURNING *
