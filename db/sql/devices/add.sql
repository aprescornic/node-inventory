/*
    Inserts a new record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
INSERT INTO ${schema~}.devices(name, workplaces_id, partners_id, dvc_type_id, inventory_nr)
VALUES($1, $2, $3, $4, $5)
RETURNING *
