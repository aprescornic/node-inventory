/*
    Creates table Configs.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/

CREATE TABLE ${schema~}.dvc_type
(
  ID SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  in_prepend VARCHAR NOT NULL,
  CONSTRAINT dvc_type_name_key UNIQUE (name)
);
