INSERT INTO ${schema~}.dvc_type(name, in_prepend)  VALUES
('Monitor','m'),
('Computer','c'),
('Notebook','n'),
('Storehouse','sh')
RETURNING id;
