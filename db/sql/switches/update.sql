/*
    Update a record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
UPDATE ${schema~}.switches
SET name=$2, size=$3
WHERE ID = $1
RETURNING *
