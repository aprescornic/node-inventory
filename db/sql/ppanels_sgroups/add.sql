/*
    Inserts a new record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
INSERT INTO ${schema~}.ppanels_sgroups(ppanels_id, sgroups_id, ppanels_pos, sgroups_pos)
VALUES($1, $2, $3, $4)
RETURNING *
