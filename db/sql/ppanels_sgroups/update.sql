/*
    Update a record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
UPDATE ${schema~}.ppanels_sgroups
SET ppanels_id = $2, sgroups_id = $3, ppanels_pos=$4, sgroups_pos=$5
WHERE id = $1
RETURNING *
