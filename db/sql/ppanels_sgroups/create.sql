/*
    Creates table Configs.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/

CREATE TABLE ${schema~}.ppanels_sgroups (
  ID SERIAL PRIMARY KEY,
  ppanels_id    int not null REFERENCES ppanels (ID) ON UPDATE CASCADE ON DELETE CASCADE,
  sgroups_id    int not null REFERENCES sgroups (ID) ON UPDATE CASCADE ON DELETE CASCADE,
  ppanels_pos   int NOT NULL ,
  sgroups_pos   int NOT NULL ,
  CONSTRAINT ppanels_sgroups_panel_uniq UNIQUE (ppanels_id,  ppanels_pos),  -- explicit pk
  CONSTRAINT ppanels_sgroups_sgroup_uniq UNIQUE (sgroups_id,  sgroups_pos)  -- explicit pk
);
