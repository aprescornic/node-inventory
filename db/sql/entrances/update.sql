/*
    Inserts a new record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
UPDATE ${schema~}.entrances
SET name=$2 , op_date=$3, partners_id=$4, invoice=$5, devices_id=$6, price=$7
WHERE ID = $1
RETURNING *
