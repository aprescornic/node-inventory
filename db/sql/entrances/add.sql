/*
    Inserts a new record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
INSERT INTO ${schema~}.entrances(name, op_date, partners_id, invoice, devices_id, price)
VALUES($1, $2, $3, $4, $5, $6)
RETURNING *
