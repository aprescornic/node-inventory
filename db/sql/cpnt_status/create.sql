/*
    Creates table Configs.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/

CREATE TABLE ${schema~}.cpnt_status
(
  ID SERIAL PRIMARY KEY,
  name VARCHAR not null,
  CONSTRAINT cpnt_status_name_key UNIQUE (name)
);
