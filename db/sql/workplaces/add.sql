/*
    Inserts a new record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
INSERT INTO ${schema~}.workplaces(name, wp_type_id, departments_id, employees_id, room, phone, ip, comments)
VALUES($1, $2, $3, $4, $5, $6, $7, $8)
RETURNING *
