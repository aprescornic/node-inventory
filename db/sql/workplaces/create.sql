/*
    Creates table Configs.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/

CREATE TABLE ${schema~}.workplaces
(
  ID SERIAL PRIMARY KEY,
  name VARCHAR not null,
  wp_type_id int not null references wp_type(ID),
  departments_id int not null references departments(ID),
  employees_id int not null references employees(ID),
  room VARCHAR,
  phone VARCHAR,
  ip VARCHAR,
  comments VARCHAR,
  CONSTRAINT workplaces_name_key UNIQUE (name)
);
