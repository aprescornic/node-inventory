/*
    Inserts a new record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
UPDATE ${schema~}.workplaces
SET name=$2 , wp_type_id=$3, departments_id=$4, employees_id=$5, room=$6, phone=$7, ip=$8, comments= $9
WHERE ID = $1
RETURNING *
