/*
    Update a record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
UPDATE ${schema~}.configs
SET name=$2, value=$3, type=$4, description=$5
WHERE ID = $1
RETURNING *
