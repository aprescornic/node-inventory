'use strict';

const QueryFile = require('pg-promise').QueryFile;
const path = require('path');

///////////////////////////////////////////////////////////////////////////////////////////////
// Criteria for deciding whether to place a particular query into an external SQL file or to
// keep it in-line (hard-coded):
//
// - Size / complexity of the query, because having it in a separate file will let you develop
//   the query and see the immediate updates without having to restart your application.
//
// - The necessity to document your query, and possibly keeping its multiple versions commented
//   out in the query file.
//
// In fact, the only reason one might want to keep a query in-line within the code is to be able
// to easily see the relation between the query logic and its formatting parameters. However, this
// is very easy to overcome by using only Named Parameters for your query formatting.
////////////////////////////////////////////////////////////////////////////////////////////////

module.exports = {
    collected: {
      create: sql('collected/create.sql'),
      empty: sql('collected/empty.sql'),
      init: sql('collected/init.sql'),
      drop: sql('collected/drop.sql'),
      add: sql('collected/add.sql'),
      update: sql('collected/update.sql')
    },
    components: {
      create: sql('components/create.sql'),
      empty: sql('components/empty.sql'),
      init: sql('components/init.sql'),
      drop: sql('components/drop.sql'),
      add: sql('components/add.sql'),
      update: sql('components/update.sql')
    },
    configs: {
        create: sql('configs/create.sql'),
        empty: sql('configs/empty.sql'),
        init: sql('configs/init.sql'),
        drop: sql('configs/drop.sql'),
        add: sql('configs/add.sql'),
        update: sql('configs/update.sql')
    },
    cpnt_type: {
        create: sql('cpnt_type/create.sql'),
        empty: sql('cpnt_type/empty.sql'),
        init: sql('cpnt_type/init.sql'),
        drop: sql('cpnt_type/drop.sql'),
        add: sql('cpnt_type/add.sql'),
        update: sql('cpnt_type/update.sql')
    },
    cpnt_status: {
        create: sql('cpnt_status/create.sql'),
        empty: sql('cpnt_status/empty.sql'),
        init: sql('cpnt_status/init.sql'),
        drop: sql('cpnt_status/drop.sql'),
        add: sql('cpnt_status/add.sql'),
        update: sql('cpnt_status/update.sql')
    },
    departments: {
      create: sql('departments/create.sql'),
      empty: sql('departments/empty.sql'),
      init: sql('departments/init.sql'),
      drop: sql('departments/drop.sql'),
      add: sql('departments/add.sql'),
      update: sql('departments/update.sql')
    },
    devices: {
      create: sql('devices/create.sql'),
      empty: sql('devices/empty.sql'),
      init: sql('devices/init.sql'),
      drop: sql('devices/drop.sql'),
      add: sql('devices/add.sql'),
      update: sql('devices/update.sql')
    },
    devices_components: {
      create: sql('devices_components/create.sql'),
      empty: sql('devices_components/empty.sql'),
      init: sql('devices_components/init.sql'),
      drop: sql('devices_components/drop.sql'),
      add: sql('devices_components/add.sql'),
      update: sql('devices_components/update.sql')
    },
    dvc_type: {
        create: sql('dvc_type/create.sql'),
        empty: sql('dvc_type/empty.sql'),
        init: sql('dvc_type/init.sql'),
        drop: sql('dvc_type/drop.sql'),
        add: sql('dvc_type/add.sql'),
        update: sql('dvc_type/update.sql')
    },
    employees: {
      create: sql('employees/create.sql'),
      empty: sql('employees/empty.sql'),
      init: sql('employees/init.sql'),
      drop: sql('employees/drop.sql'),
      add: sql('employees/add.sql'),
      update: sql('employees/update.sql')
    },
    entrances: {
      create: sql('entrances/create.sql'),
      empty: sql('entrances/empty.sql'),
      init: sql('entrances/init.sql'),
      drop: sql('entrances/drop.sql'),
      add: sql('entrances/add.sql'),
      update: sql('entrances/update.sql')
    },
    manufacturers: {
      create: sql('manufacturers/create.sql'),
      empty: sql('manufacturers/empty.sql'),
      init: sql('manufacturers/init.sql'),
      drop: sql('manufacturers/drop.sql'),
      add: sql('manufacturers/add.sql'),
      update: sql('manufacturers/update.sql')
    },
    partners: {
      create: sql('partners/create.sql'),
      empty: sql('partners/empty.sql'),
      init: sql('partners/init.sql'),
      drop: sql('partners/drop.sql'),
      add: sql('partners/add.sql'),
      update: sql('partners/update.sql')
    },
    positions: {
      create: sql('positions/create.sql'),
      empty: sql('positions/empty.sql'),
      init: sql('positions/init.sql'),
      drop: sql('positions/drop.sql'),
      add: sql('positions/add.sql'),
      update: sql('positions/update.sql')
    },
    ppanels_sgroups: {
      create: sql('ppanels_sgroups/create.sql'),
      empty: sql('ppanels_sgroups/empty.sql'),
      init: sql('ppanels_sgroups/init.sql'),
      drop: sql('ppanels_sgroups/drop.sql'),
      add: sql('ppanels_sgroups/add.sql'),
      update: sql('ppanels_sgroups/update.sql')
    },
    ppanels: {
      create: sql('ppanels/create.sql'),
      empty: sql('ppanels/empty.sql'),
      init: sql('ppanels/init.sql'),
      drop: sql('ppanels/drop.sql'),
      add: sql('ppanels/add.sql'),
      update: sql('ppanels/update.sql')
    },
    sgroups: {
      create: sql('sgroups/create.sql'),
      empty: sql('sgroups/empty.sql'),
      init: sql('sgroups/init.sql'),
      drop: sql('sgroups/drop.sql'),
      add: sql('sgroups/add.sql'),
      update: sql('sgroups/update.sql')
    },
    switches: {
      create: sql('switches/create.sql'),
      empty: sql('switches/empty.sql'),
      init: sql('switches/init.sql'),
      drop: sql('switches/drop.sql'),
      add: sql('switches/add.sql'),
      update: sql('switches/update.sql')
    },
    vlans: {
      create: sql('vlans/create.sql'),
      empty: sql('vlans/empty.sql'),
      init: sql('vlans/init.sql'),
      drop: sql('vlans/drop.sql'),
      add: sql('vlans/add.sql'),
      update: sql('vlans/update.sql')
    },
    workplaces: {
      create: sql('workplaces/create.sql'),
      empty: sql('workplaces/empty.sql'),
      init: sql('workplaces/init.sql'),
      drop: sql('workplaces/drop.sql'),
      add: sql('workplaces/add.sql'),
      update: sql('workplaces/update.sql')
    },
    wp_type: {
      create: sql('wp_type/create.sql'),
      empty: sql('wp_type/empty.sql'),
      init: sql('wp_type/init.sql'),
      drop: sql('wp_type/drop.sql'),
      add: sql('wp_type/add.sql'),
      update: sql('wp_type/update.sql')
    }
};

///////////////////////////////////////////////
// Helper for linking to external query files;
function sql(file) {

    const fullPath = path.join(__dirname, file); // generating full path;

    const options = {

        // minifying the SQL is always advised;
        // see also option 'compress' in the API;
        minify: true,

        // Showing how to use static pre-formatting parameters -
        // we have variable 'schema' in each SQL (as an example);
        params: {
            schema: 'public' // replace ${schema~} with "public"
        }
    };

    const qf = new QueryFile(fullPath, options);

    if (qf.error) {
        // Something is wrong with our query file :(
        // Testing all files through queries can be cumbersome,
        // so we also report it here, while loading the module:
        console.error(qf.error);
    }

    return qf;

    // See QueryFile API:
    // http://vitaly-t.github.io/pg-promise/QueryFile.html
}

//////////////////////////////////////////////////////////////////////////
// Possible alternative - enumerating all SQL files automatically ;)
// API: http://vitaly-t.github.io/pg-promise/utils.html#.enumSql

/*
// generating a recursive SQL tree for dynamic use of camelized names:
const enumSql = require('pg-promise').utils.enumSql;

module.exports = enumSql(__dirname, {recursive: true}, file => {
    // NOTE: 'file' contains the full path to the SQL file, as we use __dirname for enumeration.
    return new QueryFile(file, {
        minify: true,
        params: {
            schema: 'public' // replace ${schema~} with "public"
        }
    });
});
*/
