/*
    Creates table Configs.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/

CREATE TABLE ${schema~}.vlans
(
  ID SERIAL PRIMARY KEY,
  name VARCHAR,
  number integer not null ,
  CONSTRAINT vlans_name_key UNIQUE (name),
  CONSTRAINT vlans_number_key UNIQUE (number)
);
