/*
    Creates table Configs.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/

CREATE TABLE ${schema~}.sgroups
(
  ID SERIAL PRIMARY KEY,
  name VARCHAR,
  size integer not null ,
  CONSTRAINT sgroups_name_key UNIQUE (name)
);
