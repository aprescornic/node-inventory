/*
    Update a record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
UPDATE ${schema~}.devices_components
SET devices_id = $2, components_id = $3, amount=$4, serial_nr=$5
WHERE id = $1
RETURNING *
