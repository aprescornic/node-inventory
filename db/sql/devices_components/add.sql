/*
    Inserts a new record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
INSERT INTO ${schema~}.devices_components(devices_id, components_id, amount, serial_nr)
VALUES($1, $2, $3, $4)
RETURNING *
