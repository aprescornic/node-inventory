INSERT INTO ${schema~}.devices_components(devices_id, components_id, amount, serial_nr)  VALUES
(1, 6, 50, 'SN0001'),
(1, 7, 70, 'SN002'),
(2, 6, 50, 'SN003'),
(2, 8, 70, 'SN004'),
(3, 1, 120, 'SN005'),
(4, 4, 50, 'SN006'),
(5, 5, 50, 'SN007')
RETURNING devices_id;
