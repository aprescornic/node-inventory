/*
    Creates table Configs.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/

CREATE TABLE ${schema~}.devices_components (
  ID SERIAL PRIMARY KEY,
  devices_id    int not null REFERENCES devices (ID) ON UPDATE CASCADE ON DELETE CASCADE,
  components_id int not null REFERENCES components (ID) ON UPDATE CASCADE,
  amount     numeric NOT NULL DEFAULT 1,
  serial_nr varchar,
  CONSTRAINT devices_components_pkey PRIMARY KEY (devices_id, components_id)  -- explicit pk
);
