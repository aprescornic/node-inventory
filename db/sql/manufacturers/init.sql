INSERT INTO ${schema~}.manufacturers(name)  VALUES
('3Com'),
('Abit'),
('Acer'),
('AMD'),
('ATI'),
('Canon'),
('D-Link'),
('Epson'),
('Gigabite'),
('HP'),
('Intel'),
('LG'),
('MSI'),
('NVIDIA'),
('Samsung'),
('Seagate'),
('Toshiba'),
('Western digital'),
('Cisco'),
('NoName'),
('Philips')
RETURNING id;
