/*
    Inserts a new record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
INSERT INTO ${schema~}.components(name, cpnt_type_id, manufacturers_id, price, comments)
VALUES($1, $2, $3, $4, $5)
RETURNING *
