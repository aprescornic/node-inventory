/*
    Update a record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
UPDATE ${schema~}.components
SET name=$2, cpnt_type_id=$3, manufacturers_id=$4, price=$5, comments=$6
WHERE ID = $1
RETURNING *
