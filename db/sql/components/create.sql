/*
    Creates table Configs.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/

CREATE TABLE ${schema~}.components
(
  ID SERIAL PRIMARY KEY,
  name VARCHAR not null,
  cpnt_type_id int not null references cpnt_type(ID),
  manufacturers_id int not null references manufacturers(ID),
  price FLOAT(2),
  comments VARCHAR,
  CONSTRAINT components_name_key UNIQUE (name)
);
