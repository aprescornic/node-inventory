/*
    Creates table Configs.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/

CREATE TABLE ${schema~}.cpnt_type
(
  ID SERIAL PRIMARY KEY,
  name VARCHAR not null,
  CONSTRAINT cpnt_type_name_key UNIQUE (name)
);
