INSERT INTO ${schema~}.cpnt_type(name)  VALUES
('Monitor'),
('Case'),
('CPU'),
('MainBoard'),
('Keyboard'),
('Mouse'),
('HDD'),
('RAM'),
('CD-Rom'),
('LAN Card'),
('Video Card'),
('Rowter'),
('Printer'),
('Switch'),
('Access Point'),
('Adapter'),
('Cable')
RETURNING id;
