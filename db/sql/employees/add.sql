/*
    Inserts a new record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
INSERT INTO ${schema~}.employees(name, position_id, department_id, is_group, parrent_group, allow_login)
VALUES($1, $2, $3, $4, $5, $6)
RETURNING *
