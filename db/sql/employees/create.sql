/*
    Creates table Configs.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/

CREATE TABLE ${schema~}.employees
(
  ID SERIAL PRIMARY KEY,
  name VARCHAR not null,
  position_id int not null references positions(ID),
  department_id int not null references departments(ID),
  is_group boolean not null default false,
  parrent_group  int not null default 0,
  username VARCHAR,
  ad_id VARCHAR,
  allow_login boolean not null default false,
  CONSTRAINT employees_name_key UNIQUE (name)
);
