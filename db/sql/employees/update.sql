/*
    Update a record.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
UPDATE ${schema~}.employees
SET name=$2, position_id=$3, department_id=$4, is_group=$5, parrent_group=$6, allow_login=$7
WHERE ID = $1
RETURNING *
