/*
    Drops the entire table Configs.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
DROP TABLE ${schema~}.employees
