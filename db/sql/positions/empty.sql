/*
    Quickly deletes all records from table
    and all dependent records from another tables.

    NOTE: We only add schema here to demonstrate the ability of class QueryFile
    to pre-format SQL with static formatting parameters when needs to be.
*/
TRUNCATE TABLE ${schema~}.positions CASCADE
