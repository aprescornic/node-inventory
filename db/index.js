'use strict';

// Bluebird is the best promise library available today,
// and is the one recommended here:
const promise = require('bluebird');

const repos = require('./repos'); // loading all repositories

// pg-promise initialization options:
const  initOptions = {
  // Initialization Options
  promiseLib: promise,
  // Extending the database protocol with our custom repositories;
    // API: http://vitaly-t.github.io/pg-promise/global.html#event:extend
    extend(obj, dc) {
        // Database Context (dc) is mainly useful when extending multiple databases
        // with different access API-s.

        // Do not use 'require()' here, because this event occurs for every task
        // and transaction being executed, which should be as fast as possible.
        obj.collected = new repos.Collected(obj, pgp);
        obj.components = new repos.Components(obj, pgp);
        obj.configs = new repos.Configs(obj, pgp);
        obj.cpnt_status = new repos.Cpnt_status(obj,pgp);
        obj.cpnt_type = new repos.Cpnt_type(obj,pgp);
        obj.departments = new repos.Departments(obj, pgp);
        obj.devices = new repos.Devices(obj, pgp);
        obj.devices_components = new repos.Devices_components(obj,pgp);
        obj.dvc_type = new repos.Dvc_type(obj,pgp);
        obj.employees = new repos.Employees(obj, pgp);
        obj.entrances = new repos.Entrances(obj, pgp);
        obj.manufacturers = new repos.Manufacturers(obj, pgp);
        obj.partners = new repos.Partners(obj, pgp);
        obj.ports = new repos.Ports(obj, pgp);
        obj.positions = new repos.Positions(obj, pgp);
        obj.ppanels_sgroups = new repos.PPanels_SGroups(obj,pgp);
        obj.ppanels = new repos.PPanels(obj,pgp);
        obj.sgroups = new repos.SGroups(obj,pgp);
        obj.switches = new repos.Switches(obj,pgp);
        obj.vlans = new repos.VLANs(obj,pgp);
        obj.workplaces = new repos.Workplaces(obj, pgp);
        obj.wp_type = new repos.Wp_type(obj,pgp);
    }
};


// Database connection parameters:
const dbconfig = {
    host:     process.env.PG_HOST   || 'localhost',
    port:     process.env.PG_PORT   || 5432,
    database: process.env.PG_DB     || 'inventory',
    user:     process.env.PG_USER   || 'devel',
    password: process.env.PG_PASS   || '742312'
};


var pgp = require('pg-promise')(initOptions);
var db = pgp(dbconfig);


// Load and initialize optional diagnostics:
const diagnostics = require('./diagnostics');
diagnostics.init(initOptions);

// add query functions

module.exports = db;
  // getSinglePuppy: getSinglePuppy,
  // createPuppy: createPuppy,
  // updatePuppy: updatePuppy,
  // removePuppy: removePuppy

// };
