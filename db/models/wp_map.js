'use strict';
module.exports = (sequelize, DataTypes) => {
  var wp_map = sequelize.define('wp_map', {
    workplaces_id:{
      type: DataTypes.INTEGER,
        allowNull: false,},
    x_pos:{
      type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          isNumeric: true,
        }
      },
    y_pos:{
      type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          isNumeric: true,
        }
      },
    x_size:{
      type: DataTypes.INTEGER,
        allowNull: false,},
    y_size:{
      type: DataTypes.INTEGER,
        allowNull: false,}
  }, {
    paranoid: true,
    underscored: true,
    freezeTableName: true,
  });
  wp_map.associate = function(models) {
    // associations can be defined here
  };
  return wp_map;
};
