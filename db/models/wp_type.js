'use strict';
module.exports = (sequelize, DataTypes) => {
  var wp_type = sequelize.define('wp_type', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      },
  }, {
    paranoid: true,
    underscored: true,
    freezeTableName: true,
  });
  wp_type.associate = function(models) {
    // associations can be defined here
  };
  return wp_type;
};
