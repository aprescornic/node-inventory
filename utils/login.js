
var _ = require('underscore');
var ActiveDirectory = require('activedirectory');

var adConfig = { url: process.env.AD_URL || "ldap://127.0.0.1" ,
               baseDN: process.env.AD_BASE_DN || "dc=mydomain,dc=com",
               username: process.env.AD_USERNAME || "me@mydomain.com" ,
               password: process.env.AD_PASSWORD || "pass",
              domain: process.env.AD_DOMAIN || "codefactorygroup.com",
              attributes: {
                 user: [ 'sAMAccountName', 'name', 'objectSid' ]
               }
             };

// const session = require('express-session');
var db = require('../db');

// Authentication middleware
exports.checkUserSession = function (req, res, next) {
  if (!req.session.user ){//&& req.originalUrl != "/login" && req.headers.referer != "/login") {
    res.redirect('/login?redirectTo='+req.originalUrl);
    // return next ();
  }else
    return next();
};

exports.skipUserSession = function (req, res, next) {
    return next(200);
};

exports.loginPage = function(req, res){

  console.log("redirectTo " +req.query.redirectTo);
  res.render('login', { title: 'Inventory' , redirectTo: req.query.redirectTo});
};

exports.importUsers = function(req, res){
  var filter =   '(&(objectCategory=person)(objectClass=user)(!(useraccountcontrol:1.2.840.113556.1.4.803:=2))(&(objectCategory=user)(memberOf=cn=office_md,ou=Groups,dc=codefactorygroup,dc=com)))';
  var opts = {
    includeMembership : [ 'user' ], // Optionally can use 'all'
    includeDeleted : false
  };
  var ad = new ActiveDirectory(adConfig);
  ad.find(filter, function(err, results) {
  if ((err) || (! results)) {
    console.log('ERROR: ' + JSON.stringify(err));
    return;
  }

  console.log('Importing Users');
  _.each(results.users, function(user) {
    // db.employees.import(user.name, user.sAMAccountName, Buffer.from(user.objectSid).toString('base64'));
    // res.write(JSON.stringify(user) + '\n');
    // console.log('  ' + Buffer.from(user.objectSid).toString('base64'));
  });
  res.end();
});
  // res.send("Imported ")
};

exports.logout = function(req,res){
  req.session.destroy();
  res.redirect('/login');
}

exports.loginFunc = function(req,res){
  // sess=req.session;

  var user_name=req.body.user;
  var password=req.body.password;
  var redirectTo=req.body.redirectTo;
  // db.employees.checkLogin(user_name)
  //   .then(data =>{
  //     if(data){
        var username = user_name+'@'+adConfig.domain;
        var ad = new ActiveDirectory(adConfig);

        ad.authenticate(username, password, function(err, auth) {
          if (err) {
            res.status(500);
            res.send("Authentication error : "+JSON.stringify(err));
            console.log('ERROR: '+JSON.stringify(err));
            return;
          }

          if (auth) {
            req.session.user=user_name ;
            ad.findUser(user_name, function(err, user) {
              if (err) {
                console.log('ERROR: ' +JSON.stringify(err));
                return;
              }

              if (! user) console.log('User: ' + sAMAccountName + ' not found.');
              else
                req.session.user_name=user.name ;
                // console.log(ad.opts.attributes.user[1])
                console.log('Authenticated! ' + req.session.user);
                if(redirectTo)
                  res.redirect(redirectTo);
                else
                  res.redirect('/');
                console.log("Sesson "+req.session.user_name);
            });
          }
          else {
            console.log('Authentication failed! ' +user_name);
            res.status(401);
            res.send("Authentication failed: "+user_name);
          }
        })
      // }
      // else{
      //   res.status(401);
      //   res.send("User "+user_name + " not allowed to login.");
      // }
    // })

};
