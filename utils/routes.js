// routes/index.js
var _         = require('lodash'),
    fs        = require('fs');
var login = require('./login');
// List all files in a directory in Node.js recursively in a synchronous fashion
var walkSync = function(root, dir, filelist) {
  var path = path || require('path');
  var fs = fs || require('fs'),
    files = fs.readdirSync(root + dir);
  filelist = filelist || [];
  files.forEach(function(file) {
    if (fs.statSync(path.join(root+dir, file)).isDirectory()) {
      filelist = walkSync(root,path.join(dir, file), filelist);
    }
    else {
      filelist.push(path.join(dir, file.split('.')[0]));
    }
  });
  return filelist;
};

module.exports = function(app) {
  //Add root location from root
  app.use('/', require('../routes/index'));

  var filelist = walkSync(__dirname + '/../routes','/');
  for (var i = 0, len = filelist.length; i < len; i++) {
    app.all(filelist[i]+'/*', login.checkUserSession);
    app.use(filelist[i], require('../routes'+filelist[i]));
  }
};
