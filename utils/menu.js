
var menu_items = {}

module.exports.menu_items = {
  "description": "menu from json",
  "MenuItems": [
    {
      "name": "Main menu",
      "items": [
        {
          "name": "Dashboard",
          "url": "/",
          "icon": "tachometer",

        }
      ]
    },
    {
      "isDivider": true
    },
    {
      "name": "Assets",
      "items": [
        {
          "name": "Workplaces",
          "url": "/workplaces",
          "icon": "table",
        },
        {
          "name": "Reports",
          "isMenu": true,
          "icon": "bar-chart-o",
          "items": [
            {
              "name": "User Devices",
              "url": "/rp/user_devices",
              "icon": "bar-chart-o",
            },
            {
              "name": "Workplaces Map",
              "url": "/rp/wp_map",
              "icon": "map-marker",
            }
          ]
        },
        {
          "name": "Hardware",
          "isMenu": true,
          "icon": "steam",
          "items": [
            {
              "name": "Devices",
              "url": "/hw/devices",
              "icon": "cogs",
            },
            {
              "name": "Device type",
              "url": "/hw/dvc_type",
              "icon": "cloudsmith",
            },
            {
              "isDivider": true
            },
            {
              "name": "Components",
              "url": "/hw/components",
              "icon": "cog",
            },
            {
              "name": "Component type",
              "url": "/hw/cpnt_type",
              "icon": "cloudsmith",
            },
            {
              "name": "Component status",
              "url": "/hw/cpnt_status",
              "icon": "cloudsmith",
            },
            {
              "isDivider": true
            },
            {
              "name": "Workplace type",
              "url": "/hw/wp_type",
              "icon": "cloudsmith",
            },
          ]
        },
        {
          "name": "Network",
          "isMenu": true,
          "icon": "sitemap",
          "items":[
            {
              "name": "Switches",
              "url": "/net/switches",
              "icon": "arrows-alt",
            },
            {
              "name": "VLANs",
              "url": "/net/vlans",
              "icon": "magnet",
            },
            {
              "name": "PatchPanels",
              "url": "/net/ppanels",
              "icon": "connectdevelop",
            },
            {
              "name": "Socket Groups",
              "url": "/net/sgroups",
              "icon": "calendar",
            },
          ]
        }
      ]
    },
    {
      "isDivider": true
    },
    {
      "name": "Related",
      "items": [
        {
          "name": "HR",
          "isMenu": true,
          "icon": "square-o",
          "items": [
            {
              "name": "Employees",
              "url": "/hr/employees",
              "icon": "user",
            },
            {
              "name": "Departments",
              "url": "/hr/departments",
              "icon": "users",
            },
            {
              "name": "Positions",
              "url": "/hr/positions",
              "icon": "user-circle",
            },
          ]
        },
        {
          "name": "Commerce",
          "isMenu": true,
          "icon": "square-o",
          "items": [
            {
              "name": "Procurements",
              "url": "#",
              "icon": "bar-chart-o",
            },
            {
              "name": "Companies",
              "url": "/commerce/partners",
              "icon": "copyright",
            },
            {
              "name": "Manufacturers",
              "url": "/commerce/manufacturers",
              "icon": "industry",
            },
          ]
        }
      ]
    },
    {
      "isDivider": true
    },
    {
      "name": "Configs",
      "items": [
        {
          "name": "Parameters",
          "url": "/settings/parameters",
          "icon": "tachometer",
        }
      ]
    },
    {
      "isDivider": true
    },
    {
      "name": "Tests",
      "items": [
        {
          "name": "Blank pages",
          "isMenu": true,
          "icon": "square-o",
          "items": [
            {
              "name": "Blank1",
              "url": "/tests/blank1",
              "icon": "bar-chart-o",
            },
            {
              "name": "Blank2",
              "url": "/tests/blank2",
              "icon": "bar-chart-o",
            },
            {
              "name": "table",
              "url": "/tests/table",
              "icon": "bar-chart-o",
            }
          ]
        }
      ]
    },
    {
      "isDivider": true
    }
  ]
}
