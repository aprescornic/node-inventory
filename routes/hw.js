'use strict';
var express = require('express');
var router = express.Router();
var request = require('request');
var login = require('../utils/login')
var menus = require('../utils/menu');


router.get('/devices', function(req, res, next) {
    res.render('hw/devices', {
        title: 'Devices',
        table_source: '/api/devices/',
        table_pretty: 'pretty',
        table_columns: "[{ data: 'id' },{ data: 'name' },{ data: 'workplace' },{ data: 'inventory_nr' }]",
        menu_items: menus.menu_items,
        user_name: req.session.user_name,
      });
});

router.get('/devices/:id', function(req, res, next) {
    res.render('hw/devices_edit', {
        title: 'Edit device',
        table_source: '/api/devices/',
        table_pretty: 'pretty',
        element_id: req.params.id,
        table_columns: "[{ data: 'id' },{ data: 'name' },{ data: 'workplace' },{ data: 'inventory_nr' }]",
        menu_items: menus.menu_items,
        user_name: req.session.user_name,
      });
});

router.get('/components', function(req, res, next) {
    res.render('hw/components', {
        title: 'Components',
        table_source: '/api/components/',
        table_pretty: 'pretty',
        table_columns: "[{ data: 'id' },{ data: 'name' },{ data: 'type' },{ data: 'manufacturer' },{ data: 'price' },{ data: 'comments' }]",
        menu_items: menus.menu_items,
        user_name: req.session.user_name,
      });
});
router.get('/components/:id', function(req, res, next) {
    res.render('hw/components_edit', {
        title: 'Edit component',
        table_source: '/api/components/',
        table_pretty: 'pretty',
        item_id: req.params.id,
        table_columns: "[{ data: 'id' },{ data: 'name' },{ data: 'workplace' },{ data: 'inventory_nr' }]",
        menu_items: menus.menu_items,
        user_name: req.session.user_name,
      });
});

router.get('/wp_type', function(req, res, next) {
    res.render('layouts/simple_table2', {
        title: 'Workplace type',
        table_source: '/api/wp_type/',
        table_columns: "[{ data: 'id' },{ data: 'name' }]",
        menu_items: menus.menu_items,
        user_name: req.session.user_name,
      });
});

router.get('/cpnt_status', function(req, res, next) {
    res.render('layouts/simple_table2', {
        title: 'Component status',
        table_source: '/api/cpnt_status/',
        table_columns: "[{ data: 'id' },{ data: 'name' }]",
        menu_items: menus.menu_items,
        user_name: req.session.user_name,
      });
});

router.get('/cpnt_type', function(req, res, next) {
    res.render('layouts/simple_table2', {
        title: 'Components type',
        table_source: '/api/cpnt_type/',
        table_columns: "[{ data: 'id' },{ data: 'name' }]",
        menu_items: menus.menu_items,
        user_name: req.session.user_name,
      });
});

router.get('/dvc_type', function(req, res, next) {
    res.render('hw/dvc_type', {
        title: 'Devices type',
        table_source: '/api/dvc_type/',
        table_columns: "[{ data: 'id' },{ data: 'name' }]",
        menu_items: menus.menu_items,
        user_name: req.session.user_name,
      });
});


module.exports = router;
