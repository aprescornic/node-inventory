'use strict';
var express = require('express');
var router = express.Router();
var request = require('request');
var login = require('../utils/login')

router.get('/parameters', function(req, res, next) {
    res.render('settings/parameters', {
        title: 'Parameters',
        table_source: '/api/configs/',
        table_columns: "[{ data: 'id' },{ data: 'name' },{ data: 'value' },{ data: 'type' },{ data: 'description' }]"
      });
});


module.exports = router;
