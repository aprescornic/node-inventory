'use strict';
var express = require('express');
var router = express.Router();
var request = require('request');
var menus = require('../utils/menu');

router.get('/switches', function(req, res, next) {
    res.render('net/switches', {
          title: 'Switches',
          table_source: '/api/switches/',
          table_pretty: 'pretty',
          table_columns: "[{ data: 'id' },{ data: 'name' }, { data: 'size' }]",
          menu_items: menus.menu_items,
          user_name: req.session.user_name,
      });
});

router.get('/vlans', function(req, res, next) {
    res.render('net/vlans', {
          title: 'VLANs',
          table_source: '/api/vlans/',
          table_columns: "[{ data: 'id' },{ data: 'name' }, { data: 'number' }]",
          menu_items: menus.menu_items,
          user_name: req.session.user_name,
      });
});

router.get('/ppanels', function(req, res, next) {
    res.render('net/ppanels', {
          title: 'Patch Panens',
          table_source: '/api/ppanels/',
          table_columns: "[{ data: 'id' },{ data: 'name' }, { data: 'size' }]"
      });
});

router.get('/sgroups', function(req, res, next) {
    res.render('net/sgroups', {
          title: 'Socket groups',
          table_source: '/api/sgroups/',
          table_columns: "[{ data: 'id' },{ data: 'name' }, {data: 'size'}]"
      });
});

module.exports = router;
