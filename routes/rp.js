'use strict';
var express = require('express');
var router = express.Router();
var request = require('request');
var menus = require('../utils/menu');

router.get('/user_devices', function(req, res, next) {
    res.render('rp/user_devices', {
        title: 'User Devices',
        table_source: '/api/user_devices/',
        table_columns: "[{ data: 'id' },{ data: 'name' }]"
       });
});

router.get('/wp_map', function(req, res, next) {
    res.render('rp/wp_map', {
        title: 'Workplaces Map',
        menu_items: menus.menu_items,
        user_name: req.session.user_name,
      });
});



module.exports = router;
