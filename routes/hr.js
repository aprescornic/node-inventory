'use strict';
var express = require('express');
var router = express.Router();
var request = require('request');
var menus = require('../utils/menu');

router.get('/positions', function(req, res, next) {
    res.render('layouts/simple_table2', {
        title: 'Positions',
        table_source: '/api/positions/',
        table_columns: "[{ data: 'id' },{ data: 'name' }]",
        menu_items: menus.menu_items,
        user_name: req.session.user_name,
       });
});

router.get('/employees', function(req, res, next) {
    res.render('hr/employees', {
        title: 'Employees',
        table_source: '/api/employees/',
        table_pretty: 'pretty',
        table_columns: "[{ data: 'id' },{ data: 'name' },{ data: 'department' },{ data: 'position' }, {data: 'is_group'}, {data: 'parrent_group'}]",
        menu_items: menus.menu_items,
        user_name: req.session.user_name,
      });
});

router.get('/departments', function(req, res, next) {
    res.render('layouts/simple_table2', {
        title: 'Departments',
        table_source: '/api/departments/',
        table_columns: "[{ data: 'id' },{ data: 'name' }]",
        menu_items: menus.menu_items,
        user_name: req.session.user_name,
      });
});


module.exports = router;
