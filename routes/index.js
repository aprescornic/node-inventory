'use strict';

const express = require('express');
var router = express.Router();
var request = require('request');
const session = require('express-session');
var login = require('../utils/login')
var menus = require('../utils/menu');

router.get('/login', login.loginPage);
router.post('/login', login.loginFunc);

router.get('/dashboard', login.checkUserSession,function(req, res, next) {
      res.render('dashboard', { title: 'Dashboard' });
});



router.get('/workplaces', login.checkUserSession,function(req, res, next) {
      res.render('workplaces', {
        title: 'Workplaces',
        table_source: '/api/workplaces/',
        table_pretty: 'pretty',
        table_columns: "[{ data: 'id' },{ data: 'name' },{ data: 'type' },{ data: 'department' },{ data: 'employe' },{ data: 'room' },{ data: 'phone' },{ data: 'ip' },{ data: 'comments'}]",
        menu_items: menus.menu_items,
        user_name: req.session.user_name,
      })
});
router.get('/workplaces/:id', function(req, res, next) {
    res.render('workplaces_edit', {
        title: 'Edit workplace',
        table_source: '/api/workplaces/',
        table_pretty: 'pretty',
        item_id: req.params.id,
        table_columns: "[{ data: 'id' },{ data: 'name' },{ data: 'workplace' },{ data: 'inventory_nr' }]",
        menu_items: menus.menu_items,
        user_name: req.session.user_name,
      });
});

var db = require('../db');

db.configs.findByName('default_start_page')
    .then(data =>{
      router.get('/', login.checkUserSession, function(req, res, next) {
        // if (login.checkLogin(req, res)){
        // res.render('index', { title: 'Express'});
          res.redirect(data.value);
        // }
      });
    })
    .catch(error => {
        console.log('ERROR:', error); // print the error;
        router.get('/', login.checkUserSession,function(req, res, next) {
          // res.render('index', { title: 'Express'});
          if (login.checkLogin(req, res)){
            res.redirect('/dashboard');
          }
        });
    })
    .finally(() => { });


module.exports = router;
