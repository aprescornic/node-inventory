'use strict';
var express = require('express');
var router = express.Router();
var request = require('request');
var menus = require('../utils/menu');

router.get('/entrances', function(req, res, next) {
    res.render('commerce/entrances', {
          title: 'Entrances',
          table_source: '/api/entrances/',
          table_pretty: 'pretty',
          table_columns: "[{ data: 'id' },{ data: 'name' },{ data: 'date' },{ data: 'partner' },{ data: 'invoice' },{ data: 'device'},{ data: 'price' }]"
      });
});

router.get('/manufacturers', function(req, res, next) {
    res.render('layouts/simple_table2', {
          title: 'Manufacturers',
          table_source: '/api/manufacturers/',
          table_columns: "[{ data: 'id' },{ data: 'name' }]",
          menu_items: menus.menu_items,
          user_name: req.session.user_name,
      });
});

router.get('/partners', function(req, res, next) {
    res.render('layouts/simple_table2', {
          title: 'Partners',
          table_source: '/api/partners/',
          table_columns: "[{ data: 'id' },{ data: 'name' }]",
          menu_items: menus.menu_items,
          user_name: req.session.user_name,
      });
});

module.exports = router;
