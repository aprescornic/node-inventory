'use strict';
var express = require('express');
var router = express.Router();
var url = require('url');
var db = require('../db')
var bodyParser = require('body-parser');
router.use(bodyParser.json()); // support json encoded bodies
router.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
var login = require('../utils/login')

const wp_typeController = require('../db/controllers').wp_type;
const wp_mapController = require('../db/controllers').wp_map;

//COLLECTED
GET('/collected/create', () =>  db.collected.create());
GET('/collected/drop', () =>  db.collected.drop());
GET('/collected/init', () =>  db.collected.init());
GET('/collected', () =>  db.collected.all());
router.post('/collected/:empl_id',  function (req, res) {
  db.collected.add(req.params.empl_id,req.body)
    .then(data => {
        res.json({
            success: true,
            data
        });
    })
    .catch(error => {
        res.json({
            success: false,
            error: error.message || error
        });
    });
});
GET('/collected/:id', req => db.collected.findById(req.params.id));
router.get('/collected/importUserPC/:empl_id',  function (req, res) {
  db.collected.getNonImported(req.params.empl_id,req.body)
    .then(
      data => {
        importPC(data);
        res.json({
            success: true,
            data
        });
    })
    .catch(error => {
        res.json({
            success: false,
            error: error.message || error
        });
    });
});
//Components
GET('/components/create', () =>  db.components.create());
GET('/components/drop', () =>  db.components.drop());
GET('/components/init', () =>  db.components.init());
GET('/components', () =>  db.components.all());
GET('/components/devices/:id', req => db.components.findDevices(req.params.id));
GET('/components/pretty', () =>  db.components.all_prety());
router.post('/components',  function (req, res) {
  var component = req.body;
  if(component.name){
    db.components.add(component.name, component.cpnt_type_id, component.manufacturers_id, component.price, component.comments)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/components/:id', req => db.components.findById(req.params.id));
DELETE('/components/:id',req => db.components.remove(req.params.id));
router.put('/components/:id',  function (req, res) {
    var component = req.body;
    if(component.name ){
      db.components.update(req.params.id,component.name, component.cpnt_type_id, component.manufacturers_id, component.price, component.comments)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name is required "
          });
        }
});

//CONFIGS
GET('/configs/create', () =>  db.configs.create());
GET('/configs/drop', () =>  db.configs.drop());
GET('/configs/init', () =>  db.configs.init());
GET('/configs', () =>  db.configs.all());
router.post('/configs',  function (req, res) {
  var config = req.body;
  if(config.name){
    db.configs.add(config.name, config.value, config.type, config.description)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/configs/:id', req => db.configs.findById(req.params.id));
DELETE('/configs/:id',req => db.configs.remove(req.params.id));
router.put('/configs/:id',  function (req, res) {
    var config = req.body;
    if(config.name ){
      db.configs.update(req.params.id,config.name, config.value, config.type, config.description)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name is required "
          });
        }
});

//Component Status
GET('/cpnt_status/create', () =>  db.cpnt_status.create());
GET('/cpnt_status/drop', () =>  db.cpnt_status.drop());
GET('/cpnt_status/init', () =>  db.cpnt_status.init());
GET('/cpnt_status', () =>  db.cpnt_status.all());
router.post('/cpnt_status',  function (req, res) {
  var config = req.body;
  if(config.name){
    db.cpnt_status.add(config.name)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/cpnt_status/:id', req => db.cpnt_status.findById(req.params.id));
DELETE('/cpnt_status/:id',req => db.cpnt_status.remove(req.params.id));
router.put('/cpnt_status/:id',  function (req, res) {
    var cpnt_status = req.body;
    if(cpnt_status.name){
      db.cpnt_status.update(req.params.id,cpnt_status.name)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name is required "
          });
        }
});

//Component Types
GET('/cpnt_type/create', () =>  db.cpnt_type.create());
GET('/cpnt_type/drop', () =>  db.cpnt_type.drop());
GET('/cpnt_type/init', () =>  db.cpnt_type.init());
GET('/cpnt_type', () =>  db.cpnt_type.all());
router.get('/cpnt_type2',  function (req, res) {
  db.cpnt_type.all2()
    .then(data => {
        res.json(
            data
        );
    })
    .catch(error => {
        res.json({
            success: false,
            error: error.message || error
        });
    });
});
router.post('/cpnt_type',  function (req, res) {
  var config = req.body;
  if(config.name){
    db.cpnt_type.add(config.name)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/cpnt_type/:id', req => db.cpnt_type.findById(req.params.id));
GET('/cpnt_type/search/:name', req => db.cpnt_type.searchByName(req.params.name));
DELETE('/cpnt_type/:id',req => db.cpnt_type.remove(req.params.id.split('?').slice(0)));
router.put('/cpnt_type/:id',  function (req, res) {
    var cpnt_type = req.body;
    if(cpnt_type.name){
      db.cpnt_type.update(req.params.id,cpnt_type.name)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name is required "
          });
        }
});

//DEPARTMENTS
GET('/departments/create', () =>  db.departments.create());
GET('/departments/drop', () =>  db.departments.drop());
GET('/departments/init', () =>  db.departments.init());
GET('/departments', () =>  db.departments.all());
router.post('/departments',  function (req, res) {
  var config = req.body;
  if(config.name){
    db.departments.add(config.name)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/departments/:id', req => db.departments.findById(req.params.id));
DELETE('/departments/:id',req => db.departments.remove(req.params.id));
router.put('/departments/:id',  function (req, res) {
    var department = req.body;
    if(department.name){
      db.departments.update(req.params.id,department.name)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name is required "
          });
        }
});

//DEVICES
GET('/devices/create', () =>  db.devices.create());
GET('/devices/drop', () =>  db.devices.drop());
GET('/devices/init', () =>  db.devices.init());
GET('/devices', () =>  db.devices.all());
GET('/devices/pretty', () =>  db.devices.all_prety());
router.post('/devices',  function (req, res) {
  var device = req.body;
  if(device.name){
    db.devices.add(device.name, device.workplaces_id, device.partners_id, device.dvc_type_id, device.inventory_nr)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/devices/:id', req => db.devices.findById(req.params.id));
GET('/devices/components/:id', req => db.devices.findComponents(req.params.id));
DELETE('/devices/:id',req => db.devices.remove(req.params.id));
router.put('/devices/:id',  function (req, res) {
    var device = req.body;
    if(device.name ){
      db.devices.update(req.params.id, device.name, device.workplaces_id, device.partners_id, device.dvc_type_id, device.inventory_nr)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name and workplace are required "
          });
        }
});

//DEVICES components
GET('/devices_components/create', () =>  db.devices_components.create());
GET('/devices_components/drop', () =>  db.devices_components.drop());
GET('/devices_components/init', () =>  db.devices_components.init());
GET('/devices_components', () =>  db.devices_components.all());
GET('/devices_components/pretty', () =>  db.devices_components.all_prety());
router.post('/devices_components',  function (req, res) {
  var device_component = req.body;
  if(device_component.devices_id && device_component.components_id){
    db.devices_components.add(device_component.devices_id, device_component.components_id, device_component.amount, device_component.serial_nr)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Device and component are required ")
    }
});
GET('/devices_components/:devices_id/:components_id', req => db.devices_components.findById(req.params.devices_id, req.params.components_id));
DELETE('/devices_components/:id',req => db.devices_components.remove(req.params.id));
router.put('/devices_components/:devices_id/:components_id',  function (req, res) {
    var device_component = req.body;
    if(device_component.devices_id, device_component.components_id ){
      db.devices_components.update(req.device_component.devices_id, device_component.components_id, device_component.amount, device_component.serial_nr)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Device and component  are required "
          });
        }
});
router.put('/devices_components/:id',  function (req, res) {
    var device_component = req.body;
    if(device_component.devices_id, device_component.components_id ){
      db.devices_components.update(req.params.id, device_component.devices_id, device_component.components_id, device_component.amount, device_component.serial_nr)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Device and component  are required "
          });
        }
});

//Devices types
GET('/dvc_type/create', () =>  db.dvc_type.create());
GET('/dvc_type/drop', () =>  db.dvc_type.drop());
GET('/dvc_type/init', () =>  db.dvc_type.init());
GET('/dvc_type', () =>  db.dvc_type.all());
router.post('/dvc_type',  function (req, res) {
  var dvc_type = req.body;
  if(dvc_type.name && dvc_type.in_prepend){
    db.dvc_type.add(dvc_type.name, dvc_type.in_prepend)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("All fields are required ")
    }
});
GET('/dvc_type/:id', req => db.dvc_type.findById(req.params.id));
DELETE('/dvc_type/:id',req => db.dvc_type.remove(req.params.id));
router.put('/dvc_type/:id',  function (req, res) {
    var dvc_type = req.body;
    if(dvc_type.name && dvc_type.in_prepend){
      db.dvc_type.update(req.params.id, dvc_type.name, dvc_type.in_prepend)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "All fields are required "
          });
        }
});

//EMPLOYEES
GET('/employees/create', () =>  db.employees.create());
GET('/employees/drop', () =>  db.employees.drop());
GET('/employees/init', () =>  db.employees.init());

GET('/employees', () =>  db.employees.all());
GET('/employees/pretty', () =>  db.employees.all_prety());
GET('/employees/pretty/:group', req  => db.employees.all_by_group(req.params.group))
router.get('/employees/import',  function (req, res) {
  login.importUsers(req,res)
});
router.post('/employees',  function (req, res) {
  var employe = req.body;
  if(employe.name){
    db.employees.add(employe.name, employe.position_id, employe.department_id, employe.is_group, employe.parrent_group, employe.allow_login)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/employees/:id', req => db.employees.findById(req.params.id));
DELETE('/employees/:id',req => db.employees.remove(req.params.id));
router.put('/employees/:id',  function (req, res) {
    var employe = req.body;
    if(employe.name ){
      db.employees.update(req.params.id,employe.name, employe.position_id, employe.department_id, employe.is_group, employe.parrent_group, employe.allow_login)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name, department and position are required "
          });
        }
});

//ENTRANCES
GET('/entrances/create', () =>  db.entrances.create());
GET('/entrances/drop', () =>  db.entrances.drop());
GET('/entrances/init', () =>  db.entrances.init());
GET('/entrances', () =>  db.entrances.all());
GET('/entrances/pretty', () =>  db.entrances.all_prety());
router.post('/entrances',  function (req, res) {
  var employe = req.body;
  if(employe.name){
    db.entrances.add(employe.name, employe.position_id, employe.department_id)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/entrances/:id', req => db.entrances.findById(req.params.id));
DELETE('/entrances/:id',req => db.entrances.remove(req.params.id));
router.put('/entrances/:id',  function (req, res) {
    var employe = req.body;
    if(employe.name ){
      db.entrances.update(req.params.id,employe.name, employe.position_id, employe.department_id)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name, department and position are required "
          });
        }
});

//Manufacturers
GET('/manufacturers/create', () =>  db.manufacturers.create());
GET('/manufacturers/drop', () =>  db.manufacturers.drop());
GET('/manufacturers/init', () =>  db.manufacturers.init());
GET('/manufacturers', () =>  db.manufacturers.all());
router.post('/manufacturers',  function (req, res) {
  var manufacturers = req.body;
  if(manufacturers.name){
    db.manufacturers.add(manufacturers.name)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/manufacturers/:id', req => db.manufacturers.findById(req.params.id));
GET('/manufacturers/search/:name', req => db.manufacturers.searchByName(req.params.name));
DELETE('/manufacturers/:id',req => db.manufacturers.remove(req.params.id));
router.put('/manufacturers/:id',  function (req, res) {
    var config = req.body;
    if(config.name ){
      db.manufacturers.update(req.params.id,config.name)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name is required "
          });
        }
});

//PARTNERS
GET('/partners/create', () =>  db.partners.create());
GET('/partners/drop', () =>  db.partners.drop());
GET('/partners/init', () =>  db.partners.init());
GET('/partners', () =>  db.partners.all());
router.post('/partners',  function (req, res) {
  var config = req.body;
  if(config.name){
    db.partners.add(config.name)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/partners/:id', req => db.partners.findById(req.params.id));
DELETE('/partners/:id',req => db.partners.remove(req.params.id));
router.put('/partners/:id',  function (req, res) {
    var config = req.body;
    if(config.name ){
      db.partners.update(req.params.id,config.name)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name is required "
          });
        }
});

//all ports
GET('/port_list', () =>  db.ports.all());
GET('/port_list/:column/:value', req => db.ports.findBy(req.params.column,req.params.value));

//POSITIONS
GET('/positions/create', () =>  db.positions.create());
GET('/positions/drop', () =>  db.positions.drop());
GET('/positions/init', () =>  db.positions.init());
GET('/positions', () =>  db.positions.all());
router.post('/positions',  function (req, res) {
  var config = req.body;
  if(config.name){
    db.positions.add(config.name)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/positions/:id', req => db.positions.findById(req.params.id));
DELETE('/positions/:id',req => db.positions.remove(req.params.id));
router.put('/positions/:id',  function (req, res) {
    var config = req.body;
    if(config.name ){
      db.positions.update(req.params.id,config.name)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name is required "
          });
        }
});

//PatchPanelsServiceGroups
GET('/ppanels_sgroups/create', () =>  db.ppanels_sgroups.create());
GET('/ppanels_sgroups/drop', () =>  db.ppanels_sgroups.drop());
GET('/ppanels_sgroups/init', () =>  db.ppanels_sgroups.init());
GET('/ppanels_sgroups', () =>  db.ppanels_sgroups.all());
GET('/ppanels_sgroups/pretty', () =>  db.ppanels_sgroups.all_prety());
router.post('/ppanels_sgroups',  function (req, res) {
  var ppanels_sgroups = req.body;
  if(ppanels_sgroups.ppanels_id && ppanels_sgroups.ppanels_pos && ppanels_sgroups.sgroups_id && ppanels_sgroups.sgroups_pos){
    db.ppanels_sgroups.add(ppanels_sgroups.ppanels_id, ppanels_sgroups.sgroups_id, ppanels_sgroups.ppanels_pos, ppanels_sgroups.sgroups_pos)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("All elements are required ")
    }
});
GET('/ppanels_sgroups/:column/:value', req => db.ppanels_sgroups.filterBy(req.params.column, req.params.value));
DELETE('/ppanels_sgroups/:id',req => db.ppanels_sgroups.remove(req.params.id));
router.put('/ppanels_sgroups/:id',  function (req, res) {
    var ppanels_sgroups = req.body;
    if(ppanels_sgroups.ppanels_id && ppanels_sgroups.ppanels_pos && ppanels_sgroups.sgroups_id && ppanels_sgroups.sgroups_pos){
      db.ppanels_sgroups.update(req.params.id, ppanels_sgroups.ppanels_id, ppanels_sgroups.sgroups_id, ppanels_sgroups.ppanels_pos, ppanels_sgroups.sgroups_pos)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "All elements are required "
          });
        }
});

//PatchPanels
GET('/ppanels/create', () =>  db.ppanels.create());
GET('/ppanels/drop', () =>  db.ppanels.drop());
GET('/ppanels/init', () =>  db.ppanels.init());
GET('/ppanels', () =>  db.ppanels.all());
router.post('/ppanels',  function (req, res) {
  var ppanels = req.body;
  if(ppanels.name){
    db.ppanels.add(ppanels.name, ppanels.size)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/ppanels/:id', req => db.ppanels.findById(req.params.id));
GET('/ppanels/search/:name', req => db.ppanels.searchByName(req.params.name));
DELETE('/ppanels/:id',req => db.ppanels.remove(req.params.id));
router.put('/ppanels/:id',  function (req, res) {
    var config = req.body;
    if(config.name ){
      db.ppanels.update(req.params.id,config.name, config.size)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name is required "
          });
        }
});

//sgroups
GET('/sgroups/create', () =>  db.sgroups.create());
GET('/sgroups/drop', () =>  db.sgroups.drop());
GET('/sgroups/init', () =>  db.sgroups.init());
GET('/sgroups', () =>  db.sgroups.all());
router.post('/sgroups',  function (req, res) {
  var sgroups = req.body;
  if(sgroups.name){
    db.sgroups.add(sgroups.name, sgroups.size)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/sgroups/:id', req => db.sgroups.findById(req.params.id));
GET('/sgroups/search/:name', req => db.sgroups.searchByName(req.params.name));
DELETE('/sgroups/:id',req => db.sgroups.remove(req.params.id));
router.put('/sgroups/:id',  function (req, res) {
    var config = req.body;
    if(config.name ){
      db.sgroups.update(req.params.id,config.name, config.size)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name is required "
          });
        }
});

//Switches
GET('/switches/create', () =>  db.switches.create());
GET('/switches/drop', () =>  db.switches.drop());
GET('/switches/init', () =>  db.switches.init());
GET('/switches', () =>  db.switches.all());
router.post('/switches',  function (req, res) {
  var switches = req.body;
  if(switches.name){
    db.switches.add(switches.name, switches.size)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/switches/:id', req => db.switches.findById(req.params.id));
GET('/switches/search/:name', req => db.switches.searchByName(req.params.name));
DELETE('/switches/:id',req => db.switches.remove(req.params.id));
router.put('/switches/:id',  function (req, res) {
    var config = req.body;
    if(config.name ){
      db.switches.update(req.params.id,config.name, config.size)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name is required "
          });
        }
});

//vlans
GET('/vlans/create', () =>  db.vlans.create());
GET('/vlans/drop', () =>  db.vlans.drop());
GET('/vlans/init', () =>  db.vlans.init());
GET('/vlans', () =>  db.vlans.all());
router.post('/vlans',  function (req, res) {
  var vlans = req.body;
  if(vlans.name){
    db.vlans.add(vlans.name, vlans.number)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/vlans/:id', req => db.vlans.findById(req.params.id));
GET('/vlans/search/:name', req => db.vlans.searchByName(req.params.name));
DELETE('/vlans/:id',req => db.vlans.remove(req.params.id));
router.put('/vlans/:id',  function (req, res) {
    var config = req.body;
    if(config.name ){
      db.vlans.update(req.params.id,config.name, config.number)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name is required "
          });
        }
});

//Workplaces
GET('/workplaces/create', () =>  db.workplaces.create());
GET('/workplaces/drop', () =>  db.workplaces.drop());
GET('/workplaces/init', () =>  db.workplaces.init());
GET('/workplaces', () =>  db.workplaces.all());
GET('/workplaces/pretty', () =>  db.workplaces.all_prety());
router.post('/workplaces',  function (req, res) {
  var workplace = req.body;
  if(workplace.name){
    db.workplaces.add(workplace.name, workplace.wp_type_id, workplace.departments_id, workplace.employees_id, workplace.room, workplace.phone, workplace.ip, workplace.comments)
      .then(data => {
          res.json({
              success: true,
              data
          });
      })
      .catch(error => {
          res.json({
              success: false,
              error: error.message || error
          });
      });
    } else {
      res.send("Name is required ")
    }
});
GET('/workplaces/:id', req => db.workplaces.findById(req.params.id));
GET('/workplaces/devices/:id', req => db.workplaces.findDevices(req.params.id));
GET('/workplaces/employees/:id', req => db.workplaces.findByEmployee(req.params.id));
DELETE('/workplaces/:id',req => db.workplaces.remove(req.params.id));
router.put('/workplaces/:id',  function (req, res) {
    var workplace = req.body;
    if(workplace.name && workplace.departments_id && workplace.employees_id ){
      db.workplaces.update(req.params.id,workplace.name, workplace.wp_type_id, workplace.departments_id, workplace.employees_id, workplace.room, workplace.phone, workplace.ip, workplace.comments)
        .then(data => {
            res.json({
                success: true,
                data
            });
        })
        .catch(error => {
            res.json({
                success: false,
                error: error.message || error
            });
        });
        } else {
          res.json({
              success: false,
              error: "Name, employee and department are required "
          });
        }
});

//Workplaces map
GET('/wp_map', () =>  wp_mapController.list());
GET('/wp_map/:id', req =>  wp_mapController.getById(req.params.id));
POST('/wp_map', req => wp_mapController.add(req.body));
PUT('/wp_map/:id', req => wp_mapController.update(req.params.id, req.body));
DELETE('/wp_map/:id', req => wp_mapController.delete(req.params.id));

//Workplaces types
GET('/wp_type', () =>  wp_typeController.list());
GET('/wp_type/:id', req =>  wp_typeController.getById(req.params.id));
POST('/wp_type', req => wp_typeController.add(req.body));
PUT('/wp_type/:id', req => wp_typeController.update(req.params.id, req.body));
DELETE('/wp_type/:id', req => wp_typeController.delete(req.params.id));
// GET('/wp_type/create', () =>  db.wp_type.create());
// GET('/wp_type/drop', () =>  db.wp_type.drop());
// GET('/wp_type/init', () =>  db.wp_type.init());
// GET('/wp_type', () =>  db.wp_type.all());
// router.post('/wp_type',  function (req, res) {
//   var config = req.body;
//   if(config.name){
//     db.wp_type.add(config.name)
//       .then(data => {
//           res.json({
//               success: true,
//               data
//           });
//       })
//       .catch(error => {
//           res.json({
//               success: false,
//               error: error.message || error
//           });
//       });
//     } else {
//       res.send("Name is required ")
//     }
// });
// GET('/wp_type/:id', req => db.wp_type.findById(req.params.id));
// DELETE('/wp_type/:id',req => db.wp_type.remove(req.params.id));
// router.put('/wp_type/:id',  function (req, res) {
//     var wp_type = req.body;
//     if(wp_type.name){
//       db.wp_type.update(req.params.id,wp_type.name)
//         .then(data => {
//             res.json({
//                 success: true,
//                 data
//             });
//         })
//         .catch(error => {
//             res.json({
//                 success: false,
//                 error: error.message || error
//             });
//         });
//         } else {
//           res.json({
//               success: false,
//               error: "Name is required "
//           });
//         }
// });

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Api' });
});


// Generic GET handler;
function GET(url, handler) {
  router.get(url, (req, res) => {
    handler(req)
      .then(data => {
        res.json({
          success: true,
          data
        });
      })
      .catch(error => {
        res.status(400).json({
          success: false,
          error: error.message || error
        });
      });
  });
}

// Generic DELETE handler;
function DELETE(url, handler) {
  router.delete(url, (req, res) => {
    handler(req)
      .then(data => {
        res.json({
          success: true,
          data
        });
      })
      .catch(error => {
        res.status(400).json({
          success: false,
          error: error.message || error
        });
      });
  });
}

function POST(url, handler) {
  router.post(url, (req, res) => {
    handler(req)
      .then(data => {
        res.status(201).json({
          success: true,
          data
        });
      })
      .catch(error => {
        res.status(400).json({
          success: false,
          error: error.message || error
        });
      });
  });
}

function PUT(url, handler) {
  router.put(url, (req, res) => {
    handler(req)
      .then(data => {
        res.status(200).json({
          success: true,
          data
        });
      })
      .catch(error => {
        res.status(400).json({
          success: false,
          error: error.message || error
        });
      });
  });
}

async function importPC(data){
  for (var i = 0, len = data.length; i < len; i++) {
    var importedPC = {};
    importedPC.id = data[i].id;
    importedPC.employee = data[i].empl_id;
    importedPC.pc = data[i].data.id;
    importedPC.description = data[i].data.description;
    importedPC.model = data[i].data.product;
    importedPC.vendor = data[i].data.vendor;
    importedPC.serial = data[i].data.serial;
    var baseComponents = data[i].data.children;
    for (var j = 0, jlen =baseComponents.length; j<jlen; j++ ){
      if (baseComponents[j].class ==="bus"){
        var importedMB = {};
        // console.log("    Component: "+ baseComponents[j].id);
        var mb = baseComponents[j];
        importedMB.model = mb.product;
        importedMB.vendor = mb.vendor;
        importedMB.serial = mb.serial;
        var network = [];
        var storage = {};
        var hdds = [];
        var importedRAM = {};
        var importedMemBanks =[];

        var mbComponents = mb.children;
        for (var k = 0, klen = mbComponents.length; k<klen; k++){
          switch (mbComponents[k].id) {
            case "memory": //RH,Fedora
              var mbComponent = mbComponents[k];
              importedRAM.size = mbComponent.size/1024/1024/1024;
              importedRAM.units = "GB";
              var ramBank = mbComponent.children;
              for(var l=0, llen=ramBank.length; l<llen; l++){
                // var ramBank = mbComponent.children[l];
                if(ramBank[l].description !== "[empty]"){
                  var bank = {};
                  bank.position = ramBank[l].id
                  bank.model = ramBank[l].description;
                  bank.vendor = ramBank[l].vendor;
                  bank.product = ramBank[l].product;
                  bank.serial = ramBank[l].serial;
                  bank.size =  ramBank[l].size/1024/1024/1024;
                  importedMemBanks.push(bank);
                }
              }
              break;
            case "cpu":
              var importedCPU = {};
              importedCPU.id = mbComponents[k].id;
              importedCPU.name = mbComponents[k].product;
              importedCPU.vendor = mbComponents[k].vendor;
              importedCPU.cores = mbComponents[k].configuration.cores;
              importedCPU.threads = mbComponents[k].configuration.threads;
              importedMB.cpu = importedCPU;
              break;
            case "pci":
              // console.log("          mbComponent:" +mbComponents[k].id);
              var pciComponents = mbComponents[k].children;
              for (var l = 0, llen = pciComponents.length; l < llen; l++) {
                switch (pciComponents[l].id) {
                  case "sata":
                    // console.log("          sataComponent:" +pciComponents[l].id);
                    var sataComponents = pciComponents[l].children;
                    for (var m = 0, mlen=sataComponents.length; m<mlen; m++) {
                      if (sataComponents[m].id === "cdrom") {
                        var cdrom = {};
                        cdrom.description = sataComponents[m].description;
                        cdrom.product = sataComponents[m].product;
                        cdrom.vendor = sataComponents[m].vendor;
                        storage.cdrom = cdrom;
                      }else
                      if (sataComponents[m].description === "ATA Disk") {
                        var hdd = {};
                        hdd.description = sataComponents[m].description;
                        hdd.model = sataComponents[m].product;
                        hdd.serial = sataComponents[m].serial;
                        hdd.size = ToInteger(sataComponents[m].size /1000000000);
                        hdd.vendor = "n/a";
                        hdds.push(hdd);
                      }
                    }
                    break;
                  case "network":
                    var networki  = {};
                    networki.description = pciComponents[l].description;
                    networki.mac = pciComponents[l].serial;
                    network.push(networki)
                    break;
                  default:
                    if (pciComponents[l].children) {
                      var otherComponents = pciComponents[l].children;
                      for (var m = 0, mlen=otherComponents.length; m<mlen; m++) {
                        if (otherComponents[m].id === "network") {
                          var networki  = {};
                          networki.description = otherComponents[m].description;
                          networki.mac = otherComponents[m].serial;
                          network.push(networki)
                        }
                      }
                    }
                }
              }
              break;
              case "scsi":
                if (mbComponents[k].class === "storage") {
                  var scsiComponents = mbComponents[k].children;
                  for (var l = 0, llen=scsiComponents.length; l<llen; l++) {
                    if (scsiComponents[l].id === "cdrom") {
                      var cdrom = {};
                      cdrom.description = scsiComponents[l].description;
                      cdrom.product = scsiComponents[l].product;
                      cdrom.vendor = scsiComponents[l].vendor;
                      storage.cdrom = cdrom;
                    }else
                    if (scsiComponents[l].description === "ATA Disk") {
                      var hdd = {};
                      hdd.description = scsiComponents[l].description;
                      hdd.model = scsiComponents[l].product;
                      hdd.serial = scsiComponents[l].serial;
                      hdd.size = ToInteger(scsiComponents[l].size /1000000000);
                      hdd.vendor = "n/a";
                      hdds.push(hdd);
                    }
                  }
                }
                break;
            default:
              if (mbComponents[k].class === "memory") { //DEb,Ubuntu
                if (mbComponents[k].children) {
                  var bank = {};
                  bank.position = mbComponents[k].id;
                  bank.model = mbComponents[k].children[0].description;
                  bank.vendor = mbComponents[k].children[0].vendor;
                  bank.product = mbComponents[k].children[0].product;
                  bank.serial = mbComponents[k].children[0].serial;
                  bank.size = mbComponents[k].children[0].size/1024/1024/1024;
                  importedMemBanks.push(bank);
                  if (importedRAM.size) {
                    importedRAM.size = Number(importedRAM.size) + Number(bank.size);
                  }else {
                      importedRAM.units = "GB";
                      importedRAM.size = bank.size;
                  }
                }
              }
              break;

          }
        }
        importedRAM.banks = importedMemBanks;
        importedMB.memory = importedRAM;
        storage.hdd = hdds;
        importedMB.storage = storage;
        importedMB.network = network;
        importedPC.mb = importedMB;
      }

    }
    await importPCData(importedPC)
  }
  console.log("END import");
}

async function importPCData(data){
  return new Promise(resolve => {
    setTimeout(async () => {
      // console.log("PC: "+JSON.stringify(data, null, 2));
      // console.log("PC: %j",data);
      var cpu = await SelectOrInsertComponent(data.mb.cpu.name,"CPU", data.mb.cpu.vendor, data.mb.cpu.cores + " cores, "+ data.mb.cpu.threads +" threads")
      data.mb.cpu.id = cpu;
      console.log("CPU ID: " +cpu);
      if(data.mb.storage.cdrom){
        var cdrom = await SelectOrInsertComponent(data.mb.storage.cdrom.product,"CD-Rom", data.mb.storage.cdrom.vendor, data.mb.storage.cdrom.description)
        data.mb.storage.cdrom.id = cdrom;
        console.log("CD-Rom: "+ cdrom);
      }
      for (var i = 0; i < data.mb.storage.hdd.length; i++) {
        var hdd = await SelectOrInsertComponent(data.mb.storage.hdd[i].model,"HDD", data.mb.storage.hdd[i].vendor, data.mb.storage.hdd[i].size + " GB");
        data.mb.storage.hdd[i].id= hdd;
        console.log("HDD: "+ hdd);
      }
      for (var i = 0; i < data.mb.memory.banks.length; i++) {
        // var temp_model = data.mb.memory.banks[i].description.split(' ').slice(0,2).join(' ')
        // var y = x.split(' ').slice(0,2).join('+');
        var model = data.mb.memory.banks[i].model.split(' ').slice(0,2).join(' ') + ' / ' +
                    data.mb.memory.banks[i].vendor + ' / ' +
                    data.mb.memory.banks[i].product.split('-').slice(0,1).join(' ') + ' / ' +
                    data.mb.memory.banks[i].size + ' GB';
        // temp_model = data.mb.memory.banks[i].vendor
        // var model = data.mb.memory.banks[i].description + data.mb.memory.banks[i].vendor + data.mb.memory.banks[i].product + data.mb.memory.banks[i].size
        var ram = await SelectOrInsertComponent(model,"RAM", data.mb.memory.banks[i].vendor, data.mb.memory.banks[i].size  + " GB");
        data.mb.memory.banks[i].id = ram;
        console.log("RAM: "+ ram);
      }
      for (var i = 0; i < data.mb.network.length; i++) {
        var lan = await SelectOrInsertComponent(data.mb.network[i].description,"LAN Card", "NoName", "");
        data.mb.network[i].id= lan;
        console.log("LAN: "+ hdd);
      }
      var mb = await SelectOrInsertComponent(data.mb.model,"MainBoard", data.mb.vendor, "")
      data.mb.id = mb;
      console.log("MB: " + mb);
      CreatePC(data)
      resolve (cpu)
    }, 500);
  });
}

function ToInteger(x) {
    x = Number(x);
    return x < 0 ? Math.ceil(x) : Math.floor(x);
}

function SelectOrInsertManufacturer(name) {
  return new Promise(resolve => {
    setTimeout(() => {
      db.manufacturers.findByName(name).then( value => {
        if (value)
          resolve (value.id);
        else {
          var response =  db.manufacturers.add(name)
          response.then(value => {
            if(value.id)
              resolve (value.id);
          })
        }
      });
    }, 500);
  });
}

async function SelectOrInsertComponent(name, type, vendor, comments) {
  return new Promise(resolve => {
    setTimeout(() => {
      var component = db.components.findByName(name);
      component.then(value => {
        if (value) {
          // console.log("Component exists");
          resolve (value.id);
        }else {
          // console.log("New");
          var cmp_type = db.cpnt_type.findByName(type)
          cmp_type.then(async value =>  {
            if (value) {
              var manufacturer =   await SelectOrInsertManufacturer(vendor);
               var response =  db.components.add(name, value.id, manufacturer, 0, comments)
               response.then(value => {
                 if(value)
                 resolve (value.id);
               })
            }
          })
        }
      });
    }, 500);
  });
}

function CreatePC(data) {
  console.log("PC: "+JSON.stringify(data, null, 2));
//   PC: {
//   "id": 6,
//   "employee": "34",
//   "pc": "ion",
//   "description": "Desktop Computer",
//   "model": "System Product Name (SKU)",
//   "vendor": "System manufacturer",
//   "serial": "System Serial Number",
//   "mb": {
//     "model": "P8H61-M LX3 PLUS R2.0",
//     "vendor": "ASUSTeK COMPUTER INC.",
//     "serial": "MT7026016206919",
//     "cpu": {
//       "id": "cpu",
//       "name": "Intel(R) Core(TM) i3-3220 CPU @ 3.30GHz",
//       "vendor": "Intel Corp.",
//       "cores": "2",
//       "threads": "2"
//     },
//     "memory": {
//       "units": "GB",
//       "size": 8,
//       "banks": [
//         {
//           "position": "memory:0",
//           "model": "DIMM DDR3 Synchronous 1333 MHz (0,8 ns)",
//           "vendor": "06C1",
//           "product": "WLA302G08-EDJ1C",
//           "serial": "00000160",
//           "size": 4
//         },
//         {
//           "position": "memory:1",
//           "model": "DIMM DDR3 Synchronous 1333 MHz (0,8 ns)",
//           "vendor": "Kingston",
//           "product": "99U5474-038.A00LF",
//           "serial": "8B2B4F37",
//           "size": 4
//         }
//       ]
//     },
//     "storage": {
//       "hdd": [
//         {
//           "description": "ATA Disk",
//           "model": "TS256GSSD230S",
//           "serial": "011081F2D66727790294",
//           "size": 256,
//           "vendor": "n/a"
//         }
//       ]
//     },
//     "network": [
//       {
//         "description": "Ethernet interface",
//         "mac": "30:85:a9:3d:0a:87"
//       }
//     ]
//   }
// }
    //create workplace
    //1.Name
    //2. Type workplace
    //3. departmen none
    //4. employee
  var workplace = db.workplaces.add("autoimported "+ data.pc, 0, 0, data.employee, "", "", "", "AutoImported")
  workplace.then(wp_value => {
    console.log("Workplace: "+ wp_value.id);
    //Create Device
    //1. Type PC
    //2. Name
    //3. workplace
    //4. serial_nr
    //5. owner
    var pc = db.devices.add("autoimported "+ data.pc, wp_value.id, 1, 2, "autoimported "+ data.id);
    pc.then(pc_value => {
      console.log("PC: "+pc_value.id);
      //Add components
      //1. add MB  + serial_nr
      db.devices_components.add(pc_value.id, data.mb.id, 1, data.mb.serial);
      //2. Add CPU
      db.devices_components.add(pc_value.id, data.mb.cpu.id, 1, '');
      //3. Add RAM + serial_nr
      for (var i = 0; i < data.mb.memory.banks.length; i++) {
        db.devices_components.add(pc_value.id, data.mb.memory.banks[i].id, 1, data.mb.memory.banks[i].serial);
      }
      //4. Add HDD + serial_nr
      for (var i = 0; i < data.mb.storage.hdd.length; i++) {
        db.devices_components.add(pc_value.id, data.mb.storage.hdd[i].id, 1, data.mb.storage.hdd[i].serial);
      }
      if (data.mb.storage.cdrom) {
        db.devices_components.add(pc_value.id, data.mb.storage.cdrom.id, 1, "");
      }
      //5. Add Lan + serial_nr
      for (var i = 0; i < data.mb.network.length; i++) {
        db.devices_components.add(pc_value.id, data.mb.network[i].id, 1, data.mb.network[i].mac);
      }
    })
  })
  //set PC as imported
  db.collected.markAsImported(data.id);
}


module.exports = router;
